////////////////////////////////////////////////////////////////////////////////
/// \file Interpol.h
/// \brief Interface of class Interpol
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef INTERPOL_H
#define INTERPOL_H

#include <vector>

namespace rac {

////////////////////////////////////////////////////////////////////////////////
/// \class Interpol
///
/// \brief Provides 3rd order interpolations for equally distanced points.
/// \author Andre Hoang, Christopher Lepenik
////////////////////////////////////////////////////////////////////////////////

class Interpol {
 public:
  /// Creates a Interpol object using the given coordinates.
  /**
   * At least 4 data points must be provided.
   *
   * \param xStart The smallest available x-value of the data.
   * \param xDelta The distance between the data points on the x-axis.
   * \param y The related y-values.
   */
  Interpol(double xStart, double xDelta, const std::vector<double>& y,
           double precisionGoal = 1e-15, int maxIter = 200);

  /// Returns interpolation value at the given \p x.
  double operator()(double x) const;
  /// Returns interpolation derivative value at the given \p x.
  /**
   * \note This function is not continuous.
   */
  double d(double x) const;
  /// Returns interpolation double derivative value at the given \p x.
  double dd(double x) const;
  /// Returns \p x for the given interpolation value \p y near the point \p x0.
  double inverse(double y, double x0) const;
  /// Returns the x value of the interpolation minimum or maximum near \p x0.
  double extr(double x0) const;

 private:
  int _maxIter;            ///< Maximal iterations used in inverse
  double _precisionGoal;   ///< Minimal relative accuracy of inverse
  int _ny;                 ///< Size of \p _y
  double _xStart;          ///< \p xStart given to constructor
  double _xDelta;          ///< \p xDelta given to constructor
  std::vector<double> _y;  ///< \p y given to constructor

  /// Rescales \p x to match <tt>xStart=0</tt> and <tt>xDelta=1</tt>.
  double xRescale(double x) const;

  /**
   * \name Interpolation polynomials and derivatives
   * \{
   */
  double f1(double x) const;
  double f2(double x) const;
  double d_f1(double x) const;
  double d_f2(double x) const;
  double dd_f1(double x) const;
  double dd_f2(double x) const;
  /**
   * \}
   */
};

}  // namespace rac

#endif  // INTERPOL_H
