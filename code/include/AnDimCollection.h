////////////////////////////////////////////////////////////////////////////////
/// \file AnDimCollection.h
/// \brief Interface of class AnDimCollection
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef ANDIMCOLLECTION_H
#define ANDIMCOLLECTION_H

//#include <iostream>
#include <unordered_map>

#include "AnDim.h"
#include "Global.h"
#include "TypeDef.h"

namespace revo {

////////////////////////////////////////////////////////////////////////////////
/// \class AnDimCollection
///
/// \brief Provides a collection of AnDim objects in a range of flavor numbers
/// \author Christopher Lepenik and Vicent Mateu
///
/// This class provides
///   - access to AnDim objects in a certain range of flavor number schemes,
///   - the easy possibility to apply member funcions to the whole range.
////////////////////////////////////////////////////////////////////////////////

class AnDimCollection {
 public:
  /**
   * \name Constructors
   * \{
   */
  /// Usual constructor.
  /**
   * \param n0 Minimal flavor number of AnDim objects in the collection.
   * \param nTot Maximal flavor number of AnDim objects in the collection.
   * \param scheme \p scheme set for AnDim objects.
   * \param precisionGoal \p precisionGoal set for AnDim objects.
   */
  AnDimCollection(int n0, int nTot, MScheme scheme = MScheme::MSbar,
                  double precisionGoal = kMaxPrec);
  /// Copy constructor.
  AnDimCollection(const AnDimCollection& other);
  /// Move constructor.
  AnDimCollection(AnDimCollection&& other);
  /// Default constructor.
  AnDimCollection() = default;
  /**
   * \}
   */

  /**
   * \name Assignment operators
   * \{
   */
  /// Copy assignment operator.
  AnDimCollection& operator=(const AnDimCollection& other);
  /// Move assignment operator.
  AnDimCollection& operator=(AnDimCollection&& other);
  /**
   * \}
   */

  /// Applies a function to all AnDim objects in the collection.
  /**
   * \param function Pointer to member function to be applied.
   * \param pars Arguments of \p function.
   */
  template <typename F, typename... Params>
  void apply(F function, Params&&... pars);
  /// Provides access to individual elements of the collection.
  /**
   * \param nf Flavor number of the AnDim object to ba accessed.
   */
  const AnDim& at(int nf) const;
  /// Counts elements with a specific flavor number
  /**
   * \sa at
   */
  int count(int nf) const;
  /// Provides access to individual elements of the collection.
  /**
   * \sa at
   */
  const AnDim& operator[](int nf) const;

 private:
  /// Map containing the individual AnDim objects of the collection.
  std::unordered_map<int, AnDim> _andimMap;

  /// Updates the pointer to _andimMap for all collection elements.
  void updatePointer();
};

////////////////////////////////////////////////////////////////////////////////
// Function template definitions
////////////////////////////////////////////////////////////////////////////////

template <typename F, typename... Params>
void AnDimCollection::apply(F function, Params&&... pars) {
  for (auto& ad : _andimMap) {
    ((ad.second).*function)(pars...);
  }
}

}  // namespace revo

#endif  // ANDIMCOLLECTION_H
