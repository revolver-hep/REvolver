////////////////////////////////////////////////////////////////////////////////
/// \file MQuantity.h
/// \brief Interface of class MQuantity
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef MQUANTITY_H
#define MQUANTITY_H

#include "Alpha.h"
#include "TypeDef.h"

namespace revo {

////////////////////////////////////////////////////////////////////////////////
/// \class MQuantity
///
/// \brief Provides MSbar evolution with arbitrary anomalous dimensions.
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
/// This class provides MQuantity objects which can be run with MSbar-evolution
/// with arbitrary anomalous dimensions.
/// Every MQuantity object is defined by
///   - the flavor number, initial scale, initial value and anomalous dimensions
///     of an MSbar-evolving QCD parameter,
///   - the same types of parameters for the strong coupling constant, where by
///     default the known beta function coefficients are used for running.
////////////////////////////////////////////////////////////////////////////////

class MQuantity {
 public:
  /**
   * \name Constructors
   * \{
   */
  /// Full constructor of the MQuantity object.
  /**
   * Constructs an MQuantity object for an MSbar-evolving QCD parameter and
   * coupling setting.
   *
   * Note that the flavor number of the given quantity and strong couping must
   * be equal.
   *
   * \param alphaPar Specifies the flavor scheme, value and scale of the input
   *                 coupling.
   * \param mPar Specifies the flavor scheme, value and scale of the input
   *             MSbar-evolving quantity.
   * \param gamma The MSbar-anomalous dimension of the quantity.
   * \param runAlpha The loop order used for the running of the strong coupling.
                     Default: highest available order which is 5.
   * \param lambdaAlpha A parameter probing the renormalization scale dependence
   *                    of the QCD beta-function. With respect to the
   *                    perturbative series of the beta-function truncated at
   *                    the order set by \p runAlpha, a (\p runAlpha + 1) order
   *                    term is estimated from renormalization scale variation:
   *                    the estimate is obtained by expanding the original
   *                    perturbative series (in powers of \f$\alpha_s(\mu)\f$)
   *                    in terms of \f$\alpha_s(\mathtt{lambda} \times \mu)\f$,
   *                    truncating at order \p runAlpha. The result is expanded
   *                    in \f$\alpha_s(\mu)\f$ truncating at order (\p runAlpha
   *                    + 1). Default: 1.0.
   * \param lambdaMSbar A parameter probing the renormalization scale dependence
   *                    of the MSbar-evolution equation. The variation is
   *                    performed by expanding the original perturbative series
   *                    (in powers of \f$\alpha_s(mm)\f$) in terms of
   *                    \f$\alpha_s(\mathtt{lambdaMSbar}\times mm)\f$,
   *                    truncating at the original order of \p gamma.
   *                    Default: 1.0.
   * \param beta The beta function coefficients used for the running of the
   *             strong coupling. Default: the known 5 coefficients.
   * \param precisionGoal The parameter setting the relative precision of all
   *                      convergent infinite sums and iterative algorithms. The
   *                      input value is clipped to the range [1e−6, 1e−15].
   *                      Default: 1e−15.
   */
  MQuantity(const RunPar& alphaPar, const RunPar& mPar, const doubleV& gamma,
            int runAlpha = kMaxRunAlpha, double lambdaAlpha = 1.0,
            double lambdaMSbar = 1.0, const doubleV& beta = doubleV(),
            double precisionGoal = kMaxPrec);
  /// Default constructor
  /**
   * The object created with this constructor is basically useless, but the
   * state of another MQuantity object can be copied to it.
   */
  MQuantity() = default;
  /**
   * \}
   */

  /// Returns the quantity at the scale \p mu.
  /**
   * \tparam T Type of R: \p double or \p std::complex<double>.
   */
  template <typename T>
  T operator()(T mu) const;

  /// Returns a constant Alpha object.
  const Alpha& alpha() const;

 private:
  //----------------------------------------------------------------------------
  // Private data members
  //----------------------------------------------------------------------------

  int _runMSbar;           ///< Number of loops used for MSbar evolution
  double _lambdaMSbar;     ///< Scaling parameter probing MSbar scale-dependence
  RunMethod _methodMSbar;  ///< Method used for MSbar running
  int _nTot;               ///< Number of total flavors
  int _runAlpha;           ///< Number of loops used for alpha running
  double _lambdaAlpha;     ///< Scaling parameter probing alpha scale-dependence
  double _precisionGoal;   ///< relative precision of iterative algorithms
  RunMethod _methodAlpha;  ///< Method used for alpha running
  Alpha _alp;              ///< Alpha object
  double _mScaleRef;       ///< Reference scale of given quantity
  double _mValueRef;       ///< Reference value of given quantity
  double _alm;             ///< alpha at the reference scale of m

};

////////////////////////////////////////////////////////////////////////////////
// Member function template definitions
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
template <typename T>
T MQuantity::operator()(T mu) const {
  T alLambda = _alp(mu * _lambdaMSbar, _nTot);
  return _mValueRef * std::exp(_alp.andim(_nTot).wTilde(_methodMSbar, _runMSbar,
                                                        _alm, alLambda));
}

}  // namespace revo

#endif  // MQUANTITY_H