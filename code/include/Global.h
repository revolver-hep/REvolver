////////////////////////////////////////////////////////////////////////////////
/// \file Global.h
/// \brief Declarations of some namespace wide available constants and functions
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef GLOBAL_H
#define GLOBAL_H
#define _USE_MATH_DEFINES

#include <algorithm>
#include <cmath>
#include <functional>
#include <iostream>
#include <numeric>
#include <utility>
#include <vector>

#include "Grid.h"
#include "TypeDef.h"

namespace revo {

/**
 * \defgroup Global Globally (namespace wide) available constants and functions.
 * \author Christopher Lepenik and Vicent Mateu
 *
 * This group contains constants and functions which are globally available
 * in the namespace \p revo.
 *
 * The constants include frequently used numbers like 4 * pi and numbers
 * specifying the precision of some numerical evaluations.
 *
 * The functions include tools to manipulate series expansions like inverting,
 * and mathematical functions like the dilogarithm.
 * \{
 */

//------------------------------------------------------------------------------
// Constants
//------------------------------------------------------------------------------

const double kFourPi = 4 * M_PI; ///< 4 * pi
const double kMaxPrec = 1.0e-15; ///< Maximal relative precision goal for iterations
const double kMinPrec = 1.0e-6;  ///< Minimal relative precision goal for iterations
const int kDefault = -1;         ///< Signals default arguments
const int kMaxRunAlpha = 5;      ///< Maximal order of \f$\alpha_s\f$ running
const int kMaxOrderAlpha = 4;    ///< Maximal order of \f$\alpha_s\f$ threshold matching
const int kMaxRunMSbar = 5;      ///< Maximal order of \f$\overline{\mathrm{MS}}\f$ mass running
const int kMaxRunMSR = 4;        ///< Maximal order of MSR mass running
const int kMaxOrderMSbar = 4;    ///< Maximal order of threshold matching for MSbar masses
const int kMaxOrderMSR = 4;      ///< Maximal order of threshold matching for MSR masses
const int kMaxOrderPs = 4;       ///< Maximal order for PS scheme conversions
const int kMaxOrder1s = 4;       ///< Maximal order for 1S scheme conversions
const int kMaxOrderKinetic = 3;  ///< Maximal order for the kinetic scheme conversions
const int kMaxIter = 200;        ///< Maximal number of iterations in many places
const doubleP kPow4 = doubleP(4.0); ///< Powers of 4

//------------------------------------------------------------------------------
// Functions
//------------------------------------------------------------------------------

/// Sums a series in the expansion parameter \p expPara.
/**
 * \p begin and \p end specify respective iterators to the beginning and end
 * of a container, which defines the terms of a series in \p expPara. The first
 * element of the container has to be \f$\mathcal O(\mathtt{exPara})\f$. \p init
 * specifies a starting value.
 */
template <typename iter, typename T>
T sumSeries(const iter begin, const iter end, T expPara, T init);
/// Returns the root of a function \p f, given the inital guess \p x0.
/**
 * This function uses a modified version of Dekker's method.
 */
template <typename F>
double findRoot(F f, double x0, double precisionGoal = kMaxPrec);
/// Gives entry <tt>a[ele]</tt> for <tt>ele < alen</tt>, and 0 otherwise.
double seriesExtend(const std::vector<double>& a, int alen, int ele);
/// Returns the binomial coefficients.
double binomial(int n, int k);
/// Expansion of a series where the expansion parameter gets redefined
/**
 * The expansion parameter \p x of the series represented by \p b, where the
 * series is <tt>1 + b[0] * x + b[1] * x^2 + ...</tt> is replaced by
 * <tt>x -> x * (a[0] + a[1] * x + ...)</tt>. Afterwards the series is
 * reexpanded in \p x. <tt>a[0]</tt> has to be equal to \p 1.
 *
 * If the \f$\mathcal O(x)\f$ term (= 1) is included in \p b, the parameter
 * \p b0Term can be set to \p true.
 */
void seriesReExpand(const std::vector<double>& a, std::vector<double>& b,
                    bool b0Term = false);
/// Gets the expansion of the inverse of \p aIn.
std::vector<double> seriesInverse(const std::vector<double>& aIn);
/// Multiplies out a Grid containing coefficients including logs.
std::vector<double> applyLogGrid(const rac::Grid<double>& logGrid, double lg);
/// Checks wether \p x is \p kDefault up to 1e-15.
bool isDefault(double x);
/// Linear interpolation between two points
double interpolLin(double x1, double y1, double x2, double y2, double xIn);
/// Real part of the Dilogarithm.
double Li2(double x, double precisionGoal = kMaxPrec);
/// Handles errors.
void error(ErrorType type, const char* s = nullptr);
/// Checks if the ordering of the input masses is correct.
void checkMassOrd(const std::vector<double>& mm);
/// Checks if maximal number of iterations is reached.
void checkIter(int k);
/// Clamps value x to the interval [min, max].
template <typename T>
T& applyConstrain(T& x, const T& min, const T& max);
/// Same as applyConstrain, but without changing x.
template <typename T>
T constrain(const T& x, const T& min, const T& max);
/// Multiplies all elements of \p v with \p val.
template <typename T>
std::vector<T>& multiplyAll(std::vector<T>& v, const T& val);
/// \}  // End of group Global

////////////////////////////////////////////////////////////////////////////////
// Function template definitions
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
template <typename iter, typename T>
T sumSeries(const iter begin, const iter end, T expPara, T init) {
  if (begin == end) {
    return init;
  } else {
    T pow = expPara;
    return std::accumulate(begin + 1, end, init + (*begin) * expPara,
                           [expPara, &pow](T initL, T elem) {
                             pow *= expPara;
                             return initL + elem * pow;
                           });
  }
}

//------------------------------------------------------------------------------
template <typename F>
double findRoot(F f, double x0, double precisionGoal) {
  double b[2];
  double fb[2];
  double s;
  b[0] = x0;
  b[1] = x0 * 1.00001;
  fb[0] = f(b[0]);
  fb[1] = f(b[1]);

  for (int i = 0; i < kMaxIter; ++i) {
    b[0] = (b[0] * fb[1] - b[1] * fb[0]) / (fb[1] - fb[0]);
    fb[0] = f(b[0]);
    std::swap(b[0], b[1]);
    std::swap(fb[0], fb[1]);
    if (std::abs(1 - b[1] / b[0]) < precisionGoal) {
      return b[1];
    } else if (fb[1] * fb[0] < 0) {
      break;
    } else if (i == kMaxIter - 1) {
      error(ErrorType::Iter);
      return std::nan("");
    }
  }

  for (int i = 0; i < kMaxIter; ++i) {
    s = (b[0] * fb[1] - b[1] * fb[0]) / (fb[1] - fb[0]);
    if ((s - b[1]) * (s - b[0]) > 0) {
      s = (b[1] + b[0]) / 2.0;
    }
    double fs = f(s);
    if (std::abs(1 - b[1] / s) < precisionGoal ||
        std::abs(1 - b[0] / s) < precisionGoal) {
      break;
    } else if (i == kMaxIter - 1) {
      s = std::nan("");
      break;
    }
    if (fb[1] * fs < 0) {
      b[0] = s;
      fb[0] = fs;
    } else {
      b[1] = s;
      fb[1] = fs;
    }
    if (std::abs(fb[0]) < std::abs(fb[1])) {
      std::swap(b[0], b[1]);
      std::swap(fb[0], fb[1]);
    }
  }
  return s;
}

//------------------------------------------------------------------------------
template <typename T>
T& applyConstrain(T& x, const T& min, const T& max) {
  if (x < min) {
    x = min;
  } else if (x > max) {
    x = max;
  }
  return x;
}

//------------------------------------------------------------------------------
template <typename T>
T constrain(const T& x, const T& min, const T& max) {
  if (x < min) {
    return min;
  } else if (x > max) {
    return max;
  }
  return x;
}

//------------------------------------------------------------------------------
template <typename T>
std::vector<T>& multiplyAll(std::vector<T>& v, const T& val) {
  for (auto& ve : v) {
    ve *= val;
  }
  return v;
}

////////////////////////////////////////////////////////////////////////////////
// Inline function definitions
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
inline double seriesExtend(const std::vector<double>& a, int alen, int ele) {
  if (alen <= 0) {
    return 0.0;
  }

  return (ele < alen) ? a[ele] : 0.0;
}

}  // namespace revo

#endif  // GLOBAL_H
