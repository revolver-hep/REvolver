////////////////////////////////////////////////////////////////////////////////
/// \file TypeDef.h
/// \brief Provides useful type definitions and structs.
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef TYPEDEF_H
#define TYPEDEF_H

#include <vector>

#include "Grid.h"
#include "Power.h"

namespace revo {
struct RunPar;

/**
 * \defgroup Types Type definitions
 * \author Christopher Lepenik
 *
 * This group contains type definitions via aliases, structs and enums.
 * \{
 */

//------------------------------------------------------------------------------
// Aliases
//------------------------------------------------------------------------------

using doubleV = std::vector<double>;  ///< Vector of doubles
using doubleG = rac::Grid<double>;    ///< Grid of doubles
using doubleP = rac::Power<double>;   ///< Power object of doubles
using RunParV = std::vector<RunPar>;  ///< Vector of RunPar objects

//------------------------------------------------------------------------------
// Structs
//------------------------------------------------------------------------------

/// Contains input values of a running quantity.
/**
 * Consists of the active number of flavors, value and scale at which the
 * quantity is provided.
 */
struct RunPar {
  /// Constructor to enable efficient syntax.
  RunPar(int nfIn, double valueIn, double scaleIn) {
    nf = nfIn;
    value = valueIn;
    scale = scaleIn;
  };
  RunPar() = default;
  int nf;        ///< Active number of flavors of the provided quantity
  double value;  ///< Value of the provided quantity
  double scale;  ///< Scale of the provided quantity
};

//------------------------------------------------------------------------------
// Enums
//------------------------------------------------------------------------------

/// %Mass schemes
/**
 * \note Not all functions with this input type accept all possible values.
 */
enum class MScheme { MSbar, MSRn, MSRp, Pole, Default };
/// Direction of matching
enum class Direction { Up = 1, Down = -1 };
/// Enables -Up == Down etc.
inline Direction& operator-(Direction& dir) {
  return dir = Direction(-int(dir));
}
/// Methods used for computing the running of \f$\alpha_s\f$ and masses
/**
 * \note Not all functions with this input type accept all possible values.
 */
enum class RunMethod {
  Exact,
  ExactPF,
  Iterative,
  Expanded,
  ExpandedPF,
  Inverse,
  Partfrac
};
/// For reading out different types of flavor numbers
enum class NfType { Total, Massive, Massless };
/// Power counting prescriptions for the 1S mass scheme
/**
 * \p Default is used as a default input parameter.
 */
enum class Count1S { Relativistic, Nonrelativistic, Default };
/// Available method for computing the asymptotic pole mass and ambiguity
enum class PoleMethod { Min, Range, DRange, Default };
/// Optional order parameters of Core objects set by constructor
enum class OrderPar {
  runAlpha,
  orderAlpha,
  runMSbar,
  runMSR,
  orderMSbar,
  orderMSR
};
/// Optional lambda parameters of Core objects set by constructor
enum class LambdaPar { lambdaAlpha, lambdaMSbar, lambdaMSR };
/// Schemes for LambdaQCD
enum class LambdaConvention { MSbar, tScheme };
/// Types of error messages
enum class ErrorType {
  Nf,
  Internal,
  MassOrd,
  CustomBeta,
  Iter,
  Keyword,
  NumMasses,
  NoCustomBeta,
  InvalidInput,
  NoMasses,
  InvalidState
};
/// \}  // End of group Types

}  // namespace revo

#endif  // TYPEDEF_H
