////////////////////////////////////////////////////////////////////////////////
/// \file RQuantity.h
/// \brief Interface of class RQuantity
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef RQUANTITY_H
#define RQUANTITY_H

#include "Alpha.h"
#include "Evolving.h"
#include "TypeDef.h"

namespace revo {

////////////////////////////////////////////////////////////////////////////////
/// \class RQuantity
///
/// \brief Provides arbitrary power R-evolution of a quantity.
/// \author Christopher Lepenik and Vicent Mateu
///
/// This class provides RQuantity objects which can be run with R-evolution
/// where R is raised to an arbitrary integer power.
/// Every RQuantity object is defined by
///   - the flavor number, initial scale, initial value and anomalous dimensions
///     of an R-evolving QCD parameter,
///   - the same types of parameters for the strong coupling constant, where by
///     default the known beta function coefficients are used for running.
////////////////////////////////////////////////////////////////////////////////

class RQuantity : public Evolving {
 public:
  /**
   * \name Constructors
   * \{
   */
  /// Full constructor of the RQuantity object.
  /**
   * Constructs an RQuantity object for an R-evolving QCD parameter and coupling
   * setting.
   *
   * Note that the flavor number of the given quantity and strong couping must
   * be equal.
   *
   * \param alphaPar Specifies the flavor scheme, value and scale of the input
   *                 coupling.
   * \param rPar Specifies the flavor scheme, value and scale of the input
   *             R-evolving quantity.
   * \param pR The power of R in R-evolution.
   * \param gamma The R-anomalous dimension of the quantity.
   * \param runAlpha The loop order used for the running of the strong coupling.
                     Default: highest available order which is 5.
   * \param lambdaAlpha A parameter probing the renormalization scale dependence
   *                    of the QCD beta-function. With respect to the
   *                    perturbative series of the beta-function truncated at
   *                    the order set by \p runAlpha, a (\p runAlpha + 1) order
   *                    term is estimated from renormalization scale variation:
   *                    the estimate is obtained by expanding the original
   *                    perturbative series (in powers of \f$\alpha_s(\mu)\f$)
   *                    in terms of \f$\alpha_s(\mathtt{lambda} \times \mu)\f$,
   *                    truncating at order \p runAlpha. The result is expanded
   *                    in \f$\alpha_s(\mu)\f$ truncating at order (\p runAlpha
   *                    + 1). Default: 1.0.
   * \param lambdaR A parameter probing the renormalization scale dependence
   *                of the R-evolution equation. The variation is performed
   *                by expanding the original perturbative series (in powers of
   *                \f$\alpha_s(R)\f$) in terms of
   *                \f$\alpha_s(\mathtt{lambdaR}\times R)\f$, truncating at
   *                the original order of \p gamma. Default: 1.0.
   * \param beta The beta function coefficients used for the running of the
   *             strong coupling. Default: the known 5 coefficients.
   * \param precisionGoal The parameter setting the relative precision of all
   *                      convergent infinite sums and iterative algorithms. The
   *                      input value is clipped to the range [1e−6, 1e−15].
   *                      Default: 1e−15.
   */
  RQuantity(const RunPar& alphaPar, const RunPar& rPar, int pR,
            const doubleV& gamma, int runAlpha = kMaxRunAlpha,
            double lambdaAlpha = 1.0, double lambdaR = 1.0, 
            const doubleV& beta = doubleV(), double precisionGoal = kMaxPrec);
  /// Default constructor
  /**
   * The object created with this constructor is basically useless, but the
   * state of another RQuantity object can be copied to it.
   */
  RQuantity() = default;
  /**
   * \}
   */

  /// Returns the quantity at the scale \p R.
  /**
   * \tparam T Type of R: \p double or \p std::complex<double>.
   */
  template <typename T>
  T operator()(T R) const;

  /// Returns a constant Alpha object.
  const Alpha& alpha() const override;

 private:
  //----------------------------------------------------------------------------
  // Private data members
  //----------------------------------------------------------------------------

  int _nTot;               ///< Number of total flavors
  int _runAlpha;           ///< Number of loops used for alpha running
  double _lambdaAlpha;     ///< Scaling parameter probing alpha scale-dependence
  double _precisionGoal;   ///< relative precision of iterative algorithms
  RunMethod _methodAlpha;  ///< Method used for alpha running
  Alpha _alp;              ///< Alpha object
  double _rScaleRef;       ///< Reference scale of given quantity
  double _rValueRef;       ///< Reference value of given quantity

  //----------------------------------------------------------------------------
  // Private member functions
  //----------------------------------------------------------------------------

  /// Returns an Alpha object
  Alpha& alpha() override;
};

////////////////////////////////////////////////////////////////////////////////
// Member function template definitions
////////////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
template <typename T>
T RQuantity::operator()(T R) const {
  return _rValueRef + rEvol(MScheme::MSRn, _methodR, _nTot, _rScaleRef, R);
}

}  // namespace revo

#endif  // RQUANTITY_H