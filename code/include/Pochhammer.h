////////////////////////////////////////////////////////////////////////////////
/// \file Pochhammer.h
/// \brief Interface and implementation of class Pochhammer
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#ifndef POCHHAMMER_H
#define POCHHAMMER_H

#include "InfVector.h"

namespace rac {

////////////////////////////////////////////////////////////////////////////////
/// \class Pochhammer
///
/// \brief Provides Pochhammer numbers.
/// \author Christopher Lepenik
///
/// A Pochhammer object, constructed with an input value \p var, provides the
/// Pochhammer numbers \f$(\mathtt{var})_i\f$ with arbitrary integer \f$i\f$.
/// The output can be obtained by the square or round bracket operator.
////////////////////////////////////////////////////////////////////////////////

class Pochhammer : public InfVector<double> {
 public:
  /// Default constructor.
  Pochhammer() = default;
  /// \brief Constructs an object to provide the Pochhammer symbols
  ///        \f$(\mathtt{var})_i\f$.
  explicit Pochhammer(double var)
      : InfVector<double>(
            [var](std::vector<double>& v) {
              v.push_back((var + v.size() - 1) * v.back());
            },
            1.0){};
};

}  // namespace rac

#endif  // POCHHAMMER_H
