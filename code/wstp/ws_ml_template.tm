::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: File: ws_ml_template.tm
:: Description: Template file for setting up the WSTP or MathLink library
:: Authors: Andre Hoang, Christopher Lepenik, Vicent Mateu
::
:: Copyright:
::    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
::
::    This file is part of REvolver.
::
::    REvolver is free software: you can redistribute it and/or modify
::    it under the terms of the GNU General Public License as published by
::    the Free Software Foundation, either version 3 of the License, or
::    (at your option) any later version.
::
::    REvolver is distributed in the hope that it will be useful,
::    but WITHOUT ANY WARRANTY; without even the implied warranty of
::    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
::    GNU General Public License for more details.
::
::    You should have received a copy of the GNU General Public License
::    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::


::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: REvolver main package
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Evaluate:	BeginPackage["REvolver`"]

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Package infos
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Evaluate:  Print["***********************************************************"]
:Evaluate:  Print["                        <------                            "]
:Evaluate:  Print["                         REvolver                          "]
:Evaluate:  Print["                           ------>                         "]
:Evaluate:  Print["                                                           "]
:Evaluate:  Print["   Version:            1.0.1                               "]
:Evaluate:  Print["   Authors:            A. H. Hoang, C. Lepenik, V. Mateu   "]
:Evaluate:  Print["   Last modification:  2021-03-07                          "]
:Evaluate:  Print["***********************************************************"]
:Evaluate:  Print["\nTo get an overview of all available functions execute "
  <> "\"?REvolver`*\".\n"
  <> "For an overview over predefined parameters and optional Core parameters "
  <> "execute\n\"?REvolver`Parameters`*\".\n"]
:Evaluate:  Print[Style["General Notations:", Bold]]
:Evaluate:  Print[Row[{"* ",
  Style["Running mass in the nf-flavor scheme", Italic], ": MSbar mass "
  <> "if the flavor number nf\n"
  <> "  includes this massive quark, MSR mass otherwise."}]]
:Evaluate:  Print[Row[{"* ", Style["Standard running mass", Italic], ": MSbar "
  <> "mass in the flavor scheme where all lighter quarks \n"
  <> "  along with this quark are treated dynamical, evaluated at the scale "
  <> "of this mass."}]]
:Evaluate:  Print[Row[{"* ", Style["Asymptotic pole mass", Italic], ": Pole "
  <> "mass value obtained from the running mass defined by\n"
  <> "  summing the perturbative series to the order of the minimal "
  <> "correction."}]]
:Evaluate:  Print[Row[{"* ", Style["Order-dependent pole mass", Italic],
  ": Pole mass value obtained from the running mass by\n"
  <> "  truncating the perturbative series at a specified order.\n"}]]
:Evaluate:  Print[Style["REvolver  Copyright (C) 2020 Andre Hoang, "
  <> "Christopher Lepenik, Vicent Mateu\n"
  <> "REvolver comes with ABSOLUTELY NO WARRANTY.\n"
  <> "REvolver is free software, and you are welcome to redistribute it under "
  <> "certain conditions.\n"
  <> "Execute \"REvolverLicense[]\" for details.",Italic, Smaller]]

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Documentation
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

::------------------------------------------------------------------------------
:: Add mass
::------------------------------------------------------------------------------

:Evaluate:  AddMSMass::usage =
  "AddMSMass[\"CoreName\", \"NewCoreName\", nf, m, \[Mu]] "
    <> "creates based on the Core object called CoreName containing lighter "
    <> "massive quarks a new Core object with the name NewCoreName where the "
    <> "running mass of a new heavier quark at a specified scale and in a "
    <> "specified flavor number scheme is added. The precision and convention "
    <> "settings concerning running and flavor matching of the Core object "
    <> "CoreName are passed on to the new Core object NewCoreName. - nf "
    <> "specifies in which flavor number scheme the running mass value m i"
    <> "s quoted. - m specifies the value of the running mass to be added. -"
    <> " \[Mu] specifies at which scale the running mass value m is"
    <> " quoted. - Additional option parameters: fnQ. Execute \"?fnQ\" for more"
    <> " information. See also: MassMS"
:Evaluate:  AddPoleMass::usage =
  "AddPoleMass[\"CoreName\", \"NewCoreName\", mPole, scale, \[Mu], method, f] "
    <> "creates a new Core object with the name NewCoreName where a new "
    <> "heaviest quark mass computed from the given asymptotic pole mass is "
    <> "added"
    <> " to the scenario parameters of the Core object CoreName. "
    <> "From the asymptotic pole mass value the running mass at the specified "
    <> "scale in the flavor number scheme where all massive quarks are "
    <> "integrated out is determined, which is then used to create the new Core"
    <> " object with the routine AddMSMass. The precision and convention "
    <> "settings concerning running and flavor matching are passed on to the"
    <> " Core object NewCoreName."
    <> "- mPole specifies the value of the asymptotic pole mass to be added. - "
    <> "scale specifies "
    <> "the scale of the running mass to which the asymptotic pole mass "
    <> "is converted. - \[Mu] specifies "
    <> "the scale of the strong coupling used for the conversion. Default: "
    <> "scale. - method specifies the prescription used for the definition "
    <> "of the asymptotic pole mass; available options are \"min\" for the "
    <> "minimal"
    <> " correction term method, \"range\" for the range method and \"drange\" "
    <> "for the corresponding discrete version. Default: is \"min\". - f specif"
    <> "ies a constant larger then unity multiplying the minimal correction for"
    <> " the method \"range\" or \"drange\". Default: 1.25. - Additional option"
    <> " parameters: fnQ. Execute \"?fnQ\" for more information. See "
    <> "<http://arxiv.org/abs/1706.08526> for details. See also: MassPole, "
    <> "AddPoleMassFO\n"
  <> "AddPoleMass[\"CoreName\", \"NewCoreName\", mPole, scale] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another. fnQ can be set via an option parameter."
:Evaluate:  AddPoleMassFO::usage =
  "AddPoleMassFO[\"CoreName\", \"NewCoreName\", mPole, nfConv, scale, \[Mu], "
    <> "order] "
    <> "creates a new Core object with the name NewCoreName where a new "
    <> "heaviest quark mass computed from the given order-dependent pole mass "
    <> "is added to the scenario of the Core object CoreName. "
    <> "From the order-dependent pole mass value the running mass at the "
    <> "specified scale and "
    <> "flavor number scheme is determined, which is then used to create the "
    <> "new Core object with the routine AddMSMass. "
    <> "The precision and convention settings concerning running and "
    <> "flavor matching are passed on to the Core object NewCoreName. "
    <> "- mPole specifies the value of the "
    <> "order-dependent pole mass to be added. - nfConv "
    <> "specifies the flavor number scheme of the running mass to which the "
    <> "order-dependent pole mass is converted. "
    <> "- scale specifies the scale of the running mass to which the "
    <> "order-dependent pole mass is converted. "
    <> "- \[Mu] specifies the scale of the strong coupling used in the "
    <> "conversion "
    <> "formula. - order specifies how many perturbative orders should be u"
    <> "sed for the conversion to the running mass scheme; for order > "
    <> "min(runMSR, runAlpha - 1) a renormalon-based "
    <> "asymptotic formula for the coefficients of the asymptotic "
    <> "series is used.  - Additional option parameters: fnQ. Execute "
    <> "?fnQ for more information. See also: MassPoleFO, AddPoleMass"
:Evaluate:  AddPSMass::usage =
  "AddPSMass[\"CoreName\", \"NewCoreName\", mPS, \[Mu]f, nfConv , scale, "
  <> "\[Mu], rIR, order] "
    <> "creates a new Core object with the name NewCoreName where a new "
    <> "heaviest quark mass computed from the given PS mass is "
    <> "added to the scenario of the Core object CoreName. "
    <> "The value of the PS mass is always quoted in the flavor scheme of the "
    <> "number of flavors contained in the Core object CoreName. From the PS "
    <> "mass value the running mass at the specified scale and flavor number "
    <> "scheme is computed which is then used to create the new Core object "
    <> "with the routine AddMSMass. The precision and convention settings "
    <> "concerning running and "
    <> "flavor matching are passed on to the Core object NewCoreName. "
    <> "- mPS specifies the value of the PS mass to be added. - "
    <> "\[Mu]f specifies at which subtraction scale the PS mass value mPS is "
    <> "quoted. - nfConv specifies "
    <> "the flavor number scheme of the running mass to which the PS mass is "
    <> "converted; possible values are "
    <> "the total number of flavors of the Core object CoreName (the running "
    <> "mass is in the MSR scheme) or the total number of flavors of the new"
    <> " Core object (the running mass is in the MSbar scheme); if the running "
    <> "mass is in the MSbar scheme, the QCD coupling is converted to the total"
    <> " flavor number of the Core object CoreName; default: total flavor "
    <> "number of the Core object CoreName. - scale specifies the scale of the "
    <> "running mass to which the PS mass is converted. "
    <> "Default: \[Mu]f. - \[Mu] specifies the scale of the strong coupling "
    <> "used for the conversion to the running mass. Default: \[Mu]f. - rIR "
    <> "specifies "
    <> "the IR scale employed in the 4-loop term of the pole-PS mass relation: "
    <> "rIR = \[Mu]IR/\[Mu]f. Default: 1. - order specifies how many perturbati"
    <> "ve orders should be used in the conversion to the running mass sch"
    <> "eme. Default: highest available order which is 4.  - Additional option"
    <> " parameters: fnQ. Execute \"?fnQ\" for more information. See "
    <> "also: MassPS\n"
  <> "AddPSMass[\"CoreName\", \"NewCoreName\", mPS, \[Mu]f] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another. fnQ can be set via an option parameter."
:Evaluate:  Add1SMass::usage =
  "Add1SMass[\"CoreName\", \"NewCoreName\", m1S, nfConv, scale, counting, \[Mu]"
    <> ", order] "
    <> "creates a new Core object with the name NewCoreName where a new "
    <> "heaviest quark mass computed from the given 1S mass is "
    <> "added to the scenario of the Core object CoreName. "
    <> "The value of the 1S mass is always quoted in the flavor scheme of the "
    <> "number of flavors contained in the Core object CoreName. From the 1S "
    <> "mass value the running mass at the specified scale and flavor number "
    <> "scheme is computed which is then used to create the new Core object "
    <> "with the routine AddMSMass. The precision and convention settings "
    <> "concerning running and "
    <> "flavor matching are passed on to the Core object NewCoreName. "
    <> "- m1S specifies the value of the 1S mass to be added. - nfConv specifie"
    <> "s the flavor number scheme of the running mass to which the 1S mass is "
    <> "converted; possible values are "
    <> "the total number of flavors of the Core object CoreName (the running "
    <> "mass is in the MSR scheme) or the total number of flavors of the new"
    <> " Core object (the running mass is in the MSbar scheme); if the running "
    <> "mass is in the MSbar scheme, the QCD coupling is converted to the total"
    <> " flavor number of the Core object CoreName; default: total flavor "
    <> "number of the Core object CoreName. - scale specifies the scale of the "
    <> "running mass to which the 1S mass is converted."
    <> " Default: if the MSR scheme is used (determined by nfConv) the default "
    <> "scale is the natural scale of the 1S scheme, i.e. the inverse Bohr radi"
    <> "us; in the MSbar case the standard running mass is used as default. - count"
    <> "ing specifies the expansion scheme used for the conversion to the "
    <> "running mass; available options: "
    <> "\"nonrelativistic\", \"relativistic\". Default: \"nonrelativistic\" "
    <> "for MSR and \"relativistic\" for MSbar; for nonrelativistic counting "
    <> "the leading correction is O(as^2) but counted a"
    <> "s order 1; see Sec. 5.2 of arXiv:1704.01580. - \[Mu] specifies the "
    <> "scale of the strong coupling used for the conversion to the running "
    <> "mass. Default: "
    <> "scale. - order specifies how many perturbative orders should be "
    <> "used in the conversion to the running mass scheme. Default: highest "
    <> "available order which is 4. - Additional option"
    <> " parameters: fnQ. Execute \"?fnQ\" for more information. See"
    <> " also: Mass1S, MBohr.\n"
  <> "Add1SMass[\"CoreName\", \"NewCoreName\", m1S] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another. fnQ can be set via an option parameter."
:Evaluate:  AddRGIMass::usage =
  "AddRGIMass[\"CoreName\", \"NewCoreName\", mRGI, order] "
    <> "creates a new Core object with the name NewCoreName where a new "
    <> "heaviest quark mass computed from the given RGI mass is "
    <> "added to the scenario of the Core object CoreName. "
    <> "The value of the standard running mass is computed from the given "
    <> "RGI mass. The precision and convention settings concerning running and "
    <> "flavor matching are passed on to the Core object NewCoreName. "
    <> "- mRGI specifies the value of the RGI mass to be added. - order "
    <> "specifies the loop order of the strong coupling and MSbar mass "
    <> "anomalous dimensions entering the conversion formula from the standard "
    <> "running mass. Default: highest available order, which is 5. -"
    <> " Additional option parameters: fnQ. Execute \"?fnQ\" for more "
    <> "information. "
    <> "See also: MassRGI.\n"
  <> "AddRGIMass[\"CoreName\", \"NewCoreName\", mRGI] "
    <> "like above with all default values set. fnQ can be"
    <> " set via an option parameter."
:Evaluate:  AddRSMass::usage =
  "AddRSMass[\"CoreName\", \"NewCoreName\", mRS, scaleRS, nfConv, scale, \[Mu],"
    <> " order, nRS, N12] "
    <> "creates a new Core object with the name NewCoreName where a new "
    <> "heaviest quark mass computed from the given RS mass is "
    <> "added to the scenario of the Core object CoreName. "
    <> "The value of the RS mass is always quoted in the flavor scheme of the "
    <> "number of flavors contained in the Core object CoreName. From the RS "
    <> "mass value the running mass at the specified scale and flavor number "
    <> "scheme is computed which is then used to create the new Core object "
    <> "with the routine AddMSMass. The precision and convention settings "
    <> "concerning running and "
    <> "flavor matching are passed on to the Core object NewCoreName. "
    <> "- mRS specifies the value of the RS mass to be added - scaleRS "
    <> "specifies at which subtraction scale the RS mass value mRS is "
    <> "quoted. - nfConv specifies "
    <> "the flavor number scheme of the running mass to which the RS mass is "
    <> "converted; possible values are the total number of flavors of the Core "
    <> "object"
    <> " before adding the mass (the running mass is in the MSR scheme) or the "
    <> "total number of flavors of the Core object after adding the mass (the ru"
    <> "nning mass is in the MSbar scheme); if the running mass is in the MSbar"
    <> " scheme, the QCD coupling is converted to the total flavor number of th"
    <> "e Core object before adding the mass. Default: total flavor number of t"
    <> "he Core object before adding the mass. - scale specifies the scale of "
    <> "the running mass to which the RS mass is converted. Default: scaleRS. -"
    <> " \[Mu] specifies the "
    <> "scale of the strong coupling used for the conversion to the running "
    <> "mass. "
    <> "Default: scaleRS. - order specifies"
    <> " how many perturbative orders should"
    <> " be used in the conversion to the running mass scheme. Default: highest"
    <> " available order which is 4. - nRS specifies the number of terms (used "
    <> "in the perturbative construction of the Borel function and the "
    <> "normalization N12) for the calculation of the coefficients of the "
    <> "pole-RS mass perturbation series. Default: highest available order "
    <> "which is 4. - N12 specifies the pole mass renormalon normalization "
    <> "constant employed in the pole-RS mass relation. Default: value computed"
    <> " by the sum rule formula employed by the routine N12 summing up nRS "
    <> "terms in the sumrule series using all available information on the "
    <> "beta-function and anomalous dimension of the MSR mass. - Additional "
    <> "option parameters: fnQ. Execute \"?fnQ\" for more information. See "
    <> "also: MassRS.\n"
  <> "AddRSMass[\"CoreName\", \"NewCoreName\", mRS, scaleRS] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another. fnQ can be set via an option parameter."
:Evaluate:  AddKinMass::usage =
  "AddKinMass[\"CoreName\", \"NewCoreName\", mKin, scaleKin, nfConv, scale, "
    <> "\[Mu], order] "
    <> "creates a new Core object with the name NewCoreName where a new "
    <> "heaviest quark mass computed from the given kinetic mass is "
    <> "added to the scenario of the Core object CoreName. "
    <> "The value of the kinetic mass is always quoted in the flavor scheme of "
    <> "the number of flavors contained in the Core object CoreName. From the "
    <> "kinetic mass value the running mass at the specified scale and flavor "
    <> "number "
    <> "scheme is computed which is then used to create the new Core object "
    <> "with the routine AddMSMass. The precision and convention settings "
    <> "concerning running and "
    <> "flavor matching are passed on to the Core object NewCoreName. "
    <> "- mKin specifies the value of the kinetic mass to be added - scaleKin "
    <> "specifies at which Wilsonian cut-off scale the kinetic mass value "
    <> "mKin is quoted. - nfConv specifies "
    <> "the flavor number scheme of the running mass to which the RS mass is "
    <> "converted; possible values are the total number of flavors of the Core "
    <> "object"
    <> " before adding the mass (the running mass is in the MSR scheme) or the "
    <> "total number of flavors of the Core object after adding the mass (the ru"
    <> "nning mass is in the MSbar scheme); if the running mass is in the MSbar"
    <> " scheme, the QCD coupling is converted to the total flavor number of th"
    <> "e Core object before adding the mass. Default: total flavor number of t"
    <> "he Core object before adding the mass. - scale specifies the scale of "
    <> "the running mass to which the kinetic mass is converted. Default: 2 * "
    <> "scaleKin. - \[Mu] specifies the "
    <> "scale of the strong coupling used for the conversion to the running "
    <> "mass. "
    <> "Default: 2 * scaleKin. - order specifies"
    <> " how many perturbative orders should "
    <> "be used in the conversion to the running mass scheme. Default: highest "
    <> "available order which is 3. - Additional option"
    <> " parameters: fnQ. Execute \"?fnQ\" for more information. See "
    <> "also: MassKin.\n"
  <> "AddKinMass[\"CoreName\", \"NewCoreName\", mKin, scaleKin] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another. fnQ can be set via an option parameter."

::------------------------------------------------------------------------------
:: Mass conversion and running from MS
::------------------------------------------------------------------------------

:Evaluate:  MassMS::usage =
  "MassMS[\"CoreName\", nfIn, scale, nfOut] "
    <> "returns the running mass at the scale scale from the Core object with "
    <> "the name CoreName. - nfIn specifies for which "
    <> "quark the running mass is returned and refers to the number of dynamic"
    <> "al flavors of the associated standard running mass. - scale specifies "
    <> "the scale of the running mass returned (complex values are accepted). -"
    <> " nfOut specifies that the running mass returned is in the nfOut-flavor "
    <> "scheme. Default: flavor number associated to the absolute value of "
    <> "scale with respect to the flavor matching scales of the Core object.\n"
  <> "MassMS[\"CoreName\", nfIn, scale] "
    <> "uses the default value for nfOut.\n"
  <> "MassMS[\"CoreName\", nfIn] "
    <> "returns the standard running mass of the specified quark flavor."
:Evaluate:  MassPole::usage =
  "MassPole[\"CoreName\", nfIn, scale, \[Mu], method, f] "
    <>"returns an asymptotic pole mass value from the Core object with the name"
    <> " CoreName using the MSR mass in the flavor scheme where all massive "
    <> "quarks "
    <> "are integrated out. - nfIn specifies for which quark the asymptotic pol"
    <> "e mass is returned and refers to the number of dynamical flavors of the"
    <> " associated standard running mass. - scale specifies the scale of the MS"
    <> "R mass from which the asymptotic pole mass is computed. - \[Mu] specifi"
    <> "es the renormalization scale of the strong coupling when the asymptotic"
    <> " pole mass is computed, and used for scale variation when using "
    <> "\"drange\" and \"range\". Default: scale. - method specifies the method"
    <> " used; available "
    <> "options are \"min\" for the minimal correction term method, "
    <> "\"range\" for the range method and \"drange\" for the corresponding dis"
    <> "crete version. Default: \"min\". - f specifies a constant larger then "
    <> "unity multiplying the minimal correction for the method \"range\" or "
    <> "\"drange\". Default: 1.25. See <http://arxiv.org/abs/1706.08526> for de"
    <> "tails. See also: MassPoleFO.\n"
  <> "MassPole[\"CoreName\", nfIn, scale] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another."
:Evaluate:  MassPoleDetailed::usage =
  "MassPoleDetailed[\"CoreName\", nfIn, scale, \[Mu], method, f] "
    <> "returns from the Core object with the name CoreName a list containing "
    <> "{the asymptotic pole mass, the associated renormalon ambiguity, the "
    <> "order of the minimal correction term}; input variables like for "
    <> "MassPole.\n"
  <> "MassPoleDetailed[\"CoreName\", nfIn, scale, \[Mu], method] "
    <> "like above using the default for f.\n"
  <> "MassPoleDetailed[\"CoreName\", nfIn, scale, method] "
    <> "like above using the default for f, \[Mu]."
:Evaluate:  MassPoleFO::usage =
  "MassPoleFO[\"CoreName\", nfIn, nfConv, scale, \[Mu], order] "
    <> "returns a pole mass value from the Core object with the name CoreName "
    <> "using conversion at a particular order in the perturbative expansion. -"
    <> " nfIn specifies for which quark the pole mass "
    <> "value is returned and refers to the number of dynamical flavors of "
    <> "the associated standard running mass. - nfConv specifies the flavor "
    <> "number of the running mass that is used for the "
    <> "conversion; for nfConv < nfIn the MSR scheme is used; else, the MSbar"
    <> "scheme is used. - scale specifies the scale of the running "
    <> "mass from which the conversion is carried out. - \[Mu] specifies "
    <> "the renormalization scale of the strong coupling for the conversion. "
    <> "- order specifies how many perturbative orders are used in the "
    <> "conversion; for order > min(runMSR,runAlpha - 1) a renormalon-based "
    <> "asymptotic formula for the "
    <> "coefficients of the asymptotic series is used. See <http://arxiv.org/ab"
    <> "s/1706.08526> for details. See also: MassPole."
:Evaluate:  MassPS::usage =
  "MassPS[\"CoreName\", nfIn, \[Mu]f, nfConv, scale, \[Mu], rIR, order] "
    <> "returns a Potential Subtracted (PS) mass value from the Core object "
    <> "with the name CoreName. - nfIn specifies for which "
    <> "quark the PS mass value is returned and refers to the number of dynamic"
    <> "al flavors of the associated standard running mass. - \[Mu]f specifies "
    <> "the subtraction scale \[Mu]f of the PS scheme. - nfConv specifies "
    <> "the flavor number of the running mass that is used "
    <> "for the conversion; for nfConv = nfIn - 1 the MSR scheme is used; for n"
    <> "fConv = nfIn the MSbar scheme is used, where the strong coupling is rew"
    <> "ritten to the nfIn - 1 flavor scheme. Default: nfIn - 1. - scale specif"
    <> "ies the scale of the running mass from which the conversion is determin"
    <> "ed. Default: \[Mu]f. - \[Mu] specifies the scale of the strong coupling"
    <> " used for the conversion. Default: \[Mu]f. - rIR specifies the IR scale"
    <> " employed in the 4-loop term of the pole-PS mass relation; rIR = \[Mu]I"
    <> "R/\[Mu]f. Default: 1. - order specifies how many perturbative orders ar"
    <> "e used in the conversion. Default: Highest available order which is 4.\n"
  <> "MassPS[\"CoreName\", nfIn, \[Mu]f] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another."
:Evaluate:  Mass1S::usage =
  "Mass1S[\"CoreName\", nfIn, nfConv, scale, counting, \[Mu], order] "
    <> "returns a 1S mass value from the Core object with the name "
    <> "CoreName. - nfIn specifies for which quark the 1S mass value is "
    <> "returned and refers to the"
    <> " number of dynamical flavors of the associated standard running mass. -"
    <> " nfConv specifies the flavor number of the running mass "
    <> "that is used for the conversion; for nfConv = nfIn - 1 the MSR scheme "
    <> "is used; for nfConv = nfIn the MSbar scheme is used, where the strong "
    <> "coupling is rewritten to the nfIn - 1 flavor scheme. Default: nfIn -"
    <> " 1. - scale specifies the scale of the running mass from which the "
    <> "conversion is determined; "
    <> "if the MSR scheme is used (determined by nfConv) the default scale is "
    <> "the inverse Bohr radius (which is returned by MBohr); if the MSbar s"
    <> "cheme is used the default scale is the standard running mass. - countin"
    <> "g specifies the expansion scheme used for the conversion; available opt"
    <> "ions: \"nonrelativistic\", \"relativistic\". For nonrelativistic "
    <> "counting the leading correction is O(as^2) but counted a"
    <> "s order 1; see Sec. 5.2 of arXiv:1704.01580. Default: \"nonrelativistic"
    <> "\" for MSR and \"relativistic\" for MSbar. Nonrelativistic counting for"
    <> "the MSbar scheme is not supported. - \[Mu] specifies the scale "
    <> "of the strong coupling used for the conversion; default: scale. - ord"
    <> "er specifies how many perturbative orders are used in the conversion fr"
    <> "om the running mass scheme. Default: highest available order which is 4"
    <> ". "
    <> "See also: MBohr\n"
  <> "Mass1S[\"CoreName\", nfIn] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another."
:Evaluate:  MBohr::usage =
  "MBohr[\"CoreName\", nfIn, nb] "
    <> "returns the inverse Bohr radius M_B = C_F *"
    <> " \[Alpha](M_B) * m(M_B), used for conversions involving the 1S "
    <> "mass for a quark in the Core object called CoreName. - nfIn specifies "
    <> "for which quark the 1S mass scale is returned and refers to the number "
    <> "of dynamical flavors of the associated standard running mass. - nb "
    <> "specifies an additional factor on the right hand side of the equation "
    <> "above, useful for scale variation. Default: 1.0. See also: Mass1S\n"
  <> "MBohr[\"CoreName\", nfIn] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another."
:Evaluate:  MassRGI::usage =
  "MassRGI[\"CoreName\", nfIn, order] "
    <> "returns a renormalization group invariant (RGI) mass value from the "
    <> "Core object with the name CoreName. - nfIn specifies "
    <> "for which quark the RGI mass value is returned and refers to the "
    <> "number of dynamical flavors of the associated standard running mass. "
    <> "- order specifies the loop order of the strong coupling and MSbar mass "
    <> "anomalous dimensions entering the conversion formula from the standard "
    <> "running mass. Default: highest available order, which is 5.\n"
  <> "MassRGI[\"CoreName\", nfIn] "
    <> "uses default parameters for order."
:Evaluate:  MassRS::usage =
  "MassRS[\"CoreName\", nfIn, scaleRS, nfConv, scale, \[Mu], order, nRS, N12] "
    <> "returns a Renormalon Subtracted scheme mass value from the "
    <> "Core object with the name CoreName. - nfIn specifies fo"
    <> "r which quark the RS mass is returned and refers to the number of "
    <> "dynamical flavors of the associated standard running mass. "
    <> "- scaleRS specifies the subtraction scale of the RS scheme. "
    <> "- nfConv specifies "
    <> "the flavor number of the running mass that is used "
    <> "for the conversion; for nfConv = nfIn - 1 the MSR scheme is used; for n"
    <> "fConv = nfIn the MSbar scheme is used, where the strong coupling is "
    <> "rewritten to the nfIn - 1 flavor scheme. Default: nfIn - 1. - scale "
    <> "specifies the scale of the running mass from "
    <> "which the conversion is determined. Default: scaleRS. - \[Mu] specifies"
    <> " the scale of the strong coupling used for the conversion. Default: "
    <> "scaleRS. - order specifies how many perturbative orders ar"
    <> "e used in the conversion. Default: highest available order which is "
    <> "4. - nRS specifies the number of terms (used "
    <> "in the perturbative construction of the Borel function and the "
    <> "normalization N12) for the calculation of the coefficients of the "
    <> "pole-RS mass perturbation series. Default: highest available order "
    <> "which is 4. - N12 specifies the pole mass renormalon normalization "
    <> "constant employed in the pole-RS mass relation. Default: value computed"
    <> " by the sum rule formula employed by the routine N12, summing up nRS "
    <> "terms in the sum rule series using all available information on the "
    <> "beta-function and anomalous dimension of the MSR mass.\n"
  <> "MassRS[\"CoreName\", nfIn, scaleRS] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another."
:Evaluate:  MassKin::usage =
  "MassKin[\"CoreName\", nfIn, scaleKin, nfConv, scale, \[Mu], order] "
    <> "returns a kinetic scheme mass value from the "
    <> "Core object with the name CoreName. - nfIn specifies for "
    <> "which quark the kinetic mass is returned and refers to the number of "
    <> "dynamical flavors of the associated standard running mass. "
    <> "- scaleKin specifies the Wilsonian cut-off scale of the kinetic scheme."
    <> " - nfConv specifies "
    <> "the flavor number of the running mass that is used "
    <> "for the conversion; for nfConv = nfIn - 1 the MSR scheme is used; for n"
    <> "fConv = nfIn the MSbar scheme is used, where the strong coupling is "
    <> "rewritten to the nfIn - 1 flavor scheme. Default: nfIn - 1. - scale "
    <> "specifies the scale of the running mass from "
    <> "which the conversion is determined. Default: 2*scaleKin. - \[Mu] "
    <> "specifies "
    <> "the scale of the strong coupling used for the conversion. Default: "
    <> "2*scaleKin. - order specifies how many perturbative orders ar"
    <> "e used in the conversion. Default: Highest available order which is "
    <> "3.\n"
  <> "MassKin[\"CoreName\", nfIn, scaleKin] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another."
:Evaluate:  N12::usage =
  "N12[\"CoreName\", lambda] "
    <> "returns the normalization of the pole mass renormalon according to the "
    <> "N12 convention. See <https://arxiv.org/abs/1704.01580> for more details"
    <> " on the used formula. The number of massless quarks for which the "
    <> "normalization is determined is tied to the Core used to extract the "
    <> "normalization. The number of terms summed up in the sum rule formula is"
    <> " set  by runMSR specified at Core construction. The beta-function "
    <> "coefficients entering the sum rule formula are used up to runAlpha loop"
    <> " order, all higher order coefficients are set to zero. - lambda "
    <> "specifies the "
    <> "scaling parameter which probes the uncertainty of N12. Default: 1.0.\n"
  <> "N12[\"CoreName\"] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another."
:Evaluate:  P12::usage =
  "P12[\"CoreName\", lambda] "
    <> "returns the normalization of the pole mass renormalon according to the "
    <> "P12 convention. See <https://arxiv.org/abs/1704.01580> for more details"
    <> " on the used formula. The number of massless quarks for which the "
    <> "normalization is determined is tied to the Core used to extract the "
    <> "normalization. The number of terms summed up in the sum rule formula is"
    <> " set  by runMSR specified at Core construction. The beta-function "
    <> "coefficients entering the sum rule formula are used up to runAlpha loop"
    <> " order, all higher order coefficients are set to zero. - lambda "
    <> "specifies the "
    <> "scaling parameter which probes the uncertainty of P12. Default: 1.0.\n"
  <> "P12[\"CoreName\"] "
    <> "like above with all default values set. Additional parameters can be "
    <> "set one after another."

::------------------------------------------------------------------------------
:: Coupling related
::------------------------------------------------------------------------------

:Evaluate:  AlphaQCD::usage =
  "AlphaQCD[\"CoreName\", \[Mu], nf] "
    <> "returns the strong coupling value at the scale \[Mu] with nf active "
    <> "flavors from the Core object with the name CoreName. Complex values"
    <> " for \[Mu] are accepted.\n"
  <> "AlphaQCD[\"CoreName\", \[Mu]] "
    <> "same as above, but with the flavor number associated to the absolute "
    <> "value of \[Mu]with respect to the flavor matching scales of the "
    <> "Core object."
:Evaluate:  LambdaQCD::usage =
  "LambdaQCD[\"CoreName\", nf, convention] "
    <> "returns LambdaQCD for the QCD coupling in the nf flavor scheme from the"
    <> " Core object with the name CoreName based on the specified convention "
    <> "with the precision for the evolution set by the Core object. - "
    <> "convention specifies the definition of LambdaQCD. Possible inputs: "
    <> "\"MSbar\" for the conventional MSbar definition according to the "
    <> "PDG2020, \"tScheme\" for the definition based on the t-variable "
    <> "notations of arXiv:1704.01580. Default: \"MSbar\"\n"
  <> "LambdaQCD[\"CoreName\", nf] "
    <> "like above with all default values set."
:Evaluate:  BetaCoefs::usage =
  "BetaCoefs[\"CoreName\"] "
    <> "prints the beta coefficients used for the strong coupling evolution."

::------------------------------------------------------------------------------
:: Core related
::------------------------------------------------------------------------------

:Evaluate:  CoreCreate::usage =
  "CoreCreate[\"CoreName\", nfTot, {nf\[Alpha], \[Alpha], \[Mu]}, {{nf1, m1,"
    <> " \[Mu]1}, {nf2, m2, \[Mu]2}, ...}] "
    <> "creates a Core object for a QCD running quark mass and coupling setting"
    <> " with name CoreName. - nfTot specifies the total number of quark "
    <> "flavors. - {nf\[Alpha], \[Alpha], \[Mu]} specifies the coupling value "
    <> "\[Alpha] in the nf\[Alpha]-flavor scheme at scale \[Mu]. "
    <> "- {{nf1, m1, \[Mu]1}, {nf2, m2, \[Mu]2}, ...} is a sorted list of lists"
    <> " specifying the masses of heavy quarks with standard running masses in "
    <> "the order of increasing values, where m1 is the running mass value in "
    <> "the nf1-flavor scheme at scale \[Mu]1 for the 1st lightest massive "
    <> "quark for the standard running mass, and so on. - Additional option "
    <> "parameters: fMatch = {f1, f2, ...}, runAlpha, lambdaAlpha, orderAlpha, "
    <> "runMSbar, lambdaMSbar, orderMSbar, runMSR, lambdaMSR, orderMSR,"
    <> " msBarDeltaError, precisionGoal. Execute \"?<parameter>\" for more "
    <> "information, where "
    <> "<parameter> stands for any of the listed option parameters.\n"
  <> "CoreCreate[\"CoreName\", {nf\[Alpha], \[Alpha], \[Mu]}] "
    <> "creates a Core object for the QCD coupling with all quarks massless. "
    <> "- {nf\[Alpha], \[Alpha], \[Mu]} like above. "
    <> "- Additional option "
    <> "parameters: runAlpha, lambdaAlpha, precisionGoal. Execute "
    <> "\"?<parameter>\" for more "
    <> "information, where <parameter> stands for any of the listed option "
    <> "parameters.\n"
  <> "CoreCreate[\"CoreName\", {nf\[Alpha], \[Alpha], \[Mu]}, "
    <> "{beta0, beta1, ...}] "
    <> "like above but with the option to set custom beta function "
    <> "coefficients. - beta0 specifies the first custom beta function "
    <> "coefficient and so on. The number of loops used "
    <> "for the evolution is deduced from the number of given coefficients. "
    <> "Note that adding mass to Core objects with custom beta function "
    <> "coefficients is not supported. Additional option parameters like above."
:Evaluate:  CoreDelete::usage =
  "CoreDelete[\"CoreName\"] "
    <> "deletes the Core object with name CoreName from memory.\n"
  <> "CoreDelete[{\"CoreName1\", \"CoreName2\", ...}] "
    <> "deletes multiple Core objects."
:Evaluate:  CoreDeleteAll::usage =
  "CoreDeleteAll[] "
    <> "deletes all Core objects from memory."
:Evaluate:  CoreParams::usage =
  "CoreParams[\"CoreName\"] "
    <> "returns from the specified Core a list containing the total number of "
    <> "flavors, the flavor number scheme, value and renormalization scale of "
    <> "the strong coupling specified at Core creation, and the standard "
    <> "running masses in increasing order. "
    <> "{nfTot ,{nf\[Alpha], \[Alpha], \[Mu]}, m1 standard running mass, m2 "
    <> "standard running mass, ...}."
:Evaluate:  CoreParamsDetail::usage =
  "CoreParamsDetail[\"CoreName\"] "
    <> "prints the coupling and mass values at all flavor matching scales (in "
    <> "the flavor schemes above as well as below the corresponding threshold),"
    <> " the flavor scheme, value as well as scale of the strong coupling "
    <> "specified at Core creation, and all optional parameters set by "
    <> "CoreCreate.\n"
  <> "CoreParamsDetail[\"CoreName\", output] "
    <> "as above, but additionally saves the parameters to the variable output."
:Evaluate:  CoreList::usage =
  "CoreList[] "
    <> "lists the names of all defined Core objects."

::------------------------------------------------------------------------------
:: License
::------------------------------------------------------------------------------

:Evaluate:  REvolverLicense::usage =
  "REvolverLicense[] "
    <> "prints some details on copyright and license."

::------------------------------------------------------------------------------
:: Errors and Warnings
::------------------------------------------------------------------------------

:Evaluate:  General::RNoKey =
            "Error: Core Name \"`1`\" does not exist!";
:Evaluate:  General::RKeyExists =
            "Error: Core Name \"`1`\" does already exist! Not overwritten.";
:Evaluate:  General::RNotSorted = 
            "Error: Input masses not sorted!";
:Evaluate:  General::RNoMasses =
            "Error: No masses given! Please use the dedicated CoreCreate.";
:Evaluate:  General::RCustomBeta =
            "Error: Functionality not available for Core objects with custom "
            <> "beta function coefficients."
:Evaluate:  General::RNoKeyQ =
            "Error: Quantity Name \"`1`\" does not exist!";
:Evaluate:  General::RKeyExistsQ =
            "Error: Quantity Name \"`1`\" does already exist! Not overwritten.";

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Function links
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Evaluate:	Begin["`private`"]

::------------------------------------------------------------------------------
:: Option checking functions
::------------------------------------------------------------------------------

:Evaluate:      SetAttributes[CheckAddOptions, HoldFirst];
:Evaluate:      CheckAddOptions[call:func_[args__, opts:OptionsPattern[]]] :=
                ReleaseHold[Hold[NumericQ[REvolver`Parameters`fnQ]]/.{opts}
                            /.Options[func]]

:Evaluate:      SetAttributes[CheckCreateOptions, HoldFirst];
:Evaluate:      CheckCreateOptions[call:func_[args__, opts:OptionsPattern[]]] :=
                ReleaseHold[
                  Hold[
                    ListQ[REvolver`Parameters`fMatch] &&
                    IntegerQ[REvolver`Parameters`runAlpha] &&
                    NumericQ[REvolver`Parameters`lambdaAlpha] &&
                    IntegerQ[REvolver`Parameters`orderAlpha] &&
                    IntegerQ[REvolver`Parameters`runMSbar] &&
                    NumericQ[REvolver`Parameters`lambdaMSbar] &&
                    IntegerQ[REvolver`Parameters`orderMSbar] &&
                    IntegerQ[REvolver`Parameters`runMSR] &&
                    NumericQ[REvolver`Parameters`lambdaMSR] &&
                    IntegerQ[REvolver`Parameters`orderMSR] &&
                    NumericQ[REvolver`Parameters`msBarDeltaError] &&
                    NumericQ[REvolver`Parameters`precisionGoal]
                  ]/.{opts}/.Options[func]
                ]

::------------------------------------------------------------------------------
:: Add mass
::------------------------------------------------------------------------------

:Evaluate:      Options[AddMSMass] = REvolver`Parameters`private`AddOptionList

:Begin:
:Function:      addMsMass
:Pattern:       AddMSMass[CoreName_String, NewCoreName_String, nf_Integer,
                          mass_?NumericQ, scale_?NumericQ,
                          OptionsPattern[]]?CheckAddOptions
:Arguments:     {CoreName, NewCoreName, nf, N[mass], N[scale],
                 N[OptionValue[fnQ]]}
:ArgumentTypes: {String, String, Integer, Real, Real, Real}
:ReturnType:    Manual
:End:

:Evaluate:      Options[AddPoleMass] = REvolver`Parameters`private`AddOptionList

:Begin:
:Function:      addPoleMass
:Pattern:       AddPoleMass[CoreName_String, NewCoreName_String,
                            mPole_?NumericQ, scale_?NumericQ,
                            Optional[muA_?NumericQ,
                                     REvolver`Parameters`kDefault],
                            method_String:"min",
                            Optional[f_?NumericQ, 1.25],
                            OptionsPattern[]]?CheckAddOptions
:Arguments:     {CoreName, NewCoreName, N[mPole], N[scale], N[muA], method,
                 N[f], N[OptionValue[fnQ]]}
:ArgumentTypes: {String, String, Real, Real, Real, String, Real, Real}
:ReturnType:    Manual
:End:

:Evaluate:      Options[AddPoleMassFO] =
                REvolver`Parameters`private`AddOptionList

:Begin:
:Function:      addPoleMassFO
:Pattern:       AddPoleMassFO[CoreName_String, NewCoreName_String,
                              mPole_?NumericQ, nfConv_Integer, scale_?NumericQ,
                              muA_?NumericQ, order_Integer,
                              OptionsPattern[]]?CheckAddOptions
:Arguments:     {CoreName, NewCoreName, N[mPole], nfConv, N[scale], N[muA],
                 order, N[OptionValue[fnQ]]}
:ArgumentTypes: {String, String, Real, Integer, Real, Real, Integer, Real}
:ReturnType:    Manual
:End:

:Evaluate:      Options[AddPSMass] = REvolver`Parameters`private`AddOptionList

:Begin:
:Function:      addPSMass
:Pattern:       AddPSMass[CoreName_String, NewCoreName_String, mPS_?NumericQ,
                          muF_?NumericQ,
                          nfConv_Integer:REvolver`Parameters`kDefault,
                          Optional[scale_?NumericQ,
                                   REvolver`Parameters`kDefault],
                          Optional[muA_?NumericQ, REvolver`Parameters`kDefault],
                          Optional[rIR_?NumericQ, 1],
                          order_Integer:REvolver`Parameters`kMaxOrderPs,
                          OptionsPattern[]]?CheckAddOptions
:Arguments:     {CoreName, NewCoreName, N[mPS], N[muF], nfConv, N[scale],
                 N[muA], N[rIR], order, N[OptionValue[fnQ]]}
:ArgumentTypes: {String, String, Real, Real, Integer, Real, Real, Real, Integer,
                 Real}
:ReturnType:    Manual
:End:

:Evaluate:      Options[Add1SMass] = REvolver`Parameters`private`AddOptionList

:Begin:
:Function:      add1SMass
:Pattern:       Add1SMass[CoreName_String, NewCoreName_String, m1S_?NumericQ,
                          nfConv_Integer:REvolver`Parameters`kDefault,
                          Optional[scale_?NumericQ,
                                   REvolver`Parameters`kDefault],
                          counting_String:"default",
                          Optional[muA_?NumericQ, REvolver`Parameters`kDefault],
                          order_Integer:REvolver`Parameters`kMaxOrder1s,
                          OptionsPattern[]]?CheckAddOptions
:Arguments:     {CoreName, NewCoreName, N[m1S], nfConv, N[scale], counting,
                 N[muA], order, N[OptionValue[fnQ]]}
:ArgumentTypes: {String, String, Real, Integer, Real, String, Real, Integer,
                 Real}
:ReturnType:    Manual
:End:

:Evaluate:      Options[AddRGIMass] = REvolver`Parameters`private`AddOptionList

:Begin:
:Function:      addRGIMass
:Pattern:       AddRGIMass[CoreName_String, NewCoreName_String, mRGI_?NumericQ,
                           order_Integer:REvolver`Parameters`kMaxRunMSbar,
                           OptionsPattern[]]?CheckAddOptions
:Arguments:     {CoreName, NewCoreName, N[mRGI], order, N[OptionValue[fnQ]]}
:ArgumentTypes: {String, String, Real, Integer, Real}
:ReturnType:    Manual
:End:

:Evaluate:      Options[AddRSMass] = REvolver`Parameters`private`AddOptionList

:Begin:
:Function:      addRSMass
:Pattern:       AddRSMass[CoreName_String, NewCoreName_String, mRS_?NumericQ,
                          scaleRS_?NumericQ,
                          nfConv_Integer:REvolver`Parameters`kDefault,
                          Optional[scale_?NumericQ,
                                   REvolver`Parameters`kDefault],
                          Optional[muA_?NumericQ, REvolver`Parameters`kDefault],
                          order_Integer:REvolver`Parameters`kMaxRunMSR,
                          nRS_Integer:REvolver`Parameters`kMaxRunAlpha - 1,
                          Optional[N12_?NumericQ, REvolver`Parameters`kDefault],
                          OptionsPattern[]]?CheckAddOptions
:Arguments:     {CoreName, NewCoreName, N[mRS], N[scaleRS], nfConv, N[scale],
                 N[muA], order, nRS, N[N12], N[OptionValue[fnQ]]}
:ArgumentTypes: {String, String, Real, Real, Integer, Real, Real, Integer,
                 Integer, Real, Real}
:ReturnType:    Manual
:End:

:Evaluate:      Options[AddKinMass] = REvolver`Parameters`private`AddOptionList

:Begin:
:Function:      addKinMass
:Pattern:       AddKinMass[CoreName_String, NewCoreName_String, mKin_?NumericQ,
                           scaleKin_?NumericQ,
                           nfConv_Integer:REvolver`Parameters`kDefault,
                           Optional[scale_?NumericQ,
                                    REvolver`Parameters`kDefault],
                           Optional[muA_?NumericQ,
                                    REvolver`Parameters`kDefault],
                           order_Integer:REvolver`Parameters`kMaxOrderKinetic,
                           OptionsPattern[]]?CheckAddOptions
:Arguments:     {CoreName, NewCoreName, N[mKin], N[scaleKin], nfConv, N[scale],
                 N[muA], order, N[OptionValue[fnQ]]}
:ArgumentTypes: {String, String, Real, Real, Integer, Real, Real, Integer, Real}
:ReturnType:    Manual
:End:

::------------------------------------------------------------------------------
:: Mass conversion and running from MS
::------------------------------------------------------------------------------

:Begin:
:Function:      mMS
:Pattern:       MassMS[CoreName_String, nfIn_Integer]
:Arguments:     {CoreName, nfIn}
:ArgumentTypes: {String, Integer}
:ReturnType:    Manual
:End:

:Evaluate:      MassMS[CoreName_String, nfIn_Integer, scale_?NumericQ,
                       Optional[nfOut_?NumericQ,
                                REvolver`Parameters`kDefault]] := Which[
                Element[scale, Reals]&&(scale > 0),
                MassMSRe[CoreName, nfIn, scale, nfOut],
                Element[scale, Complexes],
                MassMSCo[CoreName, nfIn, Re[scale], Im[scale], nfOut],
                Element[scale, Reals]&&(scale < 0),
                MassMSCo[CoreName, nfIn, scale, 0.0, nfOut]
                ]

:Begin:
:Function:      mMS
:Pattern:       MassMSRe[CoreName_String, nfIn_Integer, scale_?NumericQ,
                         nfOut_Integer]
:Arguments:     {CoreName, nfIn, N[scale], nfOut}
:ArgumentTypes: {String, Integer, Real, Integer}
:ReturnType:    Manual
:End:

:Begin:
:Function:      mMS
:Pattern:       MassMSCo[CoreName_String, nfIn_Integer, scaleRe_, scaleIm_,
                         nfOut_Integer]
:Arguments:     {CoreName, nfIn, N[scaleRe], N[scaleIm], nfOut}
:ArgumentTypes: {String, Integer, Real, Real, Integer}
:ReturnType:    Manual
:End:

:Begin:
:Function:      mPole
:Pattern:       MassPoleDetailed[CoreName_String, nfIn_Integer, scale_?NumericQ,
                                 Optional[muA_?NumericQ,
                                 REvolver`Parameters`kDefault],
                                 method_String,
                                 Optional[f_?NumericQ, 1.25]]
:Arguments:     {CoreName, nfIn, N[scale], N[muA], method, N[f]}
:ArgumentTypes: {String, Integer, Real, Real, String, Real}
:ReturnType:    Manual
:End:

:Evaluate:      MassPole[CoreName_String, nfIn_Integer, scale_?NumericQ,
                         Optional[muA_?NumericQ, REvolver`Parameters`kDefault],
                         method_String:"min",
                         Optional[f_?NumericQ, 1.25]] :=
                MassPoleDetailed[CoreName, nfIn, scale, muA, method, f][[1]]

:Begin:
:Function:      mPoleFO
:Pattern:       MassPoleFO[CoreName_String, nfIn_Integer, nfConv_Integer,
                           scale_?NumericQ, muA_?NumericQ, order_Integer]
:Arguments:     {CoreName, nfIn, nfConv, N[scale], N[muA], order}
:ArgumentTypes: {String, Integer, Integer, Real, Real, Integer}
:ReturnType:    Manual
:End:

:Begin:
:Function:      mPS
:Pattern:       MassPS[CoreName_String, nfIn_Integer, muF_?NumericQ,
                       nfConv_Integer:REvolver`Parameters`kDefault,
                       Optional[scale_?NumericQ, REvolver`Parameters`kDefault],
                       Optional[muA_?NumericQ, REvolver`Parameters`kDefault],
                       Optional[rIR_?NumericQ, 1],
                       order_Integer:REvolver`Parameters`kMaxOrderPs]
:Arguments:     {CoreName, nfIn, N[muF], nfConv, N[scale], N[muA], N[rIR],
                 order}
:ArgumentTypes: {String, Integer, Real, Integer, Real, Real, Real, Integer}
:ReturnType:    Manual
:End:

:Begin:
:Function:      m1S
:Pattern:       Mass1S[CoreName_String, nfIn_Integer,
                       nfConv_Integer:REvolver`Parameters`kDefault,
                       Optional[scale_?NumericQ, REvolver`Parameters`kDefault],
                       counting_String:"default",
                       Optional[muA_?NumericQ, REvolver`Parameters`kDefault],
                       order_Integer:REvolver`Parameters`kMaxOrder1s]
:Arguments:     {CoreName, nfIn, nfConv, N[scale], counting, N[muA], order}
:ArgumentTypes: {String, Integer, Integer, Real, String, Real, Integer}
:ReturnType:    Manual
:End:

:Begin:
:Function:      mBohr
:Pattern:       MBohr[CoreName_String, nfIn_Integer,
                Optional[nb_?NumericQ, 1.0]]
:Arguments:     {CoreName, nfIn, N[nb]}
:ArgumentTypes: {String, Integer, Real}
:ReturnType:    Manual
:End:

:Begin:
:Function:      mRGI
:Pattern:       MassRGI[CoreName_String, nfIn_Integer,
                        order_Integer:REvolver`Parameters`kMaxRunMSbar]
:Arguments:     {CoreName, nfIn, order}
:ArgumentTypes: {String, Integer, Integer}
:ReturnType:    Manual
:End:

:Begin:
:Function:      mRS
:Pattern:       MassRS[CoreName_String, nfIn_Integer, scaleRS_?NumericQ,
                       nfConv_Integer:REvolver`Parameters`kDefault,
                       Optional[scale_?NumericQ, REvolver`Parameters`kDefault],
                       Optional[muA_?NumericQ, REvolver`Parameters`kDefault],
                       order_Integer:REvolver`Parameters`kMaxRunMSR,
                       nRS_Integer:REvolver`Parameters`kMaxRunAlpha - 1,
                       Optional[N12_?NumericQ, REvolver`Parameters`kDefault]]
:Arguments:     {CoreName, nfIn, N[scaleRS], nfConv, N[scale], N[muA], order,
                 nRS, N[N12]}
:ArgumentTypes: {String, Integer, Real, Integer, Real, Real, Integer, Integer,
                 Real}
:ReturnType:    Manual
:End:

:Begin:
:Function:      mKin
:Pattern:       MassKin[CoreName_String, nfIn_Integer, scaleKin_?NumericQ,
                        nfConv_Integer:REvolver`Parameters`kDefault,
                        Optional[scale_?NumericQ, REvolver`Parameters`kDefault],
                        Optional[muA_?NumericQ, REvolver`Parameters`kDefault],
                        order_Integer:REvolver`Parameters`kMaxOrderKinetic]
:Arguments:     {CoreName, nfIn, N[scaleKin], nfConv, N[scale], N[muA], order}
:ArgumentTypes: {String, Integer, Real, Integer, Real, Real, Integer}
:ReturnType:    Manual
:End:

:Begin:
:Function:      N12
:Pattern:       N12[CoreName_String,
                    Optional[lambda_?NumericQ, REvolver`Parameters`kDefault]]
:Arguments:     {CoreName, N[lambda]}
:ArgumentTypes: {String, Real}
:ReturnType:    Manual
:End:

:Begin:
:Function:      P12
:Pattern:       P12[CoreName_String,
                    Optional[lambda_?NumericQ, REvolver`Parameters`kDefault]]
:Arguments:     {CoreName, N[lambda]}
:ArgumentTypes: {String, Real}
:ReturnType:    Manual
:End:

::------------------------------------------------------------------------------
:: Coupling related
::------------------------------------------------------------------------------

:Evaluate:      AlphaQCD[CoreName_String, mu_?NumericQ,
                         nf_Integer:REvolver`Parameters`kDefault] :=
                Which[
                  Element[mu, Reals]&&(mu > 0),
                  AlphaQCDRe[CoreName, mu, nf],
                  Element[mu, Complexes],
                  AlphaQCDCo[CoreName, Re[mu], Im[mu], nf],
                  Element[mu, Reals]&&(mu < 0),
                  AlphaQCDCo[CoreName, mu, 0.0, nf]
                ]

:Begin:
:Function:      aQCD
:Pattern:       AlphaQCDRe[CoreName_String, mu_?NumericQ, nf_Integer]
:Arguments:     {CoreName, N[mu], nf}
:ArgumentTypes: {String, Real, Integer}
:ReturnType:    Manual
:End:

:Begin:
:Function:      aQCD
:Pattern:       AlphaQCDCo[CoreName_String, muRe_?NumericQ, muIm_?NumericQ,
                           nf_Integer]
:Arguments:     {CoreName, N[muRe], N[muIm], nf}
:ArgumentTypes: {String, Real, Real, Integer}
:ReturnType:    Manual
:End:

:Begin:
:Function:      lQCD
:Pattern:       LambdaQCD[CoreName_String, nf_Integer,
                          convention_String:"MSbar"]
:Arguments:     {CoreName, nf, convention}
:ArgumentTypes: {String, Integer, String}
:ReturnType:    Manual
:End:

:Begin:
:Function:      betaCoefs
:Pattern:       BetaCoefs[CoreName_String]
:Arguments:     {CoreName}
:ArgumentTypes: {String}
:ReturnType:    Manual
:End:

::------------------------------------------------------------------------------
:: Core related
::------------------------------------------------------------------------------

:Evaluate:      Options[CoreCreate] =
                REvolver`Parameters`private`CoreOptionList;

:Begin:
:Function:      coreCreate
:Pattern:       CoreCreate[CoreName_String, nTot_Integer, alphaPar_List,
                           mPar_List, OptionsPattern[]]?CheckCreateOptions
:Arguments:     {CoreName, nTot, N[Flatten[alphaPar]], N[Flatten[mPar]],
                 N[Flatten[OptionValue[fMatch]]], OptionValue[runAlpha],
                 N[OptionValue[lambdaAlpha]], OptionValue[orderAlpha],
                 OptionValue[runMSbar], N[OptionValue[lambdaMSbar]],
                 OptionValue[orderMSbar], OptionValue[runMSR],
                 N[OptionValue[lambdaMSR]], OptionValue[orderMSR],
                 N[OptionValue[msBarDeltaError]], N[OptionValue[precisionGoal]]}
:ArgumentTypes: {String, Integer, RealList, RealList, RealList, Integer, Real,
                 Integer, Integer, Real, Integer, Integer, Real, Integer, Real,
                 Real}
:ReturnType:    Manual
:End:

:Begin:
:Function:      coreCreate
:Pattern:       CoreCreate[CoreName_String, alphaPar_List, beta_List:{},
                           OptionsPattern[]]?CheckCreateOptions
:Arguments:     {CoreName, N[Flatten[alphaPar]], N[Flatten[beta]],
                 OptionValue[runAlpha], N[OptionValue[lambdaAlpha]],
                 N[OptionValue[precisionGoal]]}
:ArgumentTypes: {String, RealList, RealList, Integer, Real, Real}
:ReturnType:    Manual
:End:

:Begin:
:Function:      coreDelete
:Pattern:       CoreDelete[CoreName_String]
:Arguments:     {CoreName}
:ArgumentTypes: {String}
:ReturnType:    Manual
:End:

:Evaluate:      CoreDelete[CoreList_List] := (CoreDelete /@ CoreList)[[1]];

:Begin:
:Function:      coreDeleteAll
:Pattern:       CoreDeleteAll[]
:Arguments:     {}
:ArgumentTypes: {}
:ReturnType:    Manual
:End:

:Begin:
:Function:      coreParams
:Pattern:       CoreParams[CoreName_String]
:Arguments:     {CoreName}
:ArgumentTypes: {String}
:ReturnType:    Manual
:End:

:Begin:
:Function:      coreParamsDetail
:Pattern:       CoreParamsDetail[CoreName_String]
:Arguments:     {CoreName, 0}
:ArgumentTypes: {String, Integer}
:ReturnType:    Manual
:End:

:Begin:
:Function:      coreParamsDetail
:Pattern:       CoreParamsDetailHelper[CoreName_String]
:Arguments:     {CoreName, 1}
:ArgumentTypes: {String, Integer}
:ReturnType:    Manual
:End:

:Evaluate:      CoreParamsDetail[CoreName_String, output_Symbol] :=
                  Block[{}, output = CoreParamsDetailHelper[CoreName];]

:Begin:
:Function:      listKeys
:Pattern:       CoreList[]
:Arguments:     {}
:ArgumentTypes: {}
:ReturnType:    Manual
:End:

::------------------------------------------------------------------------------
:: License
::------------------------------------------------------------------------------

:Evaluate:  REvolverLicense[] := Print["REvolver\n"
    <> "Automated Running and Matching of Couplings and Masses in QCD\n\n"
    <> "Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu\n\n"
    <> "This program is free software: you can redistribute it and/or modify\n"
    <> "it under the terms of the GNU General Public License as published by\n"
    <> "the Free Software Foundation, either version 3 of the License, or\n"
    <> "(at your option) any later version.\n\n"
    <> "This program is distributed in the hope that it will be useful,\n"
    <> "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    <> "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
    <> "GNU General Public License for more details.\n\n"
    <> "You should have received a copy of the GNU General Public License\n"
    <> "along with this program.  If not, see <https://www.gnu.org/licenses/>."]

:Evaluate:  End[]
:Evaluate:  EndPackage[]


::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: MQuantity Sub-package
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Evaluate:	BeginPackage["REvolver`MQuantity`"]

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Documentation
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Evaluate:  MQuantityCreate::usage =
  "MQuantityCreate[\"QuantityName\", nf, {\[Alpha], \[Mu]\[Alpha]}, {Q, \[Mu]Q}, "
    <> ", {gamma1, gamma2, ...}] "
    <> "creates an MQuantity object for a QCD MSbar-evolving quantity. - nf specifies"
    <> " the flavor number scheme of the given quantity and strong coupling "
    <> "constant. "
    <> "- {\[Alpha], \[Mu]\[Alpha]} specifies the strong coupling value \[Alpha] "
    <> "at the scale \[Mu]\[Alpha]. "
    <> "- {Q, \[Mu]Q} specifies the quantity value Q at the scale \[Mu]Q. "
    <> "- gamma1 specifies the first MSbar-anomalous "
    <> "dimension coefficient and so on. Additional option parameters: runAlpha, lambdaMSbar, "
    <> "lambdaAlpha, precisionGoal. Execute \"?<parameter>\" for more "
    <> "information, where "
    <> "<parameter> stands for any of the listed option parameters.\n"
  <> "MQuantityCreate[\"QuantityName\", nf, {\[Alpha], \[Mu]\[Alpha]}, {Q, \[Mu]Q}, "
    <> "{gamma1, gamma2, ...}, {beta0, beta1, ...} "
    <> "like above but with the option to set custom beta function "
    <> "coefficients. - beta0 specifies the first custom beta function "
    <> "coefficient and so on. The number of loops used "
    <> "for the evolution is deduced from the number of given coefficients. "
    <> "Additional option parameters like above."  
:Evaluate:  MQuantityDelete::usage =
  "MQuantityDelete[\"QuantityName\"] "
    <> "deletes the MQuantity with name QuantityName from memory.\n"
  <> "MQuantityDelete[{\"QuantityName1\", \"QuantityName2\", ...}] "
    <> "deletes multiple MQuantities."
:Evaluate:  MQuantityDeleteAll::usage =
  "MQuantityDeleteAll[] "
    <> "deletes all MQuantities from memory."
:Evaluate:  MQuantityList::usage =
  "MQuantityList[] "
    <> "lists the names of all defined MQuantity objects."
:Evaluate:  MQuantityRun::usage =
  "MQuantityRun[\"QuantityName\", mu] "
    <> "returns the value of the quantity with name \"QuantityName\" at the "
    <> "scale mu."

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Function links
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Evaluate:	Begin["`private`"]

::------------------------------------------------------------------------------
:: Option checking functions
::------------------------------------------------------------------------------

:Evaluate:      SetAttributes[CheckCreateOptions, HoldFirst];
:Evaluate:      CheckCreateOptions[call:func_[args__, opts:OptionsPattern[]]] :=
                ReleaseHold[
                  Hold[
                    IntegerQ[REvolver`Parameters`runAlpha] &&
                    NumericQ[REvolver`Parameters`lambdaAlpha] &&
                    NumericQ[REvolver`Parameters`lambdaMSbar] &&
                    NumericQ[REvolver`Parameters`precisionGoal]
                  ]/.{opts}/.Options[func]
                ]

::------------------------------------------------------------------------------
:: Linked functions
::------------------------------------------------------------------------------

:Evaluate:      Options[MQuantityCreate] =
                REvolver`Parameters`private`MQuantityOptionList;

:Begin:
:Function:      mQuantityCreate
:Pattern:       MQuantityCreate[QuantityName_String, nf_Integer, alphaPar_List,
                                qPar_List, gamma_List, beta_List:{},
                                OptionsPattern[]]?CheckCreateOptions
:Arguments:     {QuantityName, nf, N[Flatten[alphaPar]], N[Flatten[qPar]],
                 N[Flatten[gamma]], N[Flatten[beta]], OptionValue[runAlpha],
                 N[OptionValue[lambdaAlpha]], N[OptionValue[lambdaMSbar]],
                 N[OptionValue[precisionGoal]]}
:ArgumentTypes: {String, Integer, RealList, RealList, RealList,
                 RealList, Integer, Real, Real, Real}
:ReturnType:    Manual
:End:

:Begin:
:Function:      mQuantityDelete
:Pattern:       MQuantityDelete[QuantityName_String]
:Arguments:     {QuantityName}
:ArgumentTypes: {String}
:ReturnType:    Manual
:End:

:Evaluate:      MQuantityDelete[Quantities_List] :=
                (MQuantityDelete /@ Quantities)[[1]];

:Begin:
:Function:      mQuantityDeleteAll
:Pattern:       MQuantityDeleteAll[]
:Arguments:     {}
:ArgumentTypes: {}
:ReturnType:    Manual
:End:

:Begin:
:Function:      listKeysM
:Pattern:       MQuantityList[]
:Arguments:     {}
:ArgumentTypes: {}
:ReturnType:    Manual
:End:

:Begin:
:Function:      mQuantityRun
:Pattern:       MQuantityRun[QuantityName_String, mu_?NumericQ]
:Arguments:     {QuantityName, N[mu]}
:ArgumentTypes: {String, Real}
:ReturnType:    Manual
:End:

:Evaluate:  End[]
:Evaluate:  EndPackage[]

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: RQuantity Sub-package
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Evaluate:	BeginPackage["REvolver`RQuantity`"]

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Documentation
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Evaluate:  RQuantityCreate::usage =
  "RQuantityCreate[\"QuantityName\", nf, {\[Alpha], \[Mu]\[Alpha]}, {Q, \[Mu]Q}, "
    <> "pR, {gamma1, gamma2, ...}] "
    <> "creates a Quantity object for a QCD R-evolving quantity. - nf specifies"
    <> " the flavor number scheme of the given quantity and strong coupling "
    <> "constant. "
    <> "- {\[Alpha], \[Mu]\[Alpha]} specifies the strong coupling value \[Alpha] "
    <> "at the scale \[Mu]\[Alpha]. "
    <> "- {Q, \[Mu]Q} specifies the quantity value Q at the scale \[Mu]Q. "
    <> "- pR specifies the power of R in R-evolution."
    <> "- gamma1 specifies the first R-anomalous "
    <> "dimension coefficient and so on. Additional option parameters: runAlpha, lambdaR, "
    <> "lambdaAlpha, precisionGoal. Execute \"?<parameter>\" for more "
    <> "information, where "
    <> "<parameter> stands for any of the listed option parameters.\n"
  <> "RQuantityCreate[\"QuantityName\", nf, {\[Alpha], \[Mu]\[Alpha]}, {Q, \[Mu]Q}, "
    <> "pR, {gamma1, gamma2, ...}, {beta0, beta1, ...} "
    <> "like above but with the option to set custom beta function "
    <> "coefficients. - beta0 specifies the first custom beta function "
    <> "coefficient and so on. The number of loops used "
    <> "for the evolution is deduced from the number of given coefficients. "
    <> "Additional option parameters like above."  
:Evaluate:  RQuantityDelete::usage =
  "RQuantityDelete[\"QuantityName\"] "
    <> "deletes the Quantity with name QuantityName from memory.\n"
  <> "RQuantityDelete[{\"QuantityName1\", \"QuantityName2\", ...}] "
    <> "deletes multiple Quantities."
:Evaluate:  RQuantityDeleteAll::usage =
  "RQuantityDeleteAll[] "
    <> "deletes all Quantities from memory."
:Evaluate:  RQuantityList::usage =
  "RQuantityList[] "
    <> "lists the names of all defined Quantity objects."
:Evaluate:  RQuantityRun::usage =
  "RQuantityRun[\"QuantityName\", R] "
    <> "returns the value of the quantity with name \"QuantityName\" at the "
    <> "scale R."

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Function links
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Evaluate:	Begin["`private`"]

::------------------------------------------------------------------------------
:: Option checking functions
::------------------------------------------------------------------------------

:Evaluate:      SetAttributes[CheckCreateOptions, HoldFirst];
:Evaluate:      CheckCreateOptions[call:func_[args__, opts:OptionsPattern[]]] :=
                ReleaseHold[
                  Hold[
                    IntegerQ[REvolver`Parameters`runAlpha] &&
                    NumericQ[REvolver`Parameters`lambdaAlpha] &&
                    NumericQ[REvolver`Parameters`lambdaR] &&
                    NumericQ[REvolver`Parameters`precisionGoal]
                  ]/.{opts}/.Options[func]
                ]

::------------------------------------------------------------------------------
:: Linked functions
::------------------------------------------------------------------------------

:Evaluate:      Options[RQuantityCreate] =
                REvolver`Parameters`private`RQuantityOptionList;

:Begin:
:Function:      rQuantityCreate
:Pattern:       RQuantityCreate[QuantityName_String, nf_Integer, alphaPar_List,
                                qPar_List, pR_Integer, gamma_List, beta_List:{},
                                OptionsPattern[]]?CheckCreateOptions
:Arguments:     {QuantityName, nf, N[Flatten[alphaPar]], N[Flatten[qPar]], pR,
                 N[Flatten[gamma]], N[Flatten[beta]], OptionValue[runAlpha],
                 N[OptionValue[lambdaAlpha]], N[OptionValue[lambdaR]],
                 N[OptionValue[precisionGoal]]}
:ArgumentTypes: {String, Integer, RealList, RealList, Integer, RealList,
                 RealList, Integer, Real, Real, Real}
:ReturnType:    Manual
:End:

:Begin:
:Function:      rQuantityDelete
:Pattern:       RQuantityDelete[QuantityName_String]
:Arguments:     {QuantityName}
:ArgumentTypes: {String}
:ReturnType:    Manual
:End:

:Evaluate:      RQuantityDelete[Quantities_List] :=
                (RQuantityDelete /@ Quantities)[[1]];

:Begin:
:Function:      rQuantityDeleteAll
:Pattern:       RQuantityDeleteAll[]
:Arguments:     {}
:ArgumentTypes: {}
:ReturnType:    Manual
:End:

:Begin:
:Function:      listKeysR
:Pattern:       RQuantityList[]
:Arguments:     {}
:ArgumentTypes: {}
:ReturnType:    Manual
:End:

:Begin:
:Function:      rQuantityRun
:Pattern:       RQuantityRun[QuantityName_String, R_?NumericQ]
:Arguments:     {QuantityName, N[R]}
:ArgumentTypes: {String, Real}
:ReturnType:    Manual
:End:

:Evaluate:  End[]
:Evaluate:  EndPackage[]

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Optional parameters set by options patters and predefined values
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Evaluate:	BeginPackage["REvolver`Parameters`"]

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Documentation
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

::------------------------------------------------------------------------------
:: Constants
::------------------------------------------------------------------------------

:Evaluate:  mZdef::usage =
  "mZdef "
    <> "is the value of the Z boson mass fixed to 91.187 GeV when the package "
    <> "is loaded. Value can be reassigned."

:Evaluate:  amZdef::usage =
  "amZdef "
    <> "is the value of the 5-flavor strong coupling fixed to 0.1181 when the "
    <> "package is loaded. Value can be reassigned."

::------------------------------------------------------------------------------
:: Option parameters
::------------------------------------------------------------------------------

:Evaluate:  runAlpha::usage =
  "runAlpha "
    <> "is an option specifying the loop order used for \[Alpha] running. "
    <> "Default: highest available order which is 5."
:Evaluate:  lambdaAlpha::usage =
  "lambdaAlpha "
    <> "is an option specifying a scaling parameter probing the "
    <> "scale-dependence of the beta-function. Default: 1."
:Evaluate:  orderAlpha::usage =
  "orderAlpha "
    <> "is an option specifying the loop order used for \[Alpha] threshold "
    <> "matching. Default: highest available order which is 4."
:Evaluate:  runMSbar::usage =
  "runMSbar "
    <> "is an option specifying the loop order used for MSbar mass running. "
    <> "Default: highest available order which is 5."
:Evaluate:  lambdaMSbar::usage =
  "lambdaMSbar "
    <> "is an option specifying the scaling parameter probing the "
    <> "scale-dependence of the MSbar mass evolution. Default: 1."
:Evaluate:  orderMSbar::usage =
   "orderMSbar "
    <> "is an option parameter specifying the loop order used for the flavor "
    <> "threshold matching of the MSbar mass associated to thresholds related "
    <> "to respective heavier quarks. Default: highest available order "
    <> "which is 4."
:Evaluate:  runMSR::usage =
  "runMSR "
    <> "is an option parameter specifying the loop order used for MSR mass "
    <> "running. Default: highest available order which is 4."
:Evaluate:  lambdaMSR::usage =
  "lambdaMSR "
    <> "is an option parameter specifying the scaling parameter probing the "
    <> "scale-dependence of the MSR mass evolution. Default: 1."
:Evaluate:  orderMSR::usage =
  "orderMSR "
    <> "is an option parameter specifying the loop order used for the flavor "
    <> "threshold matching of the MSR masses associated to thresholds related "
    <> "to the respective massive quark itself and to respective lighter "
    <> "massive quarks. Default: highest available order which is 4."
:Evaluate:  msBarDeltaError::usage =
  "msBarDeltaError "
    <> "is an option parameter controlling the error of the 4-loop coefficient "
    <> "in the pole-MSbar relation. Should be varied between -1 and 1 for "
    <> "scanning the standard deviation. Default: 0.0."
:Evaluate:  lambdaR::usage =
  "lambdaR "
    <> "is an option parameter specifying the scaling parameter probing the "
    <> "scale-dependence of the R-evolution. Default: 1."
:Evaluate:  fMatch::usage =
  "fMatch "
    <> "is an option parameter containing elements {f1, f2, ...}, where fn "
    <> "specifies that the flavor matching scale for the n-th lightest massive "
    <> "quark threshold is fn times the standard running mass. Default: all "
    <> "entries set to 1."
:Evaluate:  fnQ::usage =
  "fnQ "
    <> "is an option parameter specifying the matching scale for the heavy "
    <> "quark threshold, analogous to the individual entries of fMatch."
:Evaluate:  precisionGoal::usage =
  "precisionGoal "
    <> "is the parameter setting the relative precision of all convergent "
    <> "infinite sums and iterative algorithms. The input value is clipped to "
    <> "the range [10^-6, 10^-15]. Default: 10^-15."

::------------------------------------------------------------------------------
:: Constants
::------------------------------------------------------------------------------

:Evaluate:  kDefault::usage =
  "kDefault "
    <> "is a predefined constant, internally set to -1, specifying that the "
    <> "respective default values should be used."
:Evaluate:  kMaxRunAlpha::usage =
  "kMaxRunAlpha "
    <> "is the maximal order of the strong coupling running, which is 5."
:Evaluate:  kMaxOrderAlpha::usage =
  "kMaxOrderAlpha "
    <> "is the maximal order of the strong coupling threshold matching, which "
    <> "is 4."
:Evaluate:  kMaxRunMSbar::usage =
  "kMaxRunMSbar "
    <> "is the maximal order of MSbar mass running, which is 5."
:Evaluate:  kMaxRunMSR::usage =
  "kMaxRunMSR "
    <> "is the maximal order of MSR mass running, which is 4."
:Evaluate:  kMaxOrderMSbar::usage =
  "kMaxOrderMSbar "
    <> "is the maximal order of threshold matching for MSbar masses, which is "
    <> "4."
:Evaluate:  kMaxOrderMSR::usage =
  "kMaxOrderMSR "
    <> "is the maximal order of threshold matching for MSR masses, which is 4."
:Evaluate:  kMaxOrderPs::usage =
  "kMaxOrderPs "
    <> "is the maximal order for PS scheme conversions, which is 4."
:Evaluate:  kMaxOrder1s::usage =
  "kMaxOrder1s "
    <> "is the maximal order for 1S scheme conversions, which is 4."
:Evaluate:  kMaxOrderKinetic::usage =
  "kMaxOrderKinetic "
    <> "is the maximal order for kinetic scheme conversions, which is 3."
:Evaluate:  kMaxPrec::usage =
  "kMaxPrec "
    <> "is the maximal value for precisionGoal, which is 10^-15."

::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
:: Parameter definition and protection
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

:Evaluate:	Begin["`private`"]

:Evaluate:  mZdef  = 91.187
:Evaluate:  amZdef = 0.1181

:Evaluate:  kDefault = -1;
:Evaluate:  kMaxRunAlpha = 5;
:Evaluate:  kMaxOrderAlpha = 4;
:Evaluate:  kMaxRunMSbar = 5;
:Evaluate:  kMaxRunMSR = 4;
:Evaluate:  kMaxOrderMSbar = 4;
:Evaluate:  kMaxOrderMSR = 4;
:Evaluate:  kMaxOrderPs = 4;
:Evaluate:  kMaxOrder1s = 4;
:Evaluate:  kMaxOrderKinetic = 3;
:Evaluate:  kMaxPrec = N[10^(-15)];

:Evaluate:  Protect[kDefault, kMaxRunAlpha, kMaxOrderAlpha, kMaxRunMSbar,
                    kMaxRunMSR, kMaxOrderMSbar, kMaxOrderMSR, kMaxOrderPs,
                    kMaxOrder1s, kMaxOrderKinetic, kMaxPrec];

:Evaluate:  Protect[fMatch, runAlpha, lambdaAlpha, orderAlpha, runMSbar,
                    lambdaMSbar, orderMSbar, runMSR, lambdaMSR, orderMSR,
                    msBarDeltaError, lambdaR, precisionGoal, fnQ];

:Evaluate:  CoreOptionList = {fMatch->{}, runAlpha->kMaxRunAlpha,
                              lambdaAlpha->1.0, orderAlpha->kMaxOrderAlpha,
                              runMSbar->kMaxRunMSbar, lambdaMSbar->1.0,
                              orderMSbar->kMaxOrderMSbar, runMSR->kMaxRunMSR,
                              lambdaMSR->1.0, orderMSR->kMaxOrderMSR,
                              msBarDeltaError->0.0, precisionGoal->kMaxPrec}

:Evaluate:  AddOptionList = {fnQ->1.0}

:Evaluate:  RQuantityOptionList = {runAlpha->kMaxRunAlpha, lambdaAlpha->1.0,
                                   lambdaR->1.0, precisionGoal->kMaxPrec}

:Evaluate:  MQuantityOptionList = {runAlpha->kMaxRunAlpha, lambdaAlpha->1.0,
                                   lambdaMSbar->1.0, precisionGoal->kMaxPrec}

:Evaluate:  End[]
:Evaluate:  EndPackage[]
