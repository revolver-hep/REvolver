////////////////////////////////////////////////////////////////////////////////
/// \file AnDim.cpp
/// \brief Implementation of class AnDim
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#define _USE_MATH_DEFINES

#include "AnDim.h"

#include <algorithm>
#include <cmath>
//#include <iostream>
#include <numeric>
#include <utility>

#include "Power.h"

using namespace std;
using namespace std::placeholders;

namespace revo {

//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
AnDim::AnDim(int nf, MScheme scheme, double precisionGoal)
    : _nf{nf},
      _lambdaAlpha{1.0},
      _lambdaMSbar{1.0},
      _lambdaR{1.0},
      _scheme{scheme},
      _sIsStrict{true},
      _betaIsStrict{true},
      _betaOrd{kMaxRunAlpha},
      _gammaROrd{kMaxRunMSR},
      _msBarDeltaError{0.0},
      _precisionGoal{constrain(precisionGoal, kMaxPrec, kMinPrec)},
      _beta{betaGen(_nf)},
      _gammaMass{massAndim(_nf)},
      _gammaMassLambda{gammaLambda(_lambdaMSbar, _gammaMass)},
      _msrnDelta{msBarDelta(_nf, 0)},
      _msBarDelta{msBarDelta(_nf - 1, 1)},
      _gammaRn{gammaRComputer(_beta, _msrnDelta)},
      _gammaRnLambda{_gammaRn},
      _andimM{nullptr} {
  _bCoef = _beta;
  multiplyAll(_bCoef, 1.0 / _beta[0]);
}

//------------------------------------------------------------------------------
// Public member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void AnDim::setAlphaProp(int betaOrd, bool betaIsStrict) {
  applyConstrain(betaOrd, 1, int(_beta.size()));

  if (_betaOrd != betaOrd || _betaIsStrict != betaIsStrict) {
    _betaOrd = betaOrd;
    _betaIsStrict = betaIsStrict;
  }
  _andimM1.clear();
}

//------------------------------------------------------------------------------
void AnDim::setLambdaAlpha(double lambda) {
  _lambdaAlpha = lambda;
  _coefMap.clear();
  _sMap.clear();
}

//------------------------------------------------------------------------------
void AnDim::setLambdaMSbar(double lambda) {
  _lambdaMSbar = lambda;
  _gammaMassLambda = gammaLambda(lambda, _gammaMass);
}

//------------------------------------------------------------------------------
void AnDim::setLambdaR(double lambda) {
  _lambdaR = lambda;
  _gammaRnLambda = gammaLambda(lambda, _gammaRn);
  multiplyAll(_gammaRnLambda, 1.0 / _lambdaR);
  _gammaRpLambda.clear();
  _sMap.clear();
}

//------------------------------------------------------------------------------
void AnDim::setMsrProp(int gammaROrd, bool sIsStrict) {
  applyConstrain(gammaROrd, 1, int(_gammaRn.size()));

  if (_gammaROrd != gammaROrd || _sIsStrict != sIsStrict) {
    _gammaROrd = gammaROrd;
    _sIsStrict = sIsStrict;
  }
  _andimM1.clear();
}

//------------------------------------------------------------------------------
void AnDim::setMsBarDeltaError(double msBarDeltaError) {
  _msBarDeltaError = msBarDeltaError;

  _msrnDelta = msBarDelta(_nf, 0, _msBarDeltaError);
  _msBarDelta = msBarDelta(_nf - 1, 1, _msBarDeltaError);
  _gammaRn = gammaRComputer(_beta, _msrnDelta);
  setLambdaR(_lambdaR);
  _alphaMatch.clear();
  _alphaMatchInv.clear();
  _gammaRp.clear();
  _gammaRpLambda.clear();
  _msrpDelta.clear();
  _andimM1.clear();
  _sMap.clear();
  _alphaMatchLog.clear();
  _alphaMatchInvLog.clear();
}

//------------------------------------------------------------------------------
void AnDim::setBeta(const doubleV& beta) {
  _beta = beta;
  _betaOrd = _beta.size();
  _bCoef = _beta;
  multiplyAll(_bCoef, 1.0 / _beta[0]);
  _andimM1.clear();
  _coefMap.clear();
}

//------------------------------------------------------------------------------
void AnDim::setGammaR(const doubleV& gamma) {
  _gammaRn = gamma;
  _gammaROrd = _gammaRn.size();
  _gammaRnLambda = gammaLambda(_lambdaR, _gammaRn);
  multiplyAll(_gammaRnLambda, 1.0 / _lambdaR);
  _gammaRp.clear();
  _gammaRpLambda.clear();
  _sMap.clear();
}

//------------------------------------------------------------------------------
void AnDim::setGammaMass(const doubleV& gamma) {
  _gammaMass = gamma;
  _gammaMassLambda = gammaLambda(_lambdaMSbar, _gammaMass);
}

//------------------------------------------------------------------------------
const doubleV& AnDim::betaCoef() const { return _beta; }

//------------------------------------------------------------------------------
double AnDim::precisionGoal() const { return _precisionGoal; }

//------------------------------------------------------------------------------
void AnDim::provideAndimMap(std::unordered_map<int, AnDim>* andimM) {
  _andimM = andimM;
}

//------------------------------------------------------------------------------
double AnDim::Np2(int p, int betaOrd, int gammaROrd,
                  const doubleV* sCoe) const {
  if (betaOrd == kDefault) {
    betaOrd = _beta.size();
  }
  if (gammaROrd == kDefault) {
    gammaROrd = _gammaRn.size();
  }
  if (sCoe && (gammaROrd > int(sCoe->size()))) {
    gammaROrd = sCoe->size();
  }

  double pFrac = pow(abs(double(p)), p * bHat(1, betaOrd, false) - 1);
  double sum = 0.0;
  double pPow = 1.0;
  rac::Pochhammer poch(1 + p * bHat(1, betaOrd, false));
  if (sCoe) {
    for (int k = 0; k < gammaROrd; ++k) {
      pPow *= p;
      sum += (*sCoe)[k] * pPow / poch[k];
    }
  } else {
    for (int k = 0; k < gammaROrd; ++k) {
      pPow *= p;
      sum += sCoef(k, betaOrd, gammaROrd, MScheme::MSRn, false, p) * pPow /
             poch[k];
    }
  }

  return _beta[0] / 2 / M_PI * pFrac * sum;
}

//------------------------------------------------------------------------------
double AnDim::Pp2(int p, int betaOrd, int gammaROrd,
                  const doubleV* sCoe) const {
  if (betaOrd == kDefault) {
    betaOrd = _beta.size();
  }
  return 2 * M_PI / _beta[0] / tgamma(1.0 + p * bHat(1, betaOrd, false)) *
         Np2(p, betaOrd, gammaROrd, sCoe);
}

//------------------------------------------------------------------------------
// Private Struct member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
AnDim::DynCoefs::DynCoefs()
    : _bHat{1.0},
      _cCoef{1.0},
      _bHatLambda{1.0},
      _cCoefLambda{1.0},
      _m1pow{1},
      _m1powLambda{1},
      _beta02pow{1.0},
      _beta02powLambda{1.0} {}

//------------------------------------------------------------------------------
// Private member functions - related to mutable data members
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
const doubleV& AnDim::alphaMatch(AlphaMatchCoefType type) const {
  switch (type) {
    case AlphaMatchCoefType::Inv:
      return _alphaMatchInv.empty()
                 ? (_alphaMatchInv = getInverseMatch(
                        alphaMatch(AlphaMatchCoefType::Basic), 5))
                 : _alphaMatchInv;
      break;

    case AlphaMatchCoefType::Basic:
    default:
      return _alphaMatch.empty() ? (_alphaMatch = alphaMatch(_nf))
                                 : _alphaMatch;
      break;
  }
}

//------------------------------------------------------------------------------
const doubleG& AnDim::alphaMatchLog(AlphaMatchCoefType type) const {
  switch (type) {
    case AlphaMatchCoefType::InvLog:
      return _alphaMatchInvLog.empty()
                 ? (_alphaMatchInvLog = alphaMatchingLog(
                        alphaMatch(AlphaMatchCoefType::Inv), _nf - 1, _nf))
                 : _alphaMatchInvLog;
      break;

    case AlphaMatchCoefType::Log:
    default:
      return _alphaMatchLog.empty()
                 ? (_alphaMatchLog = alphaMatchingLog(
                        alphaMatch(AlphaMatchCoefType::Basic), _nf, _nf - 1))
                 : _alphaMatchLog;
      break;
  }
}

//------------------------------------------------------------------------------
const doubleV& AnDim::msrpDelta() const {
  if (_msrpDelta.empty()) {
    _msrpDelta = msBarDelta(_nf, 1, _msBarDeltaError);
    doubleV b = getInverseMatch(alphaMatch(_nf + 1), 4);
    seriesReExpand(b, _msrpDelta);
  }

  return _msrpDelta;
}

//------------------------------------------------------------------------------
const doubleV& AnDim::gammaRp() const {
  return _gammaRp.empty() ? (_gammaRp = gammaRComputer(_beta, msrpDelta()))
                          : _gammaRp;
}

//------------------------------------------------------------------------------
const doubleV& AnDim::gammaRpLambda() const {
  if (_gammaRpLambda.empty()) {
    _gammaRpLambda = gammaLambda(_lambdaR, gammaRp());
    multiplyAll(_gammaRpLambda, 1.0 / _lambdaR);
  }
  return _gammaRpLambda;
}

//------------------------------------------------------------------------------
// Private member functions - coupling related
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
doubleG AnDim::alphaMatchingLog(const doubleV& d, int nfIn, int nfOut) {
  if (int(d.size()) != 5) {
    error(ErrorType::Internal);
    return doubleG();
  }
  double ePow[5][5][5] = {};
  int jj, kk;
  doubleG tab(5, 5);

  tab[0][0] = 1.0;

  doubleV betnfIn = betaGen(nfIn);
  doubleV betnfOut = betaGen(nfOut);

  doubleG bb = expandAlpha(betnfIn, d);
  doubleV ccPre(5);
  ccPre[0] = 1.0;
  doubleG cc = expandAlpha(betnfOut, ccPre);

  for (int n = 2; n < 6; ++n) {
    for (int i = 0; i < 4; ++i) {
      ePow[i][n - 2][0] = tab[i][n - 2];
    }

    for (int i = 2; i <= n; ++i) {
      for (int l = 0; l <= n - i; ++l) {
        for (int m = 1; m <= n + 1 - i; ++m) {
          jj = max(0, l + i - 1 + m - n);
          kk = min(l, m - 1);
          for (int j = jj; j <= kk; ++j) {
            ePow[l][n - 1][i - 1] +=
                tab[j][m - 1] * ePow[l - j][n - m - 1][i - 2];
          }
        }
      }
    }

    for (int l = 0; l < n; ++l) {
      tab[l][n - 1] = bb[l][n - 1];
      for (int i = 2; i <= n; ++i) {
        jj = max(0, l + i - n);
        kk = min(l, i - 1);
        for (int j = jj; j <= kk; ++j) {
          tab[l][n - 1] -= cc[j][i - 1] * ePow[l - j][n - 1][i - 1];
        }
      }
    }
  }

  return tab;
}

//------------------------------------------------------------------------------
doubleG AnDim::expandAlpha(const doubleV& bet, const doubleV& coef) {
  int betOrder = bet.size();
  int m = coef.size();
  doubleG coefOut(m, m);

  copy(coef.begin(), coef.end(), coefOut.rowBegin(0));

  for (int n = 1; n < m; ++n) {
    for (int j = 1; j <= n; ++j) {
      for (int i = j; i <= n; ++i) {
        coefOut[j][n] += i * seriesExtend(bet, betOrder, n - i) *
                         coefOut[j - 1][i - 1] / kPow4[n + 1 - i];
      }
      coefOut[j][n] *= 2.0 / j;
    }
  }

  return coefOut;
}

//------------------------------------------------------------------------------
doubleV AnDim::betaGen(int nf) {
  doubleV coef(5);
  int nf2 = nf * nf, nf3 = nf2 * nf, nf4 = nf3 * nf;

  coef[0] = 11.0 - (2.0 * nf) / 3.0;
  coef[1] = 102.0 - (38.0 * nf) / 3.0;
  coef[2] = 2857.0 / 2.0 - (5033.0 * nf) / 18.0 + (325.0 * nf2) / 54.0;
  coef[3] = 29242.964136194132 - 6946.289617003555 * nf +
            405.08904045986293 * nf2 + 1.4993141289437586 * nf3;
  coef[4] = 537147.6740702358 - 186161.94951432804 * nf +
            17567.757653436838 * nf2 - 231.27767265113647 * nf3 -
            1.8424744081239024 * nf4;

  return coef;
}

//------------------------------------------------------------------------------
doubleV AnDim::betaLambda(int betaOrd, double lambda) const {
  doubleV betaL(betaOrd + 1);
  transform(_beta.begin(), _beta.begin() + betaOrd, kPow4.begin() + 1,
            betaL.begin() + 1, divides<double>());
  expandAlpha(1.0, lambda, betaL);
  betaL.push_back(0.0);
  expandAlpha(lambda, 1.0, betaL);
  transform(betaL.begin() + 1, betaL.end(), kPow4.begin() + 1, betaL.begin(),
            multiplies<double>());
  betaL.pop_back();
  return betaL;
}

//------------------------------------------------------------------------------
doubleV AnDim::gammaLambda(double lambda, const doubleV& gamma) const {
  doubleV gammaL(gamma);
  transform(gammaL.begin(), gammaL.end(), kPow4.begin() + 1, gammaL.begin(),
            divides<double>());
  expandAlpha(1.0, lambda, gammaL);
  transform(gammaL.begin(), gammaL.end(), kPow4.begin() + 1, gammaL.begin(),
            multiplies<double>());
  return gammaL;
}

//------------------------------------------------------------------------------
doubleV AnDim::getInverseMatch(const doubleV& c, int n) {
  if (int(c.size()) < n || n <= 0) {
    error(ErrorType::Internal);
    return doubleV();
  }
  doubleV out(n);
  doubleG e(n, n + 1);

  out[0] = 1.0;

  e[0][0] = 1.0;

  for (int i = 1; i < n; ++i) {
    for (int j = i; j <= n; ++j) {
      for (int k = 0; k <= j - i; ++k) {
        e[i][j] += c[k] * e[i - 1][j - 1 - k];
      }
    }

    for (int k = 0; k < i; ++k) {
      out[i] -= out[k] * e[k + 1][i + 1];
    }
  }

  return out;
}

//------------------------------------------------------------------------------
doubleV AnDim::alphaMatch(int nf, MScheme scheme) const {
  double d[5][5] = {};
  doubleV tab(5);
  doubleV tab2(5);

  if (scheme == MScheme::Default) {
    scheme = _scheme;
  }

  tab2[0] = 1.0;
  tab2[1] = 0.0;
  tab2[2] = -7.0 / 24.0;
  tab2[3] = -5.586361025786356 + 0.26247081195432964 * nf;
  tab2[4] = -95.80685705811597 + 10.171370332526523 * nf -
            0.23954195789610408 * nf * nf;

  if (scheme == MScheme::MSbar) {
    doubleV b = msBarDelta(nf - 1, 1, _msBarDeltaError);
    d[0][0] = 1.0;

    for (int i = 1; i < 5; ++i) {
      for (int j = 1; j < i; ++j) {
        d[1][i] += j * d[1][j] * b[i - j - 1];
      }
      d[1][i] = b[i - 1] - d[1][i] / i;
    }

    doubleG c = alphaMatchingLog(tab2, nf, nf - 1);

    for (int i = 1; i < 4; ++i) {
      for (int j = i; j < 4; ++j) {
        for (int k = 1; k <= j - i; ++k) {
          d[i + 1][j] += d[1][k] * d[i][j - k];
        }
      }
    }

    for (int k = 1; k < 6; ++k) {
      for (int i = 1; i <= k; ++i) {
        int m1pow = -1;
        for (int j = 0; j <= min(i - 1, k - i); ++j) {
          m1pow *= -1;
          tab[k - 1] += m1pow * c[j][i - 1] * d[j][k - i];
        }
      }
    }

  } else if (scheme == MScheme::Pole) {
    tab.swap(tab2);
  }

  return tab;
}

//------------------------------------------------------------------------------
void AnDim::setNextbHatc(int betaOrd) const {
  DynCoefs& coe = _coefMap[betaOrd];
  int nextCoe = coe._bHat.size();
  double cCoe = 0.0;

  coe._m1pow *= -1;
  coe._beta02pow *= 2.0 * _beta[0];

  for (int j = 0; j < nextCoe; ++j) {
    cCoe -=
        cCoef(j, betaOrd, true) * seriesExtend(_bCoef, betaOrd, nextCoe - j);
  }

  coe._cCoef.push_back(cCoe);
  coe._bHat.push_back(coe._m1pow * cCoe / coe._beta02pow);
}

//------------------------------------------------------------------------------
void AnDim::setNextbHatcLambda(int betaOrd) const {
  DynCoefs& coe = _coefMap[betaOrd];
  int nextCoe = coe._bHatLambda.size();
  double cCoe = 0.0;
  if (coe._betaLambda.empty()) {
    coe._betaLambda = betaLambda(betaOrd, _lambdaAlpha);
    for (auto b : coe._betaLambda) {
      coe._bCoefLambda.push_back(b / coe._betaLambda[0]);
    }
  }

  coe._m1powLambda *= -1;
  coe._beta02powLambda *= 2.0 * coe._betaLambda[0];

  for (int j = 0; j < nextCoe; ++j) {
    cCoe -=
        cCoefLambda(j, betaOrd, true) *
        seriesExtend(coe._bCoefLambda, coe._bCoefLambda.size(), nextCoe - j);
  }

  coe._cCoefLambda.push_back(cCoe);
  coe._bHatLambda.push_back(coe._m1powLambda * cCoe / coe._beta02powLambda);
}

//------------------------------------------------------------------------------
void AnDim::setNextgl(int betaOrd, int p) const {
  setNextglGeneric(_coefMap[betaOrd]._gl[p], p,
                   bind(&AnDim::bHat, this, _1, betaOrd, true));
}

//------------------------------------------------------------------------------
void AnDim::setNextglLambda(int betaOrd, int p) const {
  setNextglGeneric(_coefMap[betaOrd]._glLambda[p], p,
                   bind(&AnDim::bHatLambda, this, _1, betaOrd, true));
}

//------------------------------------------------------------------------------
doubleG AnDim::matchingAlphaLog(Direction dir) const {
  switch (dir) {
    case Direction::Down:
      return alphaMatchLog(AlphaMatchCoefType::Log);
      break;

    case Direction::Up:
      return alphaMatchLog(AlphaMatchCoefType::InvLog);
      break;

    default:
      error(ErrorType::Keyword);
      return doubleG(5, 5);
      break;
  }
}

//------------------------------------------------------------------------------
void AnDim::expandAlpha(double muIn, double muOut, doubleV& coef,
                        int betaOrd) const {
  if (betaOrd == kDefault || betaOrd > int(_beta.size())) {
    betaOrd = _beta.size();
  } else if (betaOrd == 0) {
    return;
  }
  double lg = log(muOut / muIn), lgList = 1.0;

  doubleG coefTab =
      expandAlpha(doubleV(_beta.begin(), _beta.begin() + betaOrd), coef);
  int m = coef.size();
  for (int j = 1; j < m; ++j) {
    lgList *= lg;
    for (int i = j - 1; i < m; ++i) {
      coef[i] += coefTab[j][i] * lgList;
    }
  }
}

//------------------------------------------------------------------------------
doubleV AnDim::thresholdMatching(Direction dir, int order, double lg) const {
  doubleV out = applyLogGrid(matchingAlphaLog(dir), lg);
  out.resize(order + 1);
  return out;
}

//------------------------------------------------------------------------------
doubleV& AnDim::matchSeries(doubleV& series, Direction dir, double log) const {
  if (abs(log) <= kMaxPrec) {
    switch (dir) {
      case Direction::Up:
        seriesReExpand(alphaMatch(AlphaMatchCoefType::Basic), series);
        break;

      case Direction::Down:
      default:
        seriesReExpand(alphaMatch(AlphaMatchCoefType::Inv), series);
        break;
    }
  } else {
    seriesReExpand(thresholdMatching(-dir, kMaxOrderAlpha, log), series);
  }
  return series;
}

//------------------------------------------------------------------------------
double AnDim::gFun(double al, int order) const {
  double t = -2 * M_PI / (al * _beta[0]), res = t, tPow = 1.0;

  int maxIter = (order == kDefault) ? kMaxIter : order;
  if (_betaOrd > 1 && (order == kDefault || order > 1)) {
    res += bHatLambda(1, _betaOrd, _betaIsStrict) * log(-t);
    for (int i = 1; i < maxIter; ++i) {
      tPow *= 1 / t;
      double corr = -bHatLambda(i + 1, _betaOrd, _betaIsStrict) / i * tPow;
      res += corr;
      if (abs(corr / res) < _precisionGoal) {
        break;
      } else if (i == kMaxIter - 1) {
        res = std::nan("");
        break;
      }
    }
  }

  return exp(res);
}

//------------------------------------------------------------------------------
int AnDim::buildSKey(int betaOrd, int gammaROrd, MScheme sType) {
  return pow(2, betaOrd) * pow(3, gammaROrd) *
         ((sType == MScheme::MSRn) ? 1 : 5);
}

//------------------------------------------------------------------------------
// Private mamber functions - mass related
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
double AnDim::statConstC1(double c2, double d2, double A) {
  return log(A / d2) / log(c2 / d2);
}

//------------------------------------------------------------------------------
map<AnDim::StPotType, double> AnDim::statConst() {
  map<StPotType, double> outMap;
  outMap[StPotType::c2] = 0.470;
  outMap[StPotType::d2] = 1.120;
  outMap[StPotType::A] = 2.3058587268749497;
  outMap[StPotType::c1] = statConstC1(
      outMap[StPotType::c2], outMap[StPotType::d2], outMap[StPotType::A]);
  outMap[StPotType::d1] = 1 - outMap[StPotType::c1];
  return outMap;
}

//------------------------------------------------------------------------------
doubleV AnDim::massAndim(int nf) {
  doubleV coef(5);
  int nf2 = nf * nf, nf3 = nf2 * nf, nf4 = nf3 * nf;

  coef[0] = -4.0;
  coef[1] = -16.0 * (101.0 / 24.0 - 5.0 * nf / 36.0);
  coef[2] =
      -64.0 * (1249.0 / 64.0 - 2.284121493373736 * nf - 35.0 / 1296.0 * nf2);
  coef[3] = -256.0 * (98.9434142552029 - 19.1074619186354 * nf +
                      0.27616255142989465 * nf2 + 0.005793222354358382 * nf3);
  coef[4] = -573139.8612330721 + 147134.94237385172 * nf -
            7661.955304616065 * nf2 - 110.91796496576201 * nf3 +
            0.08740751602244723 * nf4;

  return coef;
}

//------------------------------------------------------------------------------
doubleV AnDim::msBarDelta(int nl, int nh, double error4) {
  doubleV coef(4);
  int nh2 = nh * nh, nh3 = nh2 * nh, nl2 = nl * nl, nl3 = nl2 * nl;

  coef[0] = 4.0 / 3.0;
  coef[1] =
      13.33982910125516 + 0.10356715567659536 * nh - 1.041366911171631 * nl;
  coef[2] = 188.67172035165487 + 1.8591544419385237 * nh +
            0.06408045019609998 * nh2 - 26.677375174269212 * nl +
            0.022243482163948114 * nh * nl + 0.6526907490815437 * nl2;
  coef[3] = 3560.8519915203624 + 6.9580647832865985 * nh -
            0.23497888797223992 * nh2 + 0.07451 / 3.0 * nh3 -
            744.8508175070676 * nl - 0.9031405141668717 * nh * nl +
            0.108515 / 3.0 * nh2 * nl + 43.379048031381515 * nl2 +
            0.01720208660410344 * nh * nl2 - 0.6781410256045151 * nl3;

  if (abs(error4) > kMaxPrec) {
    coef[3] += error4 * sqrt(2.6569 + 0.0144 * nh2 + 7.29e-6 * nh * nh3 +
                             1.6e-7 * nh2 * nl2 + 0.0016 * nl2);
  }

  return coef;
}

//------------------------------------------------------------------------------
doubleV AnDim::psDelta(int nl, double rIR) {
  doubleV coef(4);
  int nl2 = nl * nl, nl3 = nl2 * nl;
  double irLg = log(rIR);

  coef[0] = 4.0 / 3.0;
  coef[1] = 10.777777777777779 - 0.8148148148148148 * nl;
  coef[2] =
      173.61795863880744 - 23.788773551477437 * nl + 0.6460905349794238 * nl2;
  coef[3] = 3567.7220249950237 - 701.2302862924382 * nl +
            41.15507811732836 * nl2 - 0.67466849565615 * nl3 +
            88.82643960980421 * irLg;

  return coef;
}

//------------------------------------------------------------------------------
doubleV AnDim::rsDelta(int nRS, double N12var) const {
  doubleV coef(4);
  applyConstrain(nRS, 0, kMaxRunAlpha - 1);
  if (isDefault(N12var)) {
    N12var = Np2(1, kMaxRunAlpha, nRS);
  }
  double bHat1P1 = 1 + bHat(1, kMaxRunAlpha, false);
  double fac = 2 * M_PI / _beta[0] / tgamma(bHat1P1) * N12var;
  doubleP beta02Pow(_beta[0] / 2);
  for (int n = 1; n <= 4; ++n) {
    for (int l = 0; l < nRS; ++l) {
      coef[n - 1] += tgamma(bHat1P1 + n - 1 - l) * gl(l, kMaxRunAlpha, false);
    }
    coef[n - 1] *= fac * beta02Pow[n];
  }

  return coef;
}

//------------------------------------------------------------------------------
doubleV AnDim::oneSDelta(int nl, double alpha) {
  doubleV coef(4);
  int nl2 = nl * nl;

  coef[0] = -0.5235987755982988;
  coef[1] = -8.4648468721725 + 0.6399540590645875 * nl;
  coef[2] =
      -176.9487003084162 + 21.449147596287634 * nl - 0.6088705909798264 * nl2;
  coef[3] = -3926.235649953501 + 690.2901220202826 * nl -
            39.40761669890877 * nl2 + 0.6584853691876519 * nl2 * nl -
            248.33730859684576 * log(alpha);

  return coef;
}

//------------------------------------------------------------------------------
void AnDim::kinDelta(int nl, doubleV& linear, doubleV& quadratic) {
  int nl2 = nl * nl;
  linear.resize(3);
  quadratic.resize(3);

  linear[0] = -1.7777777777777778;
  quadratic[0] = -0.6666666666666667;
  linear[1] = -23.078870161994644 + 1.5802469135802468 * nl;
  quadratic[1] = -6.8212429774146575 + 0.48148148148148145 * nl;
  linear[2] =
      -362.1430910903762 + 50.92088541456441 * nl - 1.4473655615553573 * nl2;
  quadratic[2] =
      -73.87015611644378 + 11.201214000953067 * nl - 0.30819418434869106 * nl2;

  return;
}

//------------------------------------------------------------------------------
double AnDim::msrnDeltaAsy(int n, int betaOrd, int gammaROrd) const {
  if (n < 0) {
    return 0.0;
  }

  if (betaOrd < 1) {
    error(ErrorType::Internal);
    return std::nan("");
  } else if (betaOrd > kMaxRunAlpha) {
    betaOrd = kMaxRunAlpha;
  }
  if (gammaROrd < 1) {
    error(ErrorType::Internal);
    return std::nan("");
  } else if (gammaROrd > kMaxRunMSR) {
    gammaROrd = kMaxRunMSR;
  }

  int key = buildSKey(betaOrd, gammaROrd, MScheme::MSRn);

  while (unsigned(n) >= _sMap[key]._msrDeltaAsy.size()) {
    setNextMSRDeltaCoef(MScheme::MSRn, betaOrd, gammaROrd);
  }
  return _sMap[key]._msrDeltaAsy[n];
}

//------------------------------------------------------------------------------
double AnDim::msrpDeltaAsy(int n, int betaOrd, int gammaROrd) const {
  if (n < 0) {
    return 0.0;
  }

  if (betaOrd < 1) {
    error(ErrorType::Internal);
    return std::nan("");
  } else if (betaOrd > kMaxRunAlpha) {
    betaOrd = kMaxRunAlpha;
  }
  if (gammaROrd < 1) {
    error(ErrorType::Internal);
    return std::nan("");
  } else if (gammaROrd > kMaxRunMSR) {
    gammaROrd = kMaxRunMSR;
  }

  int key = buildSKey(betaOrd, gammaROrd, MScheme::MSRp);

  while (unsigned(n) >= _sMap[key]._msrDeltaAsy.size()) {
    setNextMSRDeltaCoef(MScheme::MSRp, betaOrd, gammaROrd);
  }
  return _sMap[key]._msrDeltaAsy[n];
}

//------------------------------------------------------------------------------
double AnDim::msBarDeltaAsy(int n, int betaOrd, int gammaROrd) const {
  if (n < 0) {
    return 0.0;
  }

  if (betaOrd < 1) {
    error(ErrorType::Internal);
    return std::nan("");
  } else if (betaOrd > kMaxRunAlpha) {
    betaOrd = kMaxRunAlpha;
  }
  if (gammaROrd < 1) {
    error(ErrorType::Internal);
    return std::nan("");
  } else if (gammaROrd > kMaxRunMSR) {
    gammaROrd = kMaxRunMSR;
  }

  int key = buildSKey(betaOrd, gammaROrd, MScheme::MSRp);

  while (unsigned(n) >= _sMap[key]._msBarDeltaAsy.size()) {
    setNextMSBarDeltaCoef(betaOrd, gammaROrd);
  }
  return _sMap[key]._msBarDeltaAsy[n];
}

//------------------------------------------------------------------------------
doubleV AnDim::gammaRComputer(const doubleV& beta, const doubleV& delta) {
  int n = delta.size();
  doubleV gammaR(n);
  transform(delta.begin(), delta.end(), kPow4.begin() + 1, gammaR.begin(),
            multiplies<double>());

  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < i; ++j) {
      gammaR[i] -= 2 * kPow4[j + 1] * (j + 1) * delta[j] *
                   seriesExtend(beta, beta.size(), i - 1 - j);
    }
  }

  return gammaR;
}

//------------------------------------------------------------------------------
double AnDim::msLightCorr3LG(double r) {
  if (r <= 0) {
    error(ErrorType::Internal);
    return nan("");
  }

  double lr = log(r), res;
  doubleP rPow(r);

  res = r *
            (146.55437276141436 - 68.5502265986958 * r +
             72.20843181777957 * rPow[2] + 44.70786258835728 * rPow[3] +
             17.872638638242837 * rPow[4] - 0.6287605934381015 * rPow[5] +
             1.1859159626883737 * rPow[6] + 0.673242497280189 * rPow[7] +
             1.9017595879357783 * rPow[8]) /
            (6.5849022304691935 + r) +
        lr * r *
            (-18.056564104234784 +
             lr * rPow[3] *
                 (-1.2733398103055087 + 0.4074074503956954 * lr -
                  0.852006762408542 * rPow[2] - 0.30539079178379064 * rPow[4]) +
             r * (-10.686984081790198 +
                  r * (-18.66917624034727 +
                       r * (-0.2506821619225726 +
                            r * (-2.8512190595320313 +
                                 r * (0.5518674634880775 +
                                      (-0.5451781523380799 -
                                       0.09796100275651486 * r) *
                                          r))))));

  return res;
}

//------------------------------------------------------------------------------
double AnDim::msLightCorr3LNl(double r) {
  if (r <= 0) {
    error(ErrorType::Internal);
    return nan("");
  }

  double lr = log(r), lr2 = lr * lr, res;
  doubleP rPow(r);

  res = -0.0025170068027210884 * lr * r *
            (-435.6852393273681 - 435.6852393273681 * rPow[2] -
             112.12030500101459 * rPow[3] - 95.64564564564564 * lr * rPow[3] +
             29.42942942942943 * lr2 * rPow[3] + 10.594594594594595 * rPow[5] +
             rPow[7]) +
        (r * (-1.5169370316230057 - 0.7139583194060264 * r +
              0.5740149072765712 * rPow[2] - 0.28690147708463154 * rPow[3] -
              1.0821107015842863 * rPow[4] - 0.2198718445207236 * rPow[5] +
              0.09197784283922195 * rPow[6] - 0.01027610419769106 * rPow[7] +
              0.006725122803730151 * rPow[8])) /
            (1.4606375667533493 + r);

  return res;
}

//------------------------------------------------------------------------------
double AnDim::msLightCorr3LNh(double r) {
  if (r <= 0) {
    error(ErrorType::Internal);
    return nan("");
  }

  double lr = log(r);
  doubleP rPow(r);

  return rPow[2] *
         ((0.327412590226266 - 0.6530803188815482 * r +
           0.4620726956320225 * rPow[2] - 0.11513821076485932 * rPow[3] +
           0.09025690005091372 * rPow[4] - 0.061259252872525434 * rPow[5] +
           0.001927757595523288 * rPow[6] - 0.0020551485377330423 * rPow[7]) /
              (1.4733594652016009 - r) +
          lr * (3.5253490810187646e-7 - 0.1741826695338979906286 * rPow[2] -
                0.05629628654108489566051 * rPow[4] -
                0.00120748327054723502409 * rPow[6] +
                lr * rPow[4] *
                    (0.02962963801364881794712 +
                     0.007142868560795129282328 * rPow[2])));
}

//------------------------------------------------------------------------------
double AnDim::msLightCorr2L(double r) {
  if (r <= 0.0 || r > 1.0) {
    error(ErrorType::Internal);
    return nan("");
  }

  double pi2 = M_PI * M_PI, lr = log(r), r2 = r * r, r3 = r2 * r, r4 = r2 * r2,
         lr2 = lr * lr;

  if (r <= 0.001) {
    double res = pi2 / 6 * r * (1 + r2) - r2 +
                 (13 * lr / 18 - 151 / 216.0 - pi2 / 18 - lr2 / 3) * r4;
    for (int i = 3; i < kMaxIter; ++i) {
      int i2 = i * i;
      int i3 = i2 * i;
      int i4 = i3 * i;
      r4 *= r2;
      double fFun = 3 * (i - 1) / 4.0 / i / (i - 2) / (2 * i - 1) / (2 * i - 3);
      double gFun = -3 * (6 - 38 * i + 67 * i2 - 48 * i3 + 12 * i4) / 4.0 /
                    pow(1 - 2 * i, 2) / pow(3 - 2 * i, 2) / pow(i - 2, 2) / i2;
      double corr = (2 * fFun * lr + gFun) * r4;
      res += -4 / 3.0 * corr;
      if (abs(corr / res) < kMaxPrec) {
        break;
      } else if (i == kMaxIter - 1) {
        res = std::nan("");
        break;
      }
    }

    return res;
  }

  if (abs(r - 1) <= 0.001) {
    doubleP a(1 - r);

    return pi2 / 6 - 0.5 + (4 / 3.0 - 2 * pi2 / 9) * a[1] - a[5] / 90 +
           (pi2 / 12 - 1) * a[2] + (pi2 / 12 - 1) * a[3] / 3 - a[6] / 180 -
           a[8] / 600 - (pi2 - 9) * a[4] / 36 - 2 * a[7] / 675 -
           779 * a[9] / 793800 - 191 * a[10] / 317520;
  }

  return (lr2 + pi2 / 6 - r2 * (1.5 + lr) +
          (1 + r) * (1 + r3) * (Li2(-r) - lr2 / 2 + lr * log1p(r) + pi2 / 6) -
          (1 - r) * (1 - r3) * (Li2(1 - r) + lr2 / 2 + pi2 / 6)) /
         3;
}

//------------------------------------------------------------------------------
double AnDim::msLightCorr3L(int nh, int nl, double r) {
  if (r <= 0.0 || r > 1.0) {
    error(ErrorType::Internal);
    return nan("");
  }

  return msLightCorr3LG(r) + nl * msLightCorr3LNl(r) + nh * msLightCorr3LNh(r) +
         2 * msLightCorr2L(r) / 3;
}

//------------------------------------------------------------------------------
double AnDim::msLightCorr3LDoBu(double r1, double r2) {
  double rh, rh2, rat, lrat, rat2, rat3, rat4;

  if (r1 * r2 <= 0.0 || r1 <= 0.0 || r1 > 1.0 || r2 > 1.0) {
    error(ErrorType::Internal);
    return nan("");
  }

  if (r1 > r2) {
    rh = r1;
    rat = r2 / r1;
  } else {
    rh = r2;
    rat = r1 / r2;
  }

  lrat = log(rat);
  rat2 = rat * rat;
  rat3 = rat2 * rat;
  rat4 = rat3 * rat;
  rh2 = rh * rh;

  return (64.94343831699491 * rat * rh + 31.76898209724912 * rat2 * rh -
          0.16416623684141785 * rat3 * rh - 13.407339285356375 * rat2 * rh2 -
          0.7994215175732678 * rat3 * rh2 + 6.562504757079997 * rat4 * rh2 -
          70.5335117178178 * rat * rh * lrat -
          9.23584680826455 * rat2 * rh * lrat +
          10.068212741421073 * rat2 * rh2 * lrat -
          14.630200980712981 * rat3 * rh2 * lrat) /
         64.0;
}

//------------------------------------------------------------------------------
doubleV AnDim::msLightCorr(int idx, int nh, int nl, const doubleV& mm) {
  doubleV msLightCorr(3);
  double mH = mm[idx];
  for (int i = 0; i < idx; ++i) {
    msLightCorr[1] += mH * msLightCorr2L(mm[i] / mH);
    msLightCorr[2] += mH * msLightCorr3L(nh, nl, mm[i] / mH);
    for (int j = 0; j < i; ++j) {
      msLightCorr[2] += mH * msLightCorr3LDoBu(mm[i] / mH, mm[j] / mH);
    }
  }
  return msLightCorr;
}

//------------------------------------------------------------------------------
double AnDim::psLightCorr2L(double muf, double mL) {
  double r = mL / muf;

  if (r < 0) {
    error(ErrorType::Internal);
    return nan("");

  } else if (r <= 1) {
    doubleP rPow(r);

    return muf *
           (1.6440397503943704 * r + 0.005049578586646511 * pow(r, 1.5) -
            1.3493869526894149 * rPow[2] + 0.15924069721635872 * rPow[3] +
            1.928885583981453 * rPow[4] - 3.716714040160134 * rPow[5] +
            3.799215056763009 * rPow[6] - 2.216601652636038 * rPow[7] +
            0.486843660003175 * rPow[8] + 0.2389669212845065 * rPow[9] -
            0.18965570979484747 * rPow[10] + 0.03888351089869418 * rPow[11] -
            0.00011613765807827806 * r * log(r));

  } else {
    doubleP rPow(r);

    double res =
        0.07407407407407407 *
        (7 - 12 * rPow[2] + 3 * pow(1 + 4 * rPow[2], 1.5) * asinh(1 / (2 * r)) +
         6 * log(r));
    double s = sqrt(1 + 4 * rPow[2]);
    double s5 = pow(s, 5);
    res += 0.037037037037037035 * r *
           (44.41321980490211 - 14 / r + 12 * r +
            ((-9 + 464 * rPow[2] + 2576 * rPow[4] + 4608 * rPow[6]) *
             log((1 + 2 * r - s) / (2.0 * r))) /
                (16.0 * rPow[2] * s5) -
            (6 * (-1 + 2 * rPow[2]) * s * log((1 + s) / (2.0 * r))) / r -
            ((-9 + 464 * rPow[2] + 2576 * rPow[4] + 4608 * rPow[6]) *
             log(2.0 / (1 + 2 * r + s))) /
                (16.0 * rPow[2] * s5) +
            18 * log((1 + s) / (2.0 * r)) *
                log((1 - 2 * r + s) / (1 + 2 * r + s)) +
            18 * Li2(-(2 * r) / (1 + s)) - 18 * Li2((2 * r) / (1 + s)));

    return muf * res;
  }
}

//------------------------------------------------------------------------------
double AnDim::psLightCorr3L(int nl, double muf, double mL) {
  double r = mL / muf;
  if (r < kMaxPrec) {
    return 0.0;
  }

  double x = r / (1 + r);
  double lg = log(x);
  double lg2 = log(1 - x);
  auto sc = statConst();
  double cPart =
      1.0555555555555556 * sc[StPotType::c1] *
      (-2 + 4 * sc[StPotType::c2] * r * atan(0.5 / (sc[StPotType::c2] * r)) +
       log1p(0.25 / (sc[StPotType::c2] * sc[StPotType::c2] * r * r)));
  double dPart =
      1.0555555555555556 * sc[StPotType::d1] *
      (-2 + 4 * sc[StPotType::d2] * r * atan(0.5 / (sc[StPotType::d2] * r)) +
       log1p(0.25 / (sc[StPotType::d2] * sc[StPotType::d2] * r * r)));

  doubleP xPow(x);
  doubleP rPow(r);

  double nlPart;
  if (x < 0.5) {
    nlPart =
        (-0.5340704947507635 * x - 1.4647064771632332 * xPow[2] +
         0.2935344342827181 * xPow[3] - 0.5076368391447635 * xPow[4] +
         0.5643682176317187 * xPow[5] - 2.5346032175824766 * xPow[6] +
         5.586648853468869 * xPow[7] + 2.176482038899295 * xPow[8] -
         17.1089305258119 * xPow[9] + 19.40747180116465 * xPow[10] -
         6.977132478122115 * xPow[11] +
         (1 - x) * (0.5045350805245367 * lg2 + 2.1616951366417867 * lg2 * lg2) +
         x * (1.0966040055361546 * lg - 1.4716367410450128e-6 * lg * lg)) /
        (1 - x);
  } else {
    nlPart = (-0.6866964692101254 + 21.30836569511414 * x -
              31.023049115436176 * xPow[2] + 14.141868578422054 * xPow[3] +
              4.56861795443875 * xPow[4] - 30.800684512883404 * xPow[5] +
              51.62424533056591 * xPow[6] - 52.63109554834717 * xPow[7] +
              35.458464520415845 * xPow[8] - 15.496979061654711 * xPow[9] +
              4.002336019526847 * xPow[10] - 0.4653933909519701 * xPow[11] +
              (1 - x) * (0.5432100713298526 * lg2 +
                         1.5785962963019185e-8 * lg2 * lg2) +
              x * (14.906517353481648 * lg + 3.7176075875801726 * lg * lg)) /
             (1 - x);
  }

  double restPart;
  if (x < 0.3) {
    restPart =
        532.7795294278882 * x + 17682.909371451813 * xPow[2] -
        88.12345187894147 * xPow[3] - 1534.561345134128 * xPow[4] -
        1531.35738407044 * xPow[5] - 1244.9377357219887 * xPow[6] -
        1459.1716749595344 * xPow[7] + 282.76087631196845 * xPow[8] -
        3880.2485531397347 * xPow[9] + 4241.595945133972 * xPow[10] -
        4600.1010724579155 * xPow[11] +
        (1 - x) * (519.3659970416988 * lg2 - 17953.965589745094 * lg2 * lg2) +
        x * (-18.094256989084826 * lg + 1.2923430866247678e-6 * lg * lg);
  } else if (x < 0.9) {
    restPart =
        0.7343634431031869 - 71.55863791115681 * x +
        148.95132927642481 * xPow[2] - 213.60750561508613 * xPow[3] +
        328.984860374307 * xPow[4] - 390.98297080647285 * xPow[5] +
        330.7306538343686 * xPow[6] - 183.22085604088167 * xPow[7] +
        52.40051935258159 * xPow[8] + 3.2966758343184885 * xPow[9] -
        7.396138961221561 * xPow[10] + 1.6680468687461838 * xPow[11] +
        (1 - x) * (-9.929454022046105 * lg2 + 0.13681830248929847 * lg2 * lg2) +
        x * (-60.13622327257454 * lg - 8.07007816346947 * lg * lg);
  } else if (x < 0.98) {
    restPart =
        19.911194051724657 + 1.2267804934048823 * x -
        4.2022316123238594 * xPow[2] - 17.660993782181446 * xPow[3] -
        16.2746534067612 * xPow[4] + 4.630658371764665 * xPow[5] +
        21.97504134611601 * xPow[6] + 10.870173609079849 * xPow[7] -
        20.61708994480511 * xPow[8] - 23.206653031496103 * xPow[9] +
        33.91732361588121 * xPow[10] - 10.569535554169455 * xPow[11] +
        (1 - x) * (-9.864202533689406 * lg2 + 0.14592354549387285 * lg2 * lg2) +
        x * (26.42122890287121 * lg - 3.110640077663141 * lg * lg);
  } else {
    restPart =
        -426.27686227051584 - 423.80181492659466 * x -
        497.2702828935592 * xPow[2] - 236.79303006372135 * xPow[3] +
        417.30632427797934 * xPow[4] + 1186.7999070322282 * xPow[5] +
        1507.6148606981371 * xPow[6] + 654.004655773195 * xPow[7] -
        1859.4330682941159 * xPow[8] - 4706.627452850726 * xPow[9] +
        6464.787685058641 * xPow[10] - 2080.310921540948 * xPow[11] +
        (1 - x) * (-9.843690132221473 * lg2 + 0.14788901935806978 * lg2 * lg2) +
        x * (-3646.260526475269 * lg - 2693.855981434008 * lg * lg);
  }
  restPart = restPart / (1 - x) + 5.338156267526823 + 2.111111111111111 * lg;

  double msConvPart;
  if (x < 0.5) {
    msConvPart = 2.193245422464302 * r - 0.0015119444307143308 * pow(r, 1.5) -
                 3.604569897660804 * rPow[2] + 0.671096383141341 * rPow[3] +
                 10.280174691822818 * rPow[4] - 24.9670052849862 * rPow[5] +
                 31.155745793269343 * rPow[6] - 22.2968195380667 * rPow[7] +
                 7.258988301680239 * rPow[8] + 1.252079209986471 * rPow[9] -
                 1.82079553714247 * rPow[10] + 0.4368519620364587 * rPow[11] -
                 0.01294385239885661 * rPow[2] * log(r);
  } else {
    double sr = sqrt(1. + 4. * rPow[2]);
    msConvPart =
        0.0030864197530864196 *
        (224. - 768. * rPow[2] +
         8. * (-28. + 88.82643960980423 * r + 24. * rPow[2]) +
         (96. * (-1. + 4. * rPow[2] + 32. * rPow[4]) * asinh(0.5 / r)) / sr +
         ((-9. + 464. * rPow[2] + 2576. * rPow[4] + 4608. * rPow[6]) *
          log((0.5 * (1. + 2. * r - 1. * sr)) / r)) /
             (r * pow(1. + 4. * rPow[2], 2.5)) -
         96. * (-1. + 2. * rPow[2]) * sr * log((0.5 * (1. + sr)) / r) -
         (1. * (-9. + 464. * rPow[2] + 2576. * rPow[4] + 4608. * rPow[6]) *
          log(2. / (1. + 2. * r + sr))) /
             (r * pow(1. + 4. * rPow[2], 2.5)) +
         288. * r * log((0.5 * (1. + sr)) / r) *
             log((1. - 2. * r + sr) / (1. + 2. * r + sr)) +
         288. * r * Li2((-2. * r) / (1. + sr)) -
         288. * r * Li2((2. * r) / (1. + sr)));
  }

  return muf * (cPart + dPart + nl * nlPart + restPart + msConvPart);
}

//------------------------------------------------------------------------------
double AnDim::psLightCorr3LDoBu(double muf, double mL1, double mL2) {
  double rat, rLf;
  if (mL1 > mL2) {
    rat = mL2 / mL1;
    rLf = mL2 / muf;
  } else {
    rat = mL1 / mL2;
    rLf = mL1 / muf;
  }

  double logRat = log(rat), rLf2 = rLf * rLf;
  if (rLf < 0.55) {
    return muf *
           (rLf *
            (0.9125989623053927 + 1.5475271861131417 * rLf +
             0.06139548691406419 * rat * rat * rLf - 1.2195710807111817 * rLf2 +
             rat * (0.5796141657279605 - 1.6432271542700194 * rLf +
                    1.0716058483323971 * rLf2) +
             (-1.146086824600244 +
              rat * (-0.17977689149624754 + 0.3268670754477228 * rLf) +
              0.9078783521544506 * rLf - 0.25850136767210957 * rLf2) *
                 logRat +
             (-0.005318306962579802 + 0.00966964902287237 * rLf) * logRat *
                 logRat));
  } else {
    double logrLf = log(rLf);
    return muf *
           (1.2921810699588478 - (0.009876543209876543 * logRat) / rLf2 +
            1.08641975308642 * logrLf + 0.29629629629629634 * logrLf * logrLf +
            (0.009876543209876543 * (1. + rat * rat) *
             (1.1666666666666667 + logrLf)) /
                rLf2 -
            0.29629629629629634 * logRat * (1.8333333333333333 + logrLf));
  }
}

//------------------------------------------------------------------------------
doubleV AnDim::psLightCorr(int idx, int nl, double muF, const doubleV& mm) {
  doubleV psLightCorr(3);
  for (int i = 0; i < idx; ++i) {
    psLightCorr[1] += AnDim::psLightCorr2L(muF, mm[i]);
    psLightCorr[2] += AnDim::psLightCorr3L(nl, muF, mm[i]);
    for (int j = 0; j < i; ++j) {
      psLightCorr[2] += AnDim::psLightCorr3LDoBu(muF, mm[i], mm[j]);
    }
  }
  return psLightCorr;
}

//------------------------------------------------------------------------------
double AnDim::oneSLightCorr2L(double bohr, double mL) {
  double a = 2 * mL / bohr;
  return M_PI / 9.0 * (oneSLighth0(a) + log(a / 2.0) + 11.0 / 6.0);
}

//------------------------------------------------------------------------------
double AnDim::oneSLightCorr3L(int nl, double bohr, double mL, double muA) {
  double a = 2 * mL / bohr;
  double beta0 = betaGen(nl)[0];
  double gam = bohr / 2.0;
  double lg = log(a / 2.0);
  double b = a / (a + 1);
  if (b < 0 || b > 1) {
    error(ErrorType::Internal);
    return nan("");
  }
  doubleP bPow(b);
  double lgb = log(b);
  double lg1b = log(1 - b);

  double c00, c10, c20, c01, c02, c11, c04, c21, c12, c13, c14, c22, d01, d02,
      d11, c23, c24, c03;
  if (b <= 0.051038861138861136) {
    c00 = 0;
    c10 = 0;
    c20 = 0;
    c01 = 1.90110452664456516;
    c02 = -1.029038459692711934;
    c11 = -2.35619425205269358;
    c04 = 9.35239491571673955;
    c21 = 0;
    c12 = -2.35929530335247809;
    c13 = -5.58980780839759901;
    c14 = -4.58915393268937608;
    c22 = -0.000234709300959580963;
    d01 = 0;
    d02 = 0;
    d11 = 0;
    c23 = 0.00696824997469098970;
    c24 = 1.43230939876772142;
    c03 = -5.07850393199686814;
  } else if (b <= 0.9000198801198801) {
    c00 = 0.044287505246154754;
    c10 = 0.015709263179061187;
    c20 = 0.0014897123269043706;
    c01 = 1.672907250600346;
    c02 = 0.8512628934882618;
    c11 = -1.920680261243725;
    c04 = 0;
    c21 = 0.036765440928499135;
    c12 = -0.35702728835979514;
    c13 = 0;
    c14 = 0;
    c22 = 0;
    d01 = -1.2083794123532003;
    d02 = -0.0038904651108663335;
    d11 = 0.17169677823535778;
    c23 = 0;
    c24 = 0;
    c03 = 0;
  } else {
    c00 = -50201.06158803705;
    c10 = -41425.40269749409;
    c20 = -9078.906294010318;
    c01 = -48746.62298351992;
    c02 = 98950.34928195227;
    c11 = -81274.4123639453;
    c04 = 0;
    c21 = -30268.1903388468;
    c12 = -26453.26044888256;
    c13 = 0;
    c14 = 0;
    c22 = 0;
    d01 = -0.999999967129952;
    d02 = 0;
    d11 = 0;
    c23 = 0;
    c24 = 0;
    c03 = 0;
  }
  double fit1 = c00 + b * c01 + bPow[2] * c02 + bPow[3] * c03 + bPow[4] * c04 +
                d01 * lg1b + b * d11 * lg1b + d02 * lg1b * lg1b + c10 * lgb +
                b * c11 * lgb + bPow[2] * c12 * lgb + bPow[3] * c13 * lgb +
                bPow[4] * c14 * lgb + c20 * lgb * lgb + b * c21 * lgb * lgb +
                bPow[2] * c22 * lgb * lgb + bPow[3] * c23 * lgb * lgb +
                bPow[4] * c24 * lgb * lgb;

  if (b < 0.10097892107892108) {
    c00 = 0;
    c10 = 0;
    c20 = 0;
    c01 = -26.451769691526512;
    c02 = 0.70702236091614733;
    c11 = -0.43982646046327640;
    c04 = -2259.5701193787382;
    c21 = -0.035490964731981507;
    c12 = -15.397182258933281;
    c13 = -282.16630675994196;
    c14 = -722.28179218331797;
    c22 = -2.5053875423243665;
    d01 = 0;
    d02 = 0;
    d11 = 0;
    c23 = -179.11181760192293;
    c24 = -1617.8840292538800;
    c03 = 927.98023086181417;
  } else if (b < 0.9000198801198801) {
    c00 = -2.8001592883537616;
    c10 = -0.8627040826635972;
    c20 = -0.07340291620334549;
    c01 = 7.112354257413943;
    c02 = -15.920549168912885;
    c11 = 4.434308507254591;
    c04 = 0;
    c21 = 2.0770927734735847;
    c12 = 5.3556122324142486;
    c13 = 0;
    c14 = 0;
    c22 = 0;
    d01 = 18.788290422388098;
    d02 = 1.5031191940158986;
    d11 = -3.1788089372938515;
    c23 = 0;
    c24 = 0;
    c03 = 0;
  } else {
    c00 = 118139.11974059054;
    c10 = 95579.86773330937;
    c20 = 20707.053165708814;
    c01 = 109131.78521564882;
    c02 = -227282.59371793616;
    c11 = 188851.68268849238;
    c04 = 0;
    c21 = 68458.02066492023;
    c12 = 60986.228070174264;
    c13 = 0;
    c14 = 0;
    c22 = 0;
    d01 = 18.58835707845213;
    d02 = 1.50000236116084;
    d11 = -3.0088709022115836;
    c23 = 0;
    c24 = 0;
    c03 = 0;
  }
  double fit2 = c00 + b * c01 + bPow[2] * c02 + bPow[3] * c03 + bPow[4] * c04 +
                d01 * lg1b + b * d11 * lg1b + d02 * lg1b * lg1b + c10 * lgb +
                b * c11 * lgb + bPow[2] * c12 * lgb + bPow[3] * c13 * lgb +
                bPow[4] * c14 * lgb + c20 * lgb * lgb + b * c21 * lgb * lgb +
                bPow[2] * c22 * lgb * lgb + bPow[3] * c23 * lgb * lgb +
                bPow[4] * c24 * lgb * lgb;

  auto pc = statConst();
  double cdPart = 57.0 / 4.0 *
                  ((pc[StPotType::c1] * pc[StPotType::c2] * a) /
                       (1 + pc[StPotType::c2] * a) +
                   (pc[StPotType::d1] * pc[StPotType::d2] * a) /
                       (1 + pc[StPotType::d2] * a) +
                   pc[StPotType::c1] * log1p(pc[StPotType::c2] * a) +
                   pc[StPotType::d1] * log1p(pc[StPotType::d2] * a));

  return M_PI / 27.0 *
             (3 * beta0 *
                  (1.5 * (lg + oneSLighth0(a) - oneSLighth0bar(a) / 3.0 + 1.5) *
                       (log(muA / (2 * gam)) + 5.0 / 6.0) +
                   fit1) +
              fit2 + cdPart) -
         kFourPi / 27.0 * (oneSLighth0bar(a) + 1);  // Light fl Pole MS conv
}

//------------------------------------------------------------------------------
double AnDim::oneSLightCorr3LDoBu(double bohr, double mL1, double mL2) {
  double a1 = 2 * mL1 / bohr;
  double a2 = 2 * mL2 / bohr;
  double b1 = max(a1 / (1 + a1), a2 / (1 + a2));
  double b2 = min(a1 / (1 + a1), a2 / (1 + a2));

  doubleP b1Pow(b1);
  doubleP b2Pow(b2);
  doubleP lb1Pow(log(b1));
  doubleP lb2Pow(log(b2));
  doubleP lomb1Pow(log(1 - b1));
  doubleP lomb2Pow(log(1 - b2));

  map<int, double> c;
  if (b1 <= 0.5) {
    c[12] = 0.000147271501875;
    c[144] = -0.0063981437171;
    c[1740] = 0.024166599678;
    c[1716] = -0.162570546109;
    c[72] = 0.0000100931386682;
    c[876] = -0.00169772072603;
    c[10728] = 0.040797904798;
    c[852] = 0.0073685955003;
    c[10296] = -0.026023486491;
    c[10152] = 0.057731174912;
    c[30] = 0.0035699864005;
    c[366] = -0.029119630744;
    c[4494] = 0.088003233409;
    c[354] = 0.35632758839;
    c[4290] = 0.29976503761;
    c[4206] = -0.38422579436;
    c[186] = 0.00072276625897;
    c[2298] = -0.000080712585964;
    c[28554] = 0.23041900199;
    c[2166] = 0.036809244226;
    c[26598] = -0.170588693373;
    c[25386] = 0.089801416469;
    c[174] = 0.0074031831343;
    c[2094] = 0.048859973916;
    c[25374] = -0.065377051853;
    c[2082] = -0.035703892751;
    c[24882] = -0.34496082547;
    c[25086] = -0.50410186509;
    c[78] = 0.045960223525;
    c[966] = 0.028930422714;
    c[12030] = -0.115613167757;
    c[906] = 0.156699495126;
    c[11154] = -0.2194887558;
    c[10590] = 0.025043951719;
    c[498] = 0.0085608620529;
    c[6234] = 0.091512628634;
    c[78402] = -0.0195838831765;
    c[5718] = 0.086018929376;
    c[71214] = 0.078628359201;
    c[66018] = -0.0057389649475;
    c[438] = -0.0056579831291;
    c[5358] = -0.035063193466;
    c[65958] = 0.0134920347759;
    c[5154] = -0.37976476149;
    c[62634] = 0.25572423772;
    c[61062] = -0.025715199969;
    c[864] = -0.27840350575;
    c[10440] = 0.370527188;
    c[432] = 0.0185793604963;
    c[5256] = -0.37335007377;
    c[64368] = -0.22108575289;
    c[5112] = 0.121424737253;
    c[61776] = 0.23788023247;
    c[60912] = -0.042793900516;
  } else if (b1 >= 0.5 && b2 <= 0.5) {
    c[144] = -0.9680151668853136;
    c[1740] = -0.006414805214531729;
    c[1716] = -2.61583939133521;
    c[72] = 0.03152035467905305;
    c[876] = 2.118574009667999;
    c[10728] = 0.13945094762079402;
    c[852] = -0.05555655368385897;
    c[10296] = 0.26791269531632145;
    c[10152] = 0.021843126088633705;
    c[30] = 0.13100911633476117;
    c[366] = -0.2232570357907621;
    c[4494] = -0.0581068223603699;
    c[354] = -0.9677675653065094;
    c[4290] = 1.5933111364206105;
    c[4206] = 0.25496950794023554;
    c[186] = 2.7241030243797426;
    c[2298] = -5.367868069941449;
    c[28554] = -0.016282389039921095;
    c[2166] = 0.7328297795915111;
    c[26598] = -0.9460831583928238;
    c[25386] = 0.17609365537799349;
    c[174] = -0.057172405724376735;
    c[2094] = 0.08198093943483406;
    c[25374] = -0.03604880805045859;
    c[2082] = -0.862460968629979;
    c[24882] = -0.5033255593297764;
    c[25086] = 0.12156999785853602;
    c[78] = -0.35980749827924674;
    c[966] = 1.1637276726927281;
    c[12030] = 0.062213058801279794;
    c[906] = 1.316704033517357;
    c[11154] = -0.482486391147773;
    c[10590] = 0.010050368135446686;
    c[498] = -2.84761427262739;
    c[6234] = 0.9610242959900546;
    c[78402] = -0.254495724267034;
    c[5718] = -0.5686971304328398;
    c[71214] = 0.21318912488708144;
    c[66018] = -0.0027120933677893047;
    c[438] = 0.036562306125490496;
    c[5358] = -0.03271790175438399;
    c[65958] = 0.013677429594137267;
    c[5154] = 0.7561862186486201;
    c[62634] = -0.26123159103241145;
    c[61062] = 0.22653561330285446;
    c[864] = -0.3405604943533743;
    c[10440] = -0.26257851331710674;
    c[432] = -0.3822638351010492;
    c[5256] = 0.42258637764759777;
    c[64368] = -0.07494647497731521;
    c[5112] = -1.312361495055012;
    c[61776] = 0.5261397384848271;
    c[60912] = -0.18355535707687237;

    c[12] = 0.13844982954598706 - 0.9825146041801048 * b2Pow(1) +
            0.7845248511427612 * b2Pow(2) +
            (-0.03133383786809511 - 2.5450711640841224 * b2Pow(1) +
             2.4009062741829776 * b2Pow(2)) *
                lb2Pow(1) +
            (0.14898354955975696 + 0.756588749603543 * b2Pow(1) -
             1.0670947010890985 * b2Pow(2) +
             (-1.9316065549865977 + 4.532190461608673 * b2Pow(1) -
              0.776241252006759 * b2Pow(2)) *
                 lb2Pow(1)) *
                lomb2Pow(1) +
            (-0.27971242229408194 + 0.4342074925566638 * b2Pow(1) -
             0.1776977206623786 * b2Pow(2) +
             (-0.47455167761278416 + 0.17363175207583742 * b2Pow(1) +
              0.23491184109055036 * b2Pow(2)) *
                 lb2Pow(1)) *
                lomb2Pow(2);
  } else {
    c[144] = 11.938102795215835;
    c[1740] = 0.3181852977835494;
    c[1716] = 2.2713405888492324;
    c[72] = -0.5174241182598056;
    c[876] = -6.791686554664494;
    c[10728] = -0.5438176284935273;
    c[852] = -0.8592730490614154;
    c[10296] = 0.8050781158724348;
    c[10152] = -0.026235876899343934;
    c[30] = -3.941351716555487;
    c[366] = 2.993043180088912;
    c[4494] = 1.0126207197469426;
    c[354] = -0.8335019450771208;
    c[4290] = -1.1106838449537537;
    c[4206] = 1.6430194281749302;
    c[186] = -16.038065196529182;
    c[2298] = 37.233188950237526;
    c[28554] = 0.628401858206849;
    c[2166] = 0.45399558305454074;
    c[26598] = 0.6382669299213586;
    c[25386] = 0.03967402164030693;
    c[174] = -0.7996292168232066;
    c[2094] = 1.3029216244168982;
    c[25374] = -0.06969795041503374;
    c[2082] = -2.726176762226183;
    c[24882] = -0.07019726940627;
    c[25086] = 0.8892966759110109;
    c[78] = 6.42377313657739;
    c[966] = -15.064189924617287;
    c[12030] = -1.3327749663892237;
    c[906] = 0.30977140267502484;
    c[11154] = -0.4857024604831928;
    c[10590] = 0.02945667628227687;
    c[498] = 11.998917883907911;
    c[6234] = -1.8127090654675408;
    c[78402] = 1.8681441671253731;
    c[5718] = -0.1538121809980219;
    c[71214] = 0.17056098843070389;
    c[66018] = -0.012729966468196022;
    c[438] = 0.9269878766206391;
    c[5358] = -0.45620673448336235;
    c[65958] = 0.09691660299072165;
    c[5154] = 0.607429575052395;
    c[62634] = -0.23960100348035787;
    c[61062] = 0.11200749488148337;
    c[864] = 0.6730850857390074;
    c[10440] = -1.6705323139988577;
    c[432] = -1.900166125522486;
    c[5256] = 0.600475212319282;
    c[64368] = 0.4331400500466339;
    c[5112] = -0.6517449789485091;
    c[61776] = 0.08914575924346421;
    c[60912] = -0.059631911411335156;

    c[12] = -2.254311014208648 + 4.614808893106779 * b1Pow(1) -
            6.840331103513845 * b1Pow(2) +
            (0.5114184271265129 + 19.592571994435943 * b1Pow(1) -
             14.55414270625366 * b1Pow(2)) *
                lb1Pow(1) +
            (-11.487074878216513 - 4.439098579037514 * b1Pow(1) +
             15.921755816530762 * b1Pow(2) +
             (10.405501440466923 - 41.74326421309635 * b1Pow(1) +
              2.744185788860333 * b1Pow(2)) *
                 lb1Pow(1)) *
                lomb1Pow(1) +
            (-1.1058178728902233 - 0.3471258564405321 * b1Pow(1) +
             1.4526850235424864 * b1Pow(2) +
             (0.3280372666934267 - 0.8987275097587446 * b1Pow(1) -
              2.122639891392407 * b1Pow(2)) *
                 lb1Pow(1)) *
                lomb1Pow(2);
  }

  double res = 0;
  rac::Power<int> pow2(2);
  rac::Power<int> pow3(3);
  rac::Power<int> pow5(5);
  rac::Power<int> pow7(7);
  rac::Power<int> pow11(11);
  rac::Power<int> pow13(13);
  for (int p1 = 1; p1 <= 3; ++p1) {
    for (int p2 = 1; p2 <= 4 - p1; ++p2) {
      for (int pl1 = 0; pl1 <= 1; ++pl1) {
        for (int pl2 = 0; pl2 <= 1 - pl1; ++pl2) {
          for (int pl11 = 0; pl11 <= 2; ++pl11) {
            for (int pl12 = 0; pl12 <= 2 - pl11; ++pl12) {
              res += c[pow2[p1] * pow3[p2] * pow5[pl1] * pow7[pl2] *
                           pow11[pl11] * pow13[pl12] +
                       pow2[p2] * pow3[p1] * pow5[pl2] * pow7[pl1] *
                           pow11[pl12] * pow13[pl11]] *
                     b1Pow[p1] * b2Pow[p2] * lb1Pow[pl1] * lb2Pow[pl2] *
                     lomb1Pow[pl11] * lomb2Pow[pl12];
            }
          }
        }
      }
    }
  }

  return -res;
}

//------------------------------------------------------------------------------
double AnDim::oneSLighth0(double a) {
  doubleP aPow(a);
  if (a > 1.0000000001) {
    return (-11.0 / 6.0 + 3.0 * M_PI / 4.0 * a - 2.0 * aPow[2] +
            M_PI * aPow[3] +
            (2 - aPow[2] - 4 * aPow[4]) / sqrt(aPow[2] - 1) *
                atan(sqrt(a - 1) / sqrt(a + 1)));
  } else if (a < 0.9999999999) {
    return (
        -1.8333333333333333 - 2 * aPow[2] + (3 * a * M_PI) / 4. +
        aPow[3] * M_PI +
        ((2 - aPow[2] - 4 * aPow[4]) * (-log(1 - sqrt(1 - a) / sqrt(1 + a)) +
                                        log1p(sqrt(1 - a) / sqrt(1 + a)))) /
            (2. * sqrt(1 - aPow[2])));
  } else {
    return 1.0095238095238095 - 2.062853128855274 * aPow[1] + 2 * aPow[2] -
           0.9726930606959212 * aPow[3] + 0.19047619047619047 * aPow[4];
  }
}

//------------------------------------------------------------------------------
double AnDim::oneSLighth0bar(double a) {
  doubleP aPow(a);
  if (a > 1.001) {
    return (
        (4 + 14 * aPow[2] - 24 * aPow[4] - 3 * a * M_PI - 9 * aPow[3] * M_PI +
         12 * aPow[5] * M_PI) /
            (4. * (-1 + aPow[2])) -
        (3 * aPow[4] * (-5 + 4 * aPow[2]) * atan(sqrt((-1 + a) / (1 + a)))) /
            pow(-1 + aPow[2], 1.5));
  } else if (a < 0.999) {
    return (
        -(sqrt(1 - aPow[2]) * (4 + 14 * aPow[2] - 24 * aPow[4] - 3 * a * M_PI -
                               9 * aPow[3] * M_PI + 12 * aPow[5] * M_PI) +
          6 * aPow[4] * (-5 + 4 * aPow[2]) *
              log(1 - sqrt(1 - a) / sqrt(1 + a)) -
          6 * aPow[4] * (-5 + 4 * aPow[2]) * log1p(sqrt(1 - a) / sqrt(1 + a))) /
        (4. * pow(1 - aPow[2], 1.5)));
  } else {
    return -0.904428904428896 + 1.6108065448043731 * aPow[1] -
           1.6503496503496144 * aPow[2] + 1.0353007712921607 * aPow[3] -
           0.3663003663003539 * aPow[4] + 0.05594405594405383 * aPow[5];
  }
}

//------------------------------------------------------------------------------
doubleV AnDim::oneSLightCorr(int idx, int nl, double mB, const doubleV& mm,
                             double muA, Count1S counting, double rB) {
  doubleV oneSLightCorr(3);
  for (int i = 0; i < idx; ++i) {
    oneSLightCorr[1] += mB * oneSLightCorr2L(mB, mm[i]);
    oneSLightCorr[2] += mB * oneSLightCorr3L(nl, mB, mm[i], muA);
    if (counting == Count1S::Relativistic) {
      oneSLightCorr[2] +=
          rB * (AnDim::oneSLightCorr2L(mB, mm[i]) +
                M_PI / 9 * (AnDim::oneSLighth0bar(2 * mm[i] / mB) + 1));
    }
    for (int j = 0; j < i; ++j) {
      oneSLightCorr[2] += mB * AnDim::oneSLightCorr3LDoBu(mB, mm[i], mm[j]);
    }
  }
  return oneSLightCorr;
}

//------------------------------------------------------------------------------
void AnDim::setNextsCoef(int betaOrd, int gammaROrd, MScheme sType,
                         int p) const {
  int key = buildSKey(betaOrd, gammaROrd, sType);
  const doubleV& gamma = (sType == MScheme::MSRn) ? _gammaRn : gammaRp();
  if (gammaROrd > int(gamma.size())) {
    gammaROrd = gamma.size();
  }
  setNextsCoefGeneric(_sMap[key]._sCoef[p], _sMap[key]._gammaTil,
                      doubleV(gamma.begin(), gamma.begin() + gammaROrd),
                      bind(&AnDim::gTilde, this, _1, betaOrd, true, p),
                      bind(&AnDim::bHat, this, _1, betaOrd, true));
}

//------------------------------------------------------------------------------
void AnDim::setNextsCoefLambda(int betaOrd, int gammaROrd, MScheme sType,
                               int p) const {
  int key = buildSKey(betaOrd, gammaROrd, sType);
  const doubleV& gamma =
      (sType == MScheme::MSRn) ? _gammaRnLambda : gammaRpLambda();
  if (gammaROrd > int(gamma.size())) {
    gammaROrd = gamma.size();
  }
  setNextsCoefGeneric(_sMap[key]._sCoefLambda[p], _sMap[key]._gammaTilLambda,
                      doubleV(gamma.begin(), gamma.begin() + gammaROrd),
                      bind(&AnDim::gTildeLambda, this, _1, betaOrd, true, p),
                      bind(&AnDim::bHatLambda, this, _1, betaOrd, true));
}

//------------------------------------------------------------------------------
void AnDim::setNextMSRDeltaCoef(MScheme type, int betaOrd,
                                int gammaROrd) const {
  int key = buildSKey(betaOrd, gammaROrd, type);
  bool isStrict = true;
  int nextCoe = _sMap[key]._msrDeltaAsy.size() + 1;

  rac::Pochhammer pList;
  double res = 0, b02Pow = pow(_beta[0] / 2, nextCoe);

  for (int k = 0; k < min(gammaROrd, nextCoe); ++k) {
    pList = rac::Pochhammer(1 + bHat(1, betaOrd, isStrict) + k);
    for (int l = 0; l <= max(0, min(nextCoe - k - 1, betaOrd - 2)); ++l) {
      res += sCoef(k, betaOrd, gammaROrd, type, isStrict) *
             gl(l, betaOrd, isStrict) * pList[nextCoe - k - l - 1];
    }
  }
  _sMap[key]._msrDeltaAsy.push_back(b02Pow * res);
}

//------------------------------------------------------------------------------
void AnDim::setNextMSBarDeltaCoef(int betaOrd, int gammaROrd) const {
  int key = buildSKey(betaOrd, gammaROrd, MScheme::MSRp);
  int nextCoe = _sMap[key]._msBarDeltaAsy.size();
  doubleV msrPCoef(nextCoe + 1);

  if (_andimM1.empty() && (!_andimM || _andimM->count(_nf - 1) == 0)) {
    _andimM1.emplace_back(_nf - 1, _scheme, _precisionGoal);
    _andimM1[0].setAlphaProp(_betaOrd, _betaIsStrict);
    _andimM1[0].setMsrProp(_gammaROrd, _sIsStrict);
    _andimM1[0].setMsBarDeltaError(_msBarDeltaError);
  }
  const AnDim& andim = _andimM1.empty() ? _andimM->at(_nf - 1) : _andimM1[0];
  for (int i = 0; i <= nextCoe; ++i) {
    msrPCoef[i] = andim.msrpDeltaAsy(i, betaOrd, gammaROrd);
  }
  matchSeries(msrPCoef, Direction::Up);
  _sMap[key]._msBarDeltaAsy.push_back(msrPCoef[nextCoe]);
}

//------------------------------------------------------------------------------
doubleG AnDim::gammaMassDir(Direction dir, MScheme scheme) const {
  double s;
  double b[5][5][5] = {};
  doubleV gamma;
  doubleV aMatch = alphaMatch(_nf, scheme);
  doubleV aMatchInv;
  doubleG aMatchLog;

  switch (dir) {
    case Direction::Up:
      aMatchInv = getInverseMatch(aMatch, 5);
      aMatchLog = alphaMatchingLog(aMatchInv, _nf - 1, _nf);
      gamma = massAndim(_nf);
      break;

    case Direction::Down:
      aMatchLog = alphaMatchingLog(aMatch, _nf, _nf - 1);
      gamma = massAndim(_nf - 1);
      break;

    default:
      error(ErrorType::Keyword);
      return doubleG();
      break;
  }

  copy(aMatchLog.begin(), aMatchLog.end(), &b[0][0][0]);

  double pow = 1.0;
  for (auto& gam : gamma) {
    pow /= 4.0;
    gam *= pow;
  }

  for (int n = 1; n < 5; ++n) {
    for (int i = 0; i < 5; ++i) {
      for (int j = 0; j < 5; ++j) {
        for (int l = 0; l < j + 1; ++l) {
          s = 0.0;
          for (int k = 0; k < i - j + 1; ++k) {
            s += b[0][l][k + l] * b[n - 1][j - l][i - l - k];
          }
          b[n][j][i] += s;
        }
      }
    }
  }

  doubleG outCoe(5, 5);

  for (int i = 0; i < 5; ++i) {
    for (int j = 0; j < i + 1; ++j) {
      for (int n = 0; n < i + 1 - j; ++n) {
        outCoe[j][i] += gamma[n] * b[n][j][i - n];
      }
    }
  }

  return outCoe;
}

//------------------------------------------------------------------------------
doubleG AnDim::msBarMatchingLog(Direction dir, MScheme scheme) const {
  double pow = 1.0;
  doubleV gam;
  doubleV bet;

  switch (dir) {
    case Direction::Down:
      bet = betaGen(_nf);
      gam = massAndim(_nf);
      break;

    case Direction::Up:
      bet = betaGen(_nf - 1);
      gam = massAndim(_nf - 1);
      break;

    default:
    error(ErrorType::Internal);
      return doubleG();
      break;
  }

  for (int i = 0; i < int(bet.size()); ++i) {
    pow /= 4.0;
    bet[i] *= pow;
    gam[i] *= pow;
  }

  doubleG g = gammaMassDir(dir, scheme);
  doubleV outCoeV = msBarMatchingCoef(dir, scheme);
  int matchSize = outCoeV.size();
  doubleG outCoe(matchSize, matchSize);
  copy(outCoeV.begin(), outCoeV.end(), outCoe.rowBegin(0));

  for (int i = 2; i < matchSize; ++i) {
    for (int j = 0; j < i; ++j) {
      for (int n = 1; n < i - j + 1; ++n) {
        outCoe[j + 1][i] +=
            ((i - n) * bet[n - 1] - gam[n - 1]) * outCoe[j][i - n];
      }
      for (int l = 0; l < j + 1; ++l) {
        for (int k = l; k < i + l - j; ++k) {
          outCoe[j + 1][i] += outCoe[l][k] * g[j - l][i - k - 1];
        }
      }
      outCoe[j + 1][i] *= 2.0 / (j + 1);
    }
  }

  return outCoe;
}

//------------------------------------------------------------------------------
doubleV AnDim::msBarMatchingCoef(Direction dir, MScheme scheme) const {
  doubleV outCoe(5);
  switch (dir) {
    case Direction::Down:
      if (scheme == MScheme::MSbar) {
        double d[5][5] = {};

        d[0][0] = 1.0;

        for (int i = 1; i < 5; ++i) {
          for (int j = 1; j < i; ++j) {
            d[1][i] += j * d[1][j] * _msBarDelta[i - j - 1];
          }
          d[1][i] = _msBarDelta[i - 1] - d[1][i] / i;
        }

        doubleG c = msBarMatchingLog(Direction::Down, MScheme::Pole);

        for (int i = 1; i < 4; ++i) {
          for (int j = i; j < 4; ++j) {
            for (int k = 1; k < j - i + 1; ++k) {
              d[i + 1][j] += d[1][k] * d[i][j - k];
            }
          }
        }

        int m1Pow;
        for (int k = 1; k < 6; ++k) {
          for (int i = 1; i < k + 1; ++i) {
            m1Pow = -1;
            for (int j = 0; j < min(i - 1, k - i) + 1; ++j) {
              m1Pow *= -1;
              outCoe[k - 1] += m1Pow * c[j][i - 1] * d[j][k - i];
            }
          }
        }
      } else if (scheme == MScheme::Pole) {
        int nl = _nf - 1;

        outCoe[0] = 1.0;
        outCoe[1] = 0.0;
        outCoe[2] = 0.20601851851851852;
        outCoe[3] = 1.4772563718544154 + 0.02472760936815077 * nl;
        outCoe[4] = 0.22335728208881278 - 1.5035995976469412 * nl +
                    0.05615582122474318 * nl * nl;
      }
      break;

    case Direction::Up: {
      doubleV downCoe = msBarMatchingCoef(Direction::Down, scheme);
      outCoe = seriesInverse(downCoe);
      seriesReExpand(alphaMatch(AlphaMatchCoefType::Inv), outCoe, true);
      break;
    }

    default:
      break;
  }

  return outCoe;
}

//------------------------------------------------------------------------------
doubleG AnDim::msDeltaAlphaLog(MScheme scheme) const {
  doubleV aOut;
  switch (scheme) {
    case MScheme::MSbar:
      aOut = _msBarDelta;
      break;

    case MScheme::MSRn:
      aOut = _msrnDelta;
      break;

    case MScheme::MSRp:
      aOut = msrpDelta();
      break;

    default:
      error(ErrorType::Keyword);
      return doubleG();
      break;
  }

  return expandAlpha(_beta, aOut);
}

//------------------------------------------------------------------------------
doubleG AnDim::psDeltaLog(double rIR) const {
  doubleV coefV = psDelta(_nf, rIR);
  return expandAlpha(_beta, coefV);
}

//------------------------------------------------------------------------------
doubleG AnDim::rsDeltaLog(double N12var, int nRS) const {
  return expandAlpha(_beta, rsDelta(nRS, N12var));
}

//------------------------------------------------------------------------------
doubleG AnDim::oneSDeltaLog(double alpha) const {
  doubleV coefV = oneSDelta(_nf, alpha);
  return generate1SLogs(coefV);
}

//------------------------------------------------------------------------------
doubleG AnDim::generate1SLogs(const doubleV& del) const {
  if (int(del.size()) != 4) {
    error(ErrorType::Internal);
    return doubleG();
  }
  double sum;
  doubleG coef(4, 4);
  copy(del.begin(), del.end(), coef.rowBegin(0));
  for (int k = 1; k <= 4; ++k) {
    for (int j = 0; j < k - 1; ++j) {
      sum = 0;
      for (int i = j + 1; i <= k - 2; ++i) {
        sum += _beta[k - 2 - i] * ((i + 2) * coef[j][i] * kPow4[i] -
                                   (j + 1) * coef[j + 1][i] * kPow4[i]);
      }
      coef[j + 1][k - 1] =
          2.0 / (j + 1.0) *
          ((j + 2.0) * _beta[k - 2 - j] * coef[j][j] * kPow4[j] + sum) /
          kPow4[k - 1];
    }
  }
  return coef;
}

//------------------------------------------------------------------------------
doubleG AnDim::msBarDeltaAsyPiece(int length, int betaOrd, int gammaROrd,
                                  int gammaMOrd) const {
  int leP = length + 1;
  doubleG b(leP, leP);

  applyConstrain(betaOrd, 1, kMaxRunAlpha);
  applyConstrain(gammaROrd, 1, kMaxRunMSR);
  applyConstrain(gammaMOrd, 1, kMaxRunMSbar);

  for (int i = 0; i < length; ++i) {
    b[0][i + 1] = msBarDeltaAsy(i, betaOrd, gammaROrd);
  }

  b[0][0] = 1.0;

  for (int i = 1; i < leP; ++i) {
    for (int j = 0; j < i; ++j) {
      for (int n = 1; n < i - j; ++n) {
        b[j + 1][i] += (((i - n) * seriesExtend(_beta, betaOrd, n - 1) -
                         seriesExtend(_gammaMass, gammaMOrd, n - 1)) *
                            b[j][i - n] +
                        (j + 1) * seriesExtend(_gammaMass, gammaMOrd, n - 1) *
                            b[j + 1][i - n]) /
                       kPow4[n];
      }

      b[j + 1][i] =
          2 *
          (b[j + 1][i] + (j * seriesExtend(_beta, betaOrd, i - j - 1) -
                          seriesExtend(_gammaMass, gammaMOrd, i - j - 1)) *
                             b[j][j] / kPow4[i - j]) /
          (j + 1);
    }
  }

  doubleG coef(leP, length);
  for (int i = 0; i < leP; ++i) {
    copy(b.rowBegin(i) + 1, b.rowEnd(i), coef.rowBegin(i));
  }

  return coef;
}

//------------------------------------------------------------------------------
double AnDim::sCoef(int n, int betaOrd, int gammaROrd, MScheme sType,
                    bool isStrict, int p) const {
  if ((isStrict == false && (n >= gammaROrd || n >= betaOrd - 1) && n > 0) ||
      n < 0) {
    return 0.0;
  }

  int key = buildSKey(betaOrd, gammaROrd, sType);

  while (unsigned(n) >= _sMap[key]._sCoef[p].size()) {
    setNextsCoef(betaOrd, gammaROrd, sType, p);
  }
  return _sMap[key]._sCoef[p][n];
}

//------------------------------------------------------------------------------
double AnDim::sCoefLambda(int n, int betaOrd, int gammaROrd, MScheme sType,
                          bool isStrict, int p) const {
  if ((isStrict == false && (n >= gammaROrd || n >= betaOrd - 1) && n > 0) ||
      n < 0) {
    return 0.0;
  }

  int key = buildSKey(betaOrd, gammaROrd, sType);

  while (unsigned(n) >= _sMap[key]._sCoefLambda[p].size()) {
    setNextsCoefLambda(betaOrd, gammaROrd, sType, p);
  }
  return _sMap[key]._sCoefLambda[p][n];
}

}  // namespace revo
