////////////////////////////////////////////////////////////////////////////////
/// \file AnDimCollection.cpp
/// \brief Implementation of class AnDimCollection
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "AnDimCollection.h"

//#include <iostream>
#include <utility>

using namespace std;

namespace revo {

//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
AnDimCollection::AnDimCollection(int n0, int nTot, MScheme scheme,
                                 double precisionGoal) {
  for (int i = n0; i <= nTot; ++i) {
    _andimMap.emplace(std::piecewise_construct, std::forward_as_tuple(i),
                      std::forward_as_tuple(i, scheme, precisionGoal));
  }
  updatePointer();
}

//------------------------------------------------------------------------------
AnDimCollection::AnDimCollection(const AnDimCollection& other)
    : _andimMap{other._andimMap} {
  updatePointer();
}

//------------------------------------------------------------------------------
AnDimCollection::AnDimCollection(AnDimCollection&& other)
    : _andimMap{std::move(other._andimMap)} {
  updatePointer();
}

//------------------------------------------------------------------------------
// Assignments operators
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
AnDimCollection& AnDimCollection::operator=(const AnDimCollection& other) {
  if (this != &other) {
    _andimMap = other._andimMap;
    updatePointer();
  }
  return *this;
}

//------------------------------------------------------------------------------
AnDimCollection& AnDimCollection::operator=(AnDimCollection&& other) {
  if (this != &other) {
    _andimMap = std::move(other._andimMap);
    updatePointer();
  }
  return *this;
}

//------------------------------------------------------------------------------
// Public member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
const AnDim& AnDimCollection::at(int nf) const { return _andimMap.at(nf); }

//------------------------------------------------------------------------------
int AnDimCollection::count(int nf) const { return _andimMap.count(nf); }

//------------------------------------------------------------------------------
const AnDim& AnDimCollection::operator[](int nf) const { return at(nf); }

//------------------------------------------------------------------------------
// Private member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void AnDimCollection::updatePointer() {
  apply(&AnDim::provideAndimMap, &_andimMap);
}

}  // namespace revo
