////////////////////////////////////////////////////////////////////////////////
/// \file Interpol.cpp
/// \brief Implementation of class Interpol
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#define _USE_MATH_DEFINES

#include "Interpol.h"

#include <cmath>
//#include <iostream>

namespace rac {

//------------------------------------------------------------------------------
// Constructor
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
Interpol::Interpol(double xStart, double xDelta, const std::vector<double>& y,
                   double precisionGoal, int maxIter)
    : _maxIter{maxIter},
      _precisionGoal{precisionGoal},
      _ny{int(y.size())},
      _xStart{xStart},
      _xDelta{xDelta},
      _y{y} {}

//------------------------------------------------------------------------------
// Public member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
double Interpol::operator()(double x) const {
  x = xRescale(x);
  int x0(x);

  if (x < 0) {
    --x0;
  }

  if (x0 <= 0) {
    x0 = 1;
  } else if (x0 >= _ny - 2) {
    x0 = _ny - 3;
  }

  return _y[x0 - 1] * f1(x - x0 + 1) + _y[x0] * f2(x - x0) +
         _y[x0 + 1] * f2(-(x - x0 - 1)) + _y[x0 + 2] * f1(-(x - x0 - 2));
}

//------------------------------------------------------------------------------
double Interpol::d(double x) const {
  x = xRescale(x);
  int x0(x);

  if (x < 0) {
    --x0;
  }

  if (x0 <= 0) {
    x0 = 1;
  } else if (x0 >= _ny - 2) {
    x0 = _ny - 3;
  }

  return (_y[x0 - 1] * d_f1(x - x0 + 1) + _y[x0] * d_f2(x - x0) -
          _y[x0 + 1] * d_f2(-(x - x0 - 1)) - _y[x0 + 2] * d_f1(-(x - x0 - 2))) /
         _xDelta;
}

//------------------------------------------------------------------------------
double Interpol::dd(double x) const {
  x = xRescale(x);
  int x0(x);

  if (x < 0) {
    --x0;
  }

  if (x0 <= 0) {
    x0 = 1;
  } else if (x0 >= _ny - 2) {
    x0 = _ny - 3;
  }

  return (_y[x0 - 1] * dd_f1(x - x0 + 1) + _y[x0] * dd_f2(x - x0) +
          _y[x0 + 1] * dd_f2(-(x - x0 - 1)) +
          _y[x0 + 2] * dd_f1(-(x - x0 - 2))) /
         _xDelta / _xDelta;
}

//------------------------------------------------------------------------------
double Interpol::inverse(double y, double x0) const {
  bool yNear0 = std::abs(y) < _precisionGoal;

  double y0 = operator()(x0);
  for (int i = 0; i < _maxIter; ++i) {
    x0 -= (y0 - y) / d(x0);
    y0 = operator()(x0);
    if ((!yNear0 && std::abs(1 - y0 / y) < _precisionGoal) ||
        (yNear0 && std::abs(y - y0) < _precisionGoal)) {
      break;
    }
  }

  return x0;
}

//------------------------------------------------------------------------------
double Interpol::extr(double x0) const {
  double checker = x0;
  double y0 = d(x0);
  for (int i = 0; i < _maxIter; ++i) {
    x0 -= y0 / dd(x0);
    if (std::abs(1 - checker / x0) < _precisionGoal) {
      break;
    } else {
      checker = x0;
      y0 = d(x0);
    }
  }

  return x0;
}

//------------------------------------------------------------------------------
// Private member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
double Interpol::xRescale(double x) const { return (x - _xStart) / _xDelta; }

//------------------------------------------------------------------------------
double Interpol::f1(double x) const {
  x -= 2;
  return 0.16666666666666667 * x * (1 - x * x);
}

//------------------------------------------------------------------------------
double Interpol::f2(double x) const {
  --x;
  return x * (-1 + 0.5 * x * (1 + x));
}

//------------------------------------------------------------------------------
double Interpol::d_f1(double x) const {
  return -1.8333333333333333 + 2 * x - 0.5 * x * x;
}

//------------------------------------------------------------------------------
double Interpol::d_f2(double x) const { return -0.5 - 2 * x + 1.5 * x * x; }

//------------------------------------------------------------------------------
double Interpol::dd_f1(double x) const { return 2 - x; }

//------------------------------------------------------------------------------
double Interpol::dd_f2(double x) const { return -2 + 3 * x; }

}  // namespace rac
