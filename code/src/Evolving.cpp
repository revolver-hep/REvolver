////////////////////////////////////////////////////////////////////////////////
/// \file Evolving.cpp
/// \brief Implementation of class Evolving
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include "Evolving.h"

using namespace std;

namespace revo {

//------------------------------------------------------------------------------
// Constructors
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
Evolving::Evolving(int runR, int pR, double lambdaR, RunMethod methodR)
    : _runR{runR}, _powerR{pR}, _lambdaR{lambdaR}, _methodR{methodR} {}

//------------------------------------------------------------------------------
// Protected member functions
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void Evolving::applyRParameters() {
  bool sIsStrict;
  switch (_methodR) {
    case RunMethod::Expanded:
    case RunMethod::ExpandedPF:
      sIsStrict = false;
      break;

    case RunMethod::Exact:
    case RunMethod::ExactPF:
    default:
      sIsStrict = true;
      break;
  }

  alpha()._andim.apply(&AnDim::setLambdaR, _lambdaR);
  alpha()._andim.apply(&AnDim::setMsrProp, _runR, sIsStrict);
}

//------------------------------------------------------------------------------
double Evolving::renormalonN(int nl, const doubleV* sCoe) const {
  return alpha().andim(nl).Np2(_powerR, alpha().runOrd(), _runR, sCoe);
}

//------------------------------------------------------------------------------
double Evolving::renormalonP(int nl, const doubleV* sCoe) const {
  return alpha().andim(nl).Pp2(_powerR, alpha().runOrd(), _runR, sCoe);
}

}  // namespace revo
