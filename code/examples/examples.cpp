////////////////////////////////////////////////////////////////////////////////
/// \file examples.cpp
/// \brief Demonstration of some REvolver features with concrete examples
/// \author Andre Hoang, Christopher Lepenik, Vicent Mateu
///
/// In this program we demonstrate some of the features provided by
/// REvolver in a number of concrete examples as they may arise in
/// practical circumstances. For more detailed explanations please
/// consider reading the dedicated article.
///
// Copyright:
//    Copyright (C) 2020 Andre Hoang, Christopher Lepenik, Vicent Mateu
//
//    This file is part of REvolver.
//
//    REvolver is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    REvolver is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with REvolver.  If not, see <https://www.gnu.org/licenses/>.
////////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "REvolver.h"
#include "workspace.hpp"

using namespace std;
using namespace revo;

// Helper function to easily print numbers
void print(double x, int numBlanks = 1) {
  cout << x << "\n";
  for (int i = 1; i < numBlanks; ++i) {
    cout << "\n";
  }
  return;
}

// Collects the parameters for JaIntegrand
struct JaParams {
  int n;
  double mTau;
  const Core& c;
  bool imPart;
};

// Integrand of Ja
double JaIntegrand(double phi, JaParams* params) {
  complex<double> y = exp(complex<double>(0.0, 1.0) * phi);
  complex<double> res =
      pow(1.0 - y, 3) * (1.0 + y) *
      pow(params->c.alpha(params->mTau * sqrt(-y)) / M_PI, params->n) / 2.0 /
      M_PI;
  if (params->imPart) {
    return res.imag();
  } else {
    return res.real();
  }
}

// Function Ja as needed by some examples
complex<double> Ja(int n, double mTau, const Core& c) {
  double resR, resI;

  Workspace<double> ws(1024);
  JaParams paramsR{n, mTau, c, false};
  JaParams paramsI{n, mTau, c, true};
  Function<double, JaParams> integrandR(JaIntegrand, &paramsR);
  Function<double, JaParams> integrandI(JaIntegrand, &paramsI);
  try {
    double err;
    ws.qag(integrandR, 0.0, 2.0 * M_PI, 1e-10, 1e-10, resR, err);
    ws.qag(integrandI, 0.0, 2.0 * M_PI, 1e-10, 1e-10, resI, err);
  } catch (const char* reason) {
    std::cerr << reason << std::endl;
    return complex<double>(0.0);
  }

  return complex<double>(resR, resI);
}

// Collects the parameters for IAlphaIntegrand
struct IAlphaParams {
  double mu0;
  const Core& c;
  bool imPart;
};

// Integrand of IAlpha
double IAlphaIntegrand(double phi, IAlphaParams* params) {
  complex<double> y = exp(complex<double>(0.0, 1.0) * phi);
  complex<double> res = params->c.alpha(params->mu0 + y) / 2.0 / M_PI;
  if (params->imPart) {
    return res.imag();
  } else {
    return res.real();
  }
}

// Function IAlpha as needed by some examples
complex<double> IAlpha(double mu0, const Core& c) {
  double resR, resI;

  Workspace<double> ws(1024);
  IAlphaParams paramsR{mu0, c, false};
  IAlphaParams paramsI{mu0, c, true};
  Function<double, IAlphaParams> integrandR(IAlphaIntegrand, &paramsR);
  Function<double, IAlphaParams> integrandI(IAlphaIntegrand, &paramsI);
  try {
    double err;
    ws.qag(integrandR, 0.0, 2.0 * M_PI, 1e-10, 1e-10, resR, err);
    ws.qag(integrandI, 0.0, 2.0 * M_PI, 1e-10, 1e-10, resI, err);
  } catch (const char* reason) {
    std::cerr << reason << std::endl;
    return complex<double>(0.0);
  }

  return complex<double>(resR, resI);
}

// Collects the parameters for IMassIntegrand
struct IMassParams {
  double mu0;
  const Core& c;
  int nf;
  double rad;
  bool imPart;
};

// Integrand of IMass
double IMassIntegrand(double phi, IMassParams* params) {
  complex<double> y = exp(complex<double>(0.0, 1.0) * phi);
  complex<double> res =
      params->c.masses().mMS(6, params->mu0 + params->rad * y, params->nf) /
      2.0 / M_PI;
  if (params->imPart) {
    return res.imag();
  } else {
    return res.real();
  }
}

// Function IMass as needed by some examples
complex<double> IMass(double mu0, double rad, int nf, const Core& c) {
  double resR, resI;

  Workspace<double> ws(1024);
  IMassParams paramsR{mu0, c, nf, rad, false};
  IMassParams paramsI{mu0, c, nf, rad, true};
  Function<double, IMassParams> integrandR(IMassIntegrand, &paramsR);
  Function<double, IMassParams> integrandI(IMassIntegrand, &paramsI);
  try {
    double err;
    ws.qag(integrandR, 0.0, 2.0 * M_PI, 1e-10, 1e-10, resR, err);
    ws.qag(integrandI, 0.0, 2.0 * M_PI, 1e-10, 1e-10, resI, err);
  } catch (const char* reason) {
    std::cerr << reason << std::endl;
    return complex<double>(0.0);
  }

  return complex<double>(resR, resI);
}

int main() {
  cout << "\nIn this program we demonstrate some of the features provided by\n"
       << "REvolver in a number of concrete examples as they may arise in\n"
       << "practical circumstances. For more detailed explanations please\n"
       << "consider reading the dedicated article.\n\n\n";

  cout << "# Applications and Pedagogical Examples in REvolver\n\n";

  // Define default values for m_Z and \alpha_s^{(5)}(m_Z)
  double mZdef = 91.187, amZdef = 0.1181;

  cout << "## 6.1. Cores without Massive Quarks\n\n"
       << "### 6.1.1. Strong coupling evolution\n\n";

  // In this first application we demonstrate one possible way to make use of
  // the strong coupling evolution in REvolver by reproducing and analyzing some
  // results of [arXiv:1609.05331], where the CMS collaboration carried out a
  // strong coupling measurement from inclusive jet cross sections in different
  // $p_T$ bins based on 8 TeV LHC data. In that publication, the evolution of
  // the strong coupling from different values of $Q$ (the average $p_T$ in the
  // bins) to $m_Z$ was carried out with $n_F=5$ active flavors and 2-loop
  // accuracy. In the following, we focus on the result
  // $\alpha_s(m_Z)=0.0822_{-0.0031}^{+0.0034}$ for $Q=1508.04$ GeV shown in
  // Table 5 of that article.

  // In a first step the relevant values can be defined by

  double aQCentral = 0.0822, Q = 1508.04;
  double aQMin = aQCentral - 0.0031, aQMax = aQCentral + 0.0034;
  RunPar alphaPar(5, aQCentral, Q);

  // and the respective Cores for the central, upper and lower strong coupling
  // values, setting the strong-coupling evolution to 2-loop order, can be
  // created by

  Core central2(alphaPar, 2);
  Core min2(RunPar(5, aQMin, Q), 2);
  Core max2(RunPar(5, aQMax, Q), 2);

  // The central value of $\alpha_s(m_Z)$ can then be extracted by

  double amZCentral2 = central2.alpha(mZdef);
  print(amZCentral2, 2);

  // coinciding with $\alpha_s(m_Z)=0.1162$, as given in [arXiv:1609.05331].
  // Likewise the quoted uncertainties [+0.0070, -0.0062] are easily reproduced
  // by executing

  print(max2.alpha(mZdef) - amZCentral2);
  print(min2.alpha(mZdef) - amZCentral2, 2);

  // For comparison, we also employ 5-loop evolution for the strong coupling,
  // resulting in equivalent numbers after rounding:

  Core central5(RunPar(5, aQCentral, Q), 5);
  Core min5(RunPar(5, aQMin, Q), 5);
  Core max5(RunPar(5, aQMax, Q), 5);

  double amZCentral5 = central5.alpha(mZdef);
  print(amZCentral5);
  print(max5.alpha(mZdef) - amZCentral5);
  print(min5.alpha(mZdef) - amZCentral5, 2);

  // Note that the specification of the running order runAlpha=5 at Core
  // creation is not mandatory since 5-loop running is the default. We show the
  // specification to be explicit.

  // We now have a look at the perturbative uncertainty of these results. One
  // approach to estimate the perturbative uncertainty of the given 2-loop
  // result is to consider the difference of the values obtained by 2-loop and
  // 1-loop evolution. This can be easily computed using REvolver by creating a
  // new Core with specified 1-loop coupling evolution:

  Core central1(RunPar(5, aQCentral, Q), 1);
  print(amZCentral2 - central1.alpha(mZdef), 2);

  // The obtained conservative perturbative error estimate is about 25% of the
  // stated experimental error.
  // For the highest available perturbative order for running, 5 loop, this
  // proportion shrinks to 0.005%:

  Core central4(RunPar(5, aQCentral, Q), 4);
  print(amZCentral5 - central4.alpha(mZdef), 2);

  // A different approach to estimate the perturbative error of
  // $\alpha_s^{(5)}(m_Z)$  is to vary the $\beta$-function scaling parameter
  // $\lambda$.

  // Assuming 10% to be the appropriate range to estimate the perturbative
  // uncertainty of the 5-loop result, the error can be estimated by scanning
  // over $\lambda$ values. Employing REvolver for this task, we first define a
  // list of 20 logarithmically distributed values of $\lambda$ in the
  // appropriate range

  vector<double> lamList;
  for (int i = -10; i <= 10; ++i) {
    lamList.push_back(pow(1.1, i / 10.0));
  }

  // and create one Core for each value in $\lambda$

  vector<Core> runList;
  for (double lam : lamList) {
    runList.emplace_back(RunPar(5, aQCentral, Q), 5, lam);
  }

  // The span of values of $\alpha_s(m_Z)$ corresponding to the range in
  // $\lambda$ can then be obtained with

  vector<double> alamList;
  for (const Core& c : runList) {
    alamList.push_back(c.alpha(mZdef));
  }

  // The central value of this range and the associated error are consequently
  // given by

  print((*max_element(alamList.begin(), alamList.end()) +
         *min_element(alamList.begin(), alamList.end())) /
        2.0);
  print((*max_element(alamList.begin(), alamList.end()) -
         *min_element(alamList.begin(), alamList.end())) /
            2.0,
        2);

  // giving the same order of magnitude as the conservative approach.

  cout << "### 6.1.2. Complex renormalization scales\n\n";

  // In this application we illustrate the REvolver functionality to determine
  // the strong coupling $\alpha_s(\mu)$ at complex scales $\mu$ and the ability
  // to deal with user-defined $\beta$-function coefficients. To this end we
  // consider the analyses in Refs. [arXiv:0806.3156, arXiv:1606.06175], where
  // the perturbative QCD corrections $\delta^{(0)}$ to the inclusive $\tau$
  // hadronic width were considered in fixed-order perturbation theory (FOPT) as
  // well as contour-improved perturbation theory (CIPT). In the second
  // reference a scheme different from the usual MSbar definition is used for
  // $\alpha_s$. We exploit this fact to demonstrate the ability of REvolver to
  // deal with user-defined $\beta$-function coefficients.

   cout << "#### Hadronic $\\tau$ decay\n\n";

  // To reproduce the numbers for $\delta_{\rm CI}^{(0)}$ given in Eq. (3.9) of
  // Ref. [arXiv:0806.3156] we first define variables accounting for the
  // employed input values

  int nfa = 3;
  double aTau = 0.34, mTau = 1.77686;
  doubleV cn1 = {1.0, 1.640, 6.371, 49.076, 283.0};

  // and a function to compute $J_n^{a}(m_\tau^2)$, see the fuction Ja at the
  // beginning of this file.

  // Furthermore, we define a Core with 4-loop running for the coupling

  Core CI4(RunPar(nfa, aTau, mTau), 4);

  // All requirements are now set to reproduce the numbers given in Eq. (3.9) of
  // Ref. [arXiv:0806.3156]

  for (int n = 1; n <= 5; ++n) {
    complex<double> JaRes = Ja(n, mTau, CI4);
    print(cn1[n - 1] * JaRes.real());
    print(cn1[n - 1] * JaRes.imag());
  }
  cout << "\n";

  // with full agreement.

  // We can now easily inspect the corrections induced by the 5-loop running of
  // the strong coupling by creating an appropriate Core

  Core CI5(RunPar(nfa, aTau, mTau), 5);

  // and evaluating

  for (int n = 1; n <= 5; ++n) {
    complex<double> JaRes = Ja(n, mTau, CI5);
    print(cn1[n - 1] * JaRes.real());
    print(cn1[n - 1] * JaRes.imag());
  }
  cout << "\n";

  // We observe a small negative $\mathcal O(1\%)$ shift in the individual
  // coefficients. Note that the explicit specification of the running order
  // runAlpha=5 at Core creation is not mandatory since 5-loop running is the
  // default. We included it to be explicit.

  cout << "#### Hadronic $\\tau$ decay with C-scheme strong coupling\n\n";

  // We now turn to Ref. [arXiv:1606.06175] for which we reproduce the value
  // quoted for $\delta_{\rm CI}^{(0)}$ in Eq. (22). To obtain this number the
  // 3-flavor strong coupling, the related $\beta$-function coefficients and the
  // coefficients $c_{n,1}$ were converted to a class of schemes for the strong
  // coupling, called C-schemes.

  // Within that class of schemes one needs to fix a parameter C to uniquely
  // specify the strong coupling. This parameter was set to C=-1.246 in Eq. (22)
  // of Ref. [arXiv:1606.06175] with the argument that the unknown 5-loop
  // coefficient can then be neglected. Solving Eq. (6) of the reference paper
  // to convert the quoted value of the MSbar strong coupling to the scheme
  // described above leads to $\hat \alpha_s(m_\tau, C=-1.246)=0.477$. The
  // transformed coefficients $c_{n,1}$ can be extracted from Eq. (12) therein.
  // REvolver allows the order-by-order specification of user-defined $\beta$
  // functions, so we expand $\hat\beta(\hat\alpha_s)$ up to
  // $\mathcal{O}(\hat\alpha_s^{11})$.

  // We define the relevant input values related to \hat\alpha_s, the
  // coefficients $c_{n,1}$ and $\hat\beta_i$ for $n_f=3$ with

  nfa = 3, aTau = 0.477, mTau = 1.77686;

  double c = -1.246;
  cn1 = {1, 1.640 + 2.25 * c, 7.682 + 11.38 * c + 5.063 * c * c,
         61.06 + 72.08 * c + 47.4 * c * c + 11.39 * c * c * c};

  doubleV betCustom;
  double betaTmp = 9.0;
  for (int i = 0; i < 10; ++i) {
    betCustom.push_back(betaTmp);
    betaTmp *= 64.0 / 9.0;
  }

  // and the related Core

  Core hat(RunPar(nfa, aTau, mTau), 5, 1., betCustom);

  // Reusing the function Ja from the previous example we obtain

  complex<double> res(0.0);
  for (int n = 1; n <= 4; ++n) {
    res += cn1[n - 1] * Ja(n, mTau, hat);
  }
  print(res.real());
  print(res.imag(), 2);

  // for $\delta_{\rm CI}^{(0)}(\hat \alpha_s, C = -1.246)$ and

  res = Ja(4, mTau, hat) * cn1[3];
  print(res.real());
  print(res.imag(), 2);

  // for the last correction term. Both the central value and the size of the
  // last correction term are in perfect agreement with Ref. [arXiv:1606.06175].

  cout << "#### Cauchy integral theorem for the strong coupling\n\n";

  // It is worth mentioning that Cauchy's integral formula can be utilized to
  // check the numerical quality of the coupling evolution routine implemented
  // in REvolver. For example, valuating $\alpha_s^{(3)}(2\,{\rm GeV})$ directly
  // as well as by employing Cauchy's integral formula with a radius of 1 GeV
  // gives equivalent results up to machine precision

  double mu0 = 2.0;
  double aDirect = CI5.alpha(mu0);
  complex<double> aResidue = IAlpha(mu0, CI5);
  print(aDirect);
  print(aResidue.real());
  print(aResidue.imag());
  print(aResidue.real() - aDirect, 2);

  // For all practical purposes the solution for the strong coupling evolution
  // provided by REvolver based on a given QCD $\beta$-function can be
  // considered as exact.

  cout << "### 6.2. Cores with one Massive Quark\n\n";

  // In the previous sample applications the impact of flavor thresholds was
  // not considered. In the following we discuss examples where threshold
  // effects associated to one massive quark are accounted for.

  cout << "### 6.2.1. Strong coupling with a flavor threshold\n\n"
       << "#### Strong coupling from multijet events\n\n";

  // We consider Ref. [arXiv:1707.02562], where the strong coupling value was
  // determined by the ATLAS collaboration from transverse energy-energy
  // correlations in multijet events based on 8 TeV LHC data. The measurements
  // were made for different values of the transverse momentum sum $H_{T2}$ of
  // the two leading jets. All values of $H_{T2}$ considered in that analysis
  // were much larger than the top quark mass, consequently the associated
  // respective strong coupling values $\alpha_s^{(6)}(Q)$ are defined in the
  // 6-flavor scheme. In Tabs. 2 and 3 of Ref. [arXiv:1707.02562] values for
  // $\alpha_s^{(6)}(Q)$ and the associated results for $\alpha_s^{(5)}(m_Z)$
  // are quoted. In the following, we focus on the associated results
  // $\alpha_s^{(5)}(m_Z)=0.1186_{-0.0047}^{+0.0090}$ and
  // $\alpha_s^{(6)}(810\,{\rm GeV})=0.0907_{-0.0026}^{+0.0052}$, where for
  // simplicity, we added the respective upper and lower uncertainties in
  // quadrature. We reproduce the asymmetric uncertainties for
  // $\alpha_s^{(6)}(810\,{\rm GeV})$ for the given $\alpha_s^{(5)}(m_Z)$ range
  // and start by defining the relevant parameters for the 5-flavor coupling
  // $\alpha_s^{(5)}(m_Z)$, where we set the top quark standard running mass to
  // $m_t$=163 GeV

  double amZCentral = 0.1186, mtmt = 163;
  Q = 810;
  double amZMax = amZCentral + 0.0090, amZMin = amZCentral - 0.0047;

  // Next we create the Cores for our evaluation

  RunParV mPar = {RunPar(6, mtmt, mtmt)};
  doubleV fMatch = {1.0};
  central2 = Core(6, RunPar(5, amZCentral, mZdef), mPar, fMatch, 2);
  max2 = Core(6, RunPar(5, amZMax, mZdef), mPar, fMatch, 2);
  min2 = Core(6, RunPar(5, amZMin, mZdef), mPar, fMatch, 2);

  // To obtain the 6-flavor strong coupling values at $Q = 810$ GeV including
  // the error range we simply evaluate

  double aQCentral2 = central2.alpha(Q, 6);
  print(aQCentral2);
  print(min2.alpha(Q, 6) - aQCentral2);
  print(max2.alpha(Q, 6) - aQCentral2, 2);

  // agreeing very well with the result given in Ref. [arXiv:1707.02562].

  // We investigate this setup with two-loop evolution further by determining
  // the uncertainty related to varying the matching scale accounting for the
  // top threshold corrections at one loop.  This is easily done using REvolver
  // by creating a set of Cores with a range of f-parameters. To this end we
  // first define a table containing 20 logarithmically scaled f-parameters in
  // the range (1/2, 2), which corresponds to matching scales between one half
  // and twice the standard running top mass. Subsequently the table is used to
  // create the corresponding set of Cores

  vector<Core> alphaList2;
  for (int i = -10; i <= 10; ++i) {
    fMatch[0] = pow(2.0, i / 10.0);
    alphaList2.emplace_back(6, RunPar(5, amZCentral, mZdef), mPar, fMatch, 2, 1,
                            1);
  }

  // Finally, we can determine a list of the corresponding strong coupling
  // values $\alpha_s^{(6)}(810\,{\rm GeV})$ and compute the central value as
  // well as the error range

  doubleV aQlist2;
  for (const Core& core : alphaList2) {
    aQlist2.push_back(core.alpha(Q, 6));
  }

  print((*max_element(aQlist2.begin(), aQlist2.end()) +
         *min_element(aQlist2.begin(), aQlist2.end())) /
        2.0);
  print((*max_element(aQlist2.begin(), aQlist2.end()) -
         *min_element(aQlist2.begin(), aQlist2.end())) /
            2.0,
        2);

  // The perturbative uncertainties associated to the threshold corrections is
  // about 3% of the quoted experimental error.

  // Employing 5-loop instead of 2-loop running for the strong coupling and
  // 4-loop matching corrections we obtain

  central5 = Core(6, RunPar(5, amZCentral, mZdef), mPar);
  max5 = Core(6, RunPar(5, amZMax, mZdef), mPar);
  min5 = Core(6, RunPar(5, amZMin, mZdef), mPar);

  double aQCentral5 = central5.alpha(Q, 6);
  print(aQCentral5);
  print(min5.alpha(Q, 6) - aQCentral5);
  print(max5.alpha(Q, 6) - aQCentral5, 2);

  // for $\alpha_s^{(6)}(810\,{\rm GeV})$ and its upper and lower uncertainty
  // and

  vector<Core> alphaList5;
  for (int i = -10; i <= 10; ++i) {
    fMatch[0] = pow(2.0, i / 10.0);
    alphaList5.emplace_back(6, RunPar(5, amZCentral, mZdef), mPar, fMatch);
  }

  doubleV aQlist5;
  for (const Core& core : alphaList5) {
    aQlist5.push_back(core.alpha(Q, 6));
  }

  print((*max_element(aQlist5.begin(), aQlist5.end()) +
         *min_element(aQlist5.begin(), aQlist5.end())) /
        2.0);
  print((*max_element(aQlist5.begin(), aQlist5.end()) -
         *min_element(aQlist5.begin(), aQlist5.end())) /
            2.0,
        2);

  // for the central value and error estimate derived from varying the matching
  // scale.

  cout << "#### Strong coupling from inclusive jet cross sections\n\n";

  // We return to the analysis of Ref. [arXiv:1609.05331] and investigate the
  // impact of a top quark threshold on the values given there, staying with
  // 2-loop running as employed in that reference. In the following we create
  // Cores with the given range of values of
  // $\alpha_s^{(5)}(m_Z)=0.1162_{-0.0062}^{0.0070}$ and determine the strong
  // coupling $\alpha_s^{(6)}(1508.04\,{\rm GeV})$ in the 6-flavor scheme
  // instead of the  5-flavor coupling $\alpha_s^{(5)}(1508.04\,{\rm GeV})$
  // determined in  Ref. [arXiv:1609.05331].

  // After defining the parameters

  amZCentral = 0.1162, Q = 1508.04, mtmt = 163;
  amZMax = amZCentral + 0.007;
  amZMin = amZCentral - 0.0062;

  // and creating the respective Cores, accounting for the standard running top
  // quark mass $\overline m_t$=163 GeV and default 4-loop matching

  mPar = {RunPar(6, mtmt, mtmt)};
  fMatch = {1.0};

  central2 = Core(6, RunPar(5, amZCentral, mZdef), mPar, fMatch, 2);
  max2 = Core(6, RunPar(5, amZMax, mZdef), mPar, fMatch, 2);
  min2 = Core(6, RunPar(5, amZMin, mZdef), mPar, fMatch, 2);

  // we obtain

  aQCentral2 = central2.alpha(Q, 6);
  print(aQCentral2);
  print(min2.alpha(Q, 6) - aQCentral2);
  print(max2.alpha(Q, 6) - aQCentral2, 2);

  // for the central value and the upper and lower uncertainties, respectively.
  // Comparing these values to
  // $\alpha_s^{(6)}(1508.04\,{\rm GeV})=0.0822_{-0.0031}^{+0.0034}$,
  // quoted in Ref. [arXiv:1609.05331], we observe a positive shift of about
  // 0.002 in the central value. This already amounts to about 60% of the given
  // experimental error, illustrating that flavor-thresholds effects can lead to
  // significant changes.

  cout << "#### 6.2.2. Asymptotic pole and low-scale MSR mass\n\n";

  // To demonstrate the REvolver functionalities related to mass conversions
  // accounting for flavor threshold effects, we investigate how much the
  // asymptotic top quark pole mass $m_t^{\rm pole}$ as well as the top MSR mass
  // at 2 GeV $m_t^{\rm MSR}(2\,{\rm GeV})$ would change if the strong coupling
  // value quoted in Ref. [arXiv:1609.05331]
  // $\alpha_s(1508.04\,{\rm GeV})=0.0822_{-0.0031}^{+0.0034}$ would be
  // interpreted as a 6-flavor compared to a 5-flavor result.

  // After defining the relevant constants and Cores

  double aQ = 0.0822;

  Core core5(6, RunPar(5, aQ, Q), mPar);
  Core core6(6, RunPar(6, aQ, Q), mPar);

  // where we have employed default highest-order precision for coupling and
  // mass evolution and flavor threshold matching, we first extract
  // $m_t^{\rm MSR}(2\,{\rm GeV})$ from both Cores and then determine their
  // difference:

  double mMSR25 = core5.masses().mMS(6, 2.0);
  double mMSR26 = core6.masses().mMS(6, 2.0);
  print(mMSR25);
  print(mMSR26);
  print(mMSR25 - mMSR26, 2);

  double mPole5 = core5.masses().mPole(6, mtmt);
  double mPole6 = core6.masses().mPole(6, mtmt);
  print(mPole5);
  print(mPole6);
  print(mPole5 - mPole6, 2);

  // where we adopted $\overline m_t$=163 GeV as the conversion scale for the
  // asymptotic pole mass determination. The differences we obtain amount to
  // 358 MeV and 386 MeV, both of which exceed the uncertainty of the current
  // world average for direct top mass measurements.

  cout << "### 6.2.3. Bottom and charm quark short-distance masses\n\n";

  cout << "#### Bottom MSbar mass at high scales\n\n";

  // In Ref. [arXiv:2004.04752] the partonic cross section for Higgs production
  // via bottom quark fusion was presented. This involves the bottom running
  // mass at the Higgs scale $m_H=125.09\,$GeV, quoted to be
  // $m_b^{(5)}(m_H)=2.79\,$GeV using 4-loop MSbar-mass running,
  // $\alpha_s^{(5)}=0.118$ and the bottom quark standard running mass
  // $\overline m_b$=4.18 GeV, see Tab. 2 therein. We can reproduce this
  // relation in REvolver by defining the relevant parameters

  double mH = 125.09, amZ = 0.118, mbmb = 4.18;

  // and creating a corresponding Core

  mPar = {RunPar(5, mbmb, mbmb)};
  fMatch = {1.0};
  Core coreb(5, RunPar(5, amZ, mZdef), mPar, fMatch, 5, 1, 4, 4);

  // The value of $m_b^{(5)}(m_H)$ can now be extracted with

  print(coreb.masses().mMS(5, mH), 2);

  // showing perfect agreement with Ref. [arXiv:2004.04752].

  cout << "#### Bottom quark PS and 1S masses\n\n";

  // In Ref. [arXiv:1502.01030] the conversion between the MSbar and various
  // low-scale short-distance masses was carried out with 4-loop fixed-order
  // formulae. To illustrate the functionalities of REvolver for mass
  // conversions we now consider the values of the PS and 1S bottom quark masses
  // given in Tab. II of that article. We start defining the input parameters
  // $\alpha_s^{(5)}=0.1185$, $\overline m_b$=4.163 GeV and $\mu_f$=2.0 GeV for
  // the strong coupling, the standard running bottom mass and the bottom PS
  // mass renormalization scale, respectively, as specified in Ref.
  // [arXiv:1502.01030].

  amZ = 0.1185, mbmb = 4.163;
  double mufB = 2.0, mbPSRef = 4.483;

  // and the related Core

  coreb = Core(5, RunPar(5, amZ, mZdef), {RunPar(5, mbmb, mbmb)}, {2.0});

  // Following Ref. [arXiv:1502.01030] we set the bottom quark matching scale
  // to twice the standard running bottom mass. Comparing the 4-loop value for
  // the bottom PS mass $m_b^{\rm PS}(2\,{\rm GeV})=4.483\,$GeV quoted in Ref.
  // [arXiv:1502.01030] and obtained by REvolver we obtain

  double mbPSREvo = coreb.masses().mPS(5, mufB, 5, mbmb, mbmb);
  print(mbPSREvo);
  print(mbPSREvo - mbPSRef, 2);

  // where, following Ref. [arXiv:1502.01030], the conversion is carried out
  // using the fixed-order relation between the standard running and PS masses
  // using the standard running mass $\overline m_b$ as the renormalization
  // scale for the strong coupling.

  // The small difference of about 1 MeV results from the fact that in REvolver
  // all conversions between mass schemes are based on formulae starting from
  // the value of the running mass, while in Ref. [arXiv:1502.01030] the
  // conversion was obtained the other way around, i.e. the running mass was
  // computed starting from a value in the PS scheme. The difference is
  // naturally covered by the perturbative uncertainty and not relevant for
  // practical purposes.

  // In the previous example we have used REvolver to determine the PS from the
  // standard running mass $\overline m_b$. The conversion in the opposite
  // direction can be achieved using the REvolver functionality to add a heavier
  // mass to an existing Core. To determine the standard running mass we first
  // create a Core containing 4 massless flavors

  double anf4mu3 = coreb.alpha(3.0, 4);
  Core coreO4(RunPar(4, anf4mu3, 3));

  // which requires the 4-flavor strong coupling $\alpha_s^{(4)}(3\,{\rm GeV})$
  // as an input, which here we obtain from the previously created Core named
  // "coreb".

  // We now add the bottom PS mass $m_b^{\rm PS}(2\,{\rm GeV})=4.483\,$GeV to
  // the Core named "coreO4", reusing the parameters mbPSRef and mufB defined in
  // the previous example as well as the matching scale factor 2.0, specified by
  // the parameter fnQ

  coreO4.addPSMass(mbPSRef, mufB, 5, 4.2, 4.2, 1, 4, 2);

  // Internally, first the 5-flavor running mass is determined and subsequently
  // used to extend the Core. To match precisely the conversion method used in
  // Ref. [arXiv:1502.01030] we set the running mass scale and the
  // renormalization scale of the strong coupling for that conversion to 4.2 GeV
  // here.

  // Reading out the resulting bottom quark standard running mass results in

  print(coreO4.masses().mMS(5), 2);

  // with the expected 1 MeV difference to the value $m_b = 4.163$ GeV
  // quoted in the reference paper. This difference arises here again because
  // the numerical conversion formulae employed in REvolver are exactly
  // invertible, i.e. they produce the same numerical mass differences
  // regardless in which way the conversion is carried out.

  // Note that the default REvolver routines convert from the MSR mass to a
  // low-scale short-distance mass at the intrinsic scale of the low-scale
  // short-distance mass to resum potentially large logarithms involving the
  // ratio of the quark mass and the renormalization scale. In the example
  // above, we have, however, explicitly set the input parameter nfConv of the
  // function mPS to 5 to enforce conversion at the scale of the MSbar mass
  // (which is the approach used in Ref. [arXiv:1502.01030] and which does not
  // resum these logarithms). The result including log-resummation via
  // R-evolution differs by around 15 MeV and can be extracted by using the
  // corresponding REvolver commands with default parameter settings. For
  // example, converting from the standard running mass to the PS mass we obtain

  print(coreb.masses().mPS(5, mufB), 2);

  // where nfConv is automatically set to 4 and R as well as the renormalization
  // scale of the strong coupling are set to mufB.

  // The corresponding (log-resummed) conversion from the PS mass to the
  // standard running mass is achieved by resetting the Core "coreO4" and adding
  // the bottom quark PS mass $m_b^{\rm PS}(2\,{\rm GeV})=4.483\,$ GeV,
  // specifying log resummation via R - evolution:

  coreO4 = Core(RunPar(4, anf4mu3, 3));
  coreO4.addPSMass(mbPSRef, mufB, -1, -1, -1, 1, 4, 2);

  // resulting in a bottom quark standard running mass of

  print(coreO4.masses().mMS(5), 2);

  // At this point it should be mentioned that for bottom quarks large scale
  // hierarchies cannot arise, such that the log-resummed conversion is not
  // superior and the difference between the fixed-order and the log-resummed
  // conversions may be better considered as a scheme variation. To illustrate
  // this we show, order by order, the bottom quark PS mass computed in the
  // fixed-order expansion as well as by utilizing R-evolution:

  for (int n = 1; n <= 4; ++n) {
    print(coreb.masses().mPS(5, mufB, 5, mbmb, mbmb, 1.0, n));
  }
  cout << "\n";
  for (int n = 1; n <= 4; ++n) {
    print(coreb.masses().mPS(5, mufB, 4, mufB, mufB, 1.0, n));
  }
  cout << "\n";

  // Next, we consider the 1S scheme. Comparing the 1S bottom quark mass
  // $m_b^{\rm 1S} = 4.670\,$GeV quoted Ref. [arXiv:1502.01030] and obtained in
  // REvolver using the relativistic counting and conversion at the high scale
  // $\overline m_b$ (which again agrees with the approach used in Ref.
  // [arXiv:1502.01030] and does not account for the resummation of logarithms
  // involving the bottom mass and inverse Bohr radius) we obtain

  double mb1SRef = 4.670;
  double mb1SRevo = coreb.masses().m1S(5, 5, mbmb, Count1S::Relativistic);
  print(mb1SRevo);
  print(mb1SRevo - mb1SRef, 2);

  // Again, the $1$ MeV discrepancy emerges from the perturbative difference
  // between converting from or to the standard running mass. For comparison, we
  // also present the corresponding 1S mass value accounting for log-resummation
  // via R-evolution. This can be achieved by calling the routine m1S without
  // any optional parameters. This implies that the default setting is used,
  // namely that the 1S mass is converted from the MSR mass at the inverse Bohr
  // radius $M_{q,B}$ (the intrinsic scale of the 1S mass) and that
  // non-relativistic counting is applied. The bottom 1S mass is, however,
  // sensitive to the ultra-soft scale $M^2_{q,B}/m_b$, related to logarithms of
  // the strong coupling. This entails that for the conversion from the MSR mass
  // (at the inverse Bohr radius) a scale larger than $M_{q,B}$ should be
  // adopted as the renormalization scale for the strong coupling to avoid
  // perturbative instabilities. Adopting $2M_{q,B}$ for the renormalization
  // scale of the strong coupling, the 1S mass accounting for log-resummation is
  // obtained by

  double MbB = coreb.masses().mBohr(5);
  print(MbB);
  print(coreb.masses().m1S(5, 4, MbB, Count1S::Nonrelativistic, 2 * MbB));

  // The result differs by 4 MeV to the one using fixed-order conversion. As for
  // the bottom PS mass, there is no conceptual improvement using
  // log-resummation. This can again be seen from comparing the 1S mass values
  // at lower orders. Order by order, the values of the bottom quark 1S mass,
  // derived in fixed-order and with R-evolution, respectively, are given by

  for (int n = 1; n <= 4; ++n) {
    print(coreb.masses().m1S(5, 5, mbmb, Count1S::Relativistic, mbmb, n));
  }
  cout << "\n";
  for (int n = 1; n <= 4; ++n) {
    print(coreb.masses().m1S(5, 4, MbB, Count1S::Nonrelativistic, 2 * MbB, n));
  }
  cout << "\n";

  // Of course one can also convert from the 1S scheme to the standard running
  // mass using the routine add1SMass, shortly demonstrated in the following.

  // Redefining the previously used the Core named "coreO4" to containing 4
  // massless quarks, we add the bottom 1S mass using fixed-order conversion

  coreO4 = Core(RunPar(4, anf4mu3, 3));
  coreO4.add1SMass(mb1SRef, 5, mbmb, Count1S::Relativistic, mbmb, 4, 2.0);

  // which results in the following bottom quark standard running mass

  print(coreO4.masses().mMS(5));

  // again with the expected 1 MeV difference to the value $m_b=4.163$ GeV
  // quoted in Ref. [arXiv:1502.01030], related to invertible numerical
  // conversion algorithm employed in REvolver. The analogous procedure for
  // converting the 1S mass to the standard running mass using R-evolution can
  // be performed by executing

  coreO4 = Core(RunPar(4, anf4mu3, 3));
  coreO4.add1SMass(mb1SRef, 4, MbB, Count1S::Nonrelativistic, 2 * MbB, 4, 2.0);

  print(coreO4.masses().mMS(5), 2);

  cout << "#### Charm and bottom quark RS masses\n\n";

  // In Ref. [arXiv:1806.05197] the charm quark RS mass
  // $m_c^{\rm RS}(\mu_f)$=1.202 GeV at the scale $\mu_f$=1 GeV was extracted
  // from charmonium bound states masses, given in Eq. (2.15) of that paper.
  // The RS mass was converted to the standard running charm mass
  // $\overline m_c=1.217\,$GeV with 4-loop fixed-order formulae for
  // $\alpha_s^{(5)}(m_Z)$=0.1184 and using $\mu_c$=1.27 GeV as the charm
  // threshold matching scale. The renormalization scale of the strong coupling
  // was set to Subscript[\[Nu], c]=1.5 GeV and the pole mass renormalon
  // normalization constant for $n_f=3$ active flavors is quoted to be
  // $N_m$=0.5626. This relation can be easily reproduced with REvolver. First,
  // we set the relevant variables

  amZ = 0.1184;
  double Nm = 0.5626;
  double mcmc = 1.217, mcmcMatch = 1.27, nufc = 1.0, nuc = 1.5;
  mbmb = 4.185;
  double mbmbMatch = 4.2;

  // and create the respective Core

  mPar = {RunPar(4, mcmc, mcmc), RunPar(5, mbmb, mbmb)};
  fMatch = {mcmcMatch / mcmc, mbmbMatch / mbmb};
  Core corecb(5, RunPar(5, amZ, mZdef), mPar, fMatch);

  // We include the standard running bottom mass $\overline m_b$=4.185 GeV with
  // the corresponding threshold matching scale $\mu_b$=4.2 GeV to enable
  // automatic matching of the strong coupling constant which is given at
  // $\mu=m_Z$. The charm quark RS mass computed by REvolver and the difference
  // to the value quoted in the reference paper are given by

  double mcRSRef = 1.202;
  double mcRSREvo = corecb.masses().mRS(4, nufc, 4, mcmc, nuc, 4, 4, Nm);
  print(mcRSREvo);
  print(mcRSRef - mcRSREvo, 2);

  // showing agreement at the sub-MeV level.

  // In Ref. [arXiv:1806.05197] also the bottom quark RS mass value
  // $m_b^{\rm RS}(\nu_f = 4.379=4.379\,$GeV at the scale $\nu_f$=2 GeV was
  // extracted from bottomonium meson masses [see Eq. (2.3) of that paper] and
  // subsequently converted to the standard running bottom mass
  // $\overline m_b$=4.379 GeV with 4-loop fixed-order formulae using
  // $\mu_b=4.2\,$GeV as the bottom threshold matching scale and
  // $\nu_b=2.5\,$GeV as the renormalization scale of the strong coupling. The
  // charm mass corrections in the perturbative relation between the RS and the
  // MSbar mass have been implemented in an effective way by evaluating the
  // conversion series that relates the MSbar and the RS masses for the bottom
  // quark and $n_\ell$ massless quarks for $n_\ell$=3 dynamical flavors, but
  // using the 3-flavor strong coupling $\alpha_s^{(3)}$ computed with the charm
  // mass threshold properly accounted for. This treats the charm quark as a
  // decoupled flavor. The REvolver RS mass routines do not directly provide
  // this functionality, but in the following we show how this evaluation can
  // still be carried out in REvolver.

  // We first set the necessary parameters not yet defined before

  double nufb = 2.0, nub = 2.5;

  // and compute the 3-flavor strong coupling $\alpha_s^{(3)}(\mu_c)$ at the
  // charm matching scale $\mu_c$=1.27 GeV, taking into account the bottom as
  // well as charm quark thresholds in the usual way

  double as3 = corecb.alpha(mcmcMatch, 3);

  // Next, we define a Core with decoupled charm quarks by using the precomputed
  // $n_f$=3 strong coupling and specifying a total number of 4 flavors,
  // including 3 massless flavors and the massive bottom quark as the 4-th
  // flavor

  fMatch = {mbmbMatch / mbmb};
  coreb = Core(4, RunPar(3, as3, mcmcMatch), {RunPar(4, mbmb, mbmb)}, fMatch);

  // Setting all previously defined scales we get

  double mbRSRef = 4.379;
  double mbRSREvo = coreb.masses().mRS(4, nufb, 4, mbmb, nub, 4, 4, Nm);
  print(mbRSREvo);
  print(mbRSREvo - mbRSRef, 2);

  // for the bottom quark mass in the RS scheme with a negligible difference to
  // the value quoted in the reference article.

  // We can also convert from the bottom RS to the standard running using the
  // REvolver routine addRSMass. To do that we first create a Core containing
  // the 3 massless non-decoupled flavors with

  Core coreO3(RunPar(3, as3, mcmcMatch));

  // and subsequently add to coreO3 the bottom RS mass setting the various input
  // parameters in analogy to the example above, i.e. using fixed-order
  // conversion

  coreO3.addRSMass(mbRSRef, nufb, 4, 4.2, nub, 4, 4, Nm);

  // Internally, first the 5-flavor running mass is determined and subsequently
  // used to extend the Core "coreO3". We set the running mass scale for that
  // conversion to 4.2 GeV.

  // Extracting the bottom quark standard running mass from the newly created
  // Core results in

  print(coreO3.masses().mMS(4), 2);

  cout << "#### Bottom quark kinetic masses\n\n";

  // In Ref. [arXiv:2005.06487] the 3-loop corrections to the perturbative
  // relation between the pole and kinetic quark mass schemes have been
  // determined for the case of one massive quark and $n_\ell$ massless quarks.
  // Here we show how to employ REvolver to reproduce the values for the bottom
  // quark kinetic mass $m_b^{\rm kin}(\mu)$ at the scale $\mu=1\,$GeV given in
  // Eq. (8) of that paper, obtained by converting from the standard running
  // mass $\overline m_b=4.163\,$GeV.

  // The conversion from $m_b=4.163\,$ to the bottom kinetic mass was considered
  // using the perturbative series computed with the massive bottom quark and
  // $n_\ell$ massless quarks for $n_\ell=4$ (i.e. with the charm quark treated
  // as massless) with the result $m_b^{\rm kin}(\mu)=4.523\,$GeV, as well as
  // for $n_\ell=3$ (i.e. with the charm quark treated as decoupled) with the
  // result $m_b^{\rm kin}(\mu)=4.521\,$GeV. The first case can be reproducedin
  // a straightforward way with REvolver as it corresponds to a realistic
  // physical scenario.

  // First we define the relevant parameters

  mbmb = 4.163, amZ = 0.1179;
  double muCutb = 1.0;

  // and create a Core in which the charm quark is treated as massless

  Core coreb4(5, RunPar(5, amZ, mZdef), {RunPar(5, mbmb, mbmb)});

  // The value obtained by REvolver and the difference to the value quoted in
  // Ref. [arXiv:2005.06487] is

  double mKinb4Ref = 4.523;
  double mKinb4REvo = coreb4.masses().mKin(5, muCutb, 5, mbmb, mbmb);
  print(mKinb4REvo);
  print(mKinb4REvo - mKinb4Ref, 2);

  // i.e. there is perfect agreement.

  // The second case is in close analogy to the treatment of the RS mass just
  // discussed above and requires that the conversion series, which relates the
  // MSbar and the kinetic mass for the bottom quark and $n_\ell$ massless
  // quarks, is evaluated for $n_\ell$=3 dynamical flavors, but using the
  // 3-flavor strong coupling $\alpha_s^{(3)}$ computed with the charm mass
  // threshold properly accounted for. REvolver does not provide functionality
  // to carry out this conversion directly, but in the following we show how
  // this evaluation can still be performed.

  // To obtain the $n_f$=3 strong coupling in the usual way, we first create a
  // new Core involving a massive bottom as well as charm quark

  mcmc = 1.263;
  mPar = {RunPar(4, mcmc, mcmc), RunPar(5, mbmb, mbmb)};
  Core corebc(5, RunPar(5, amZ, mZdef), mPar);

  // and extract $\alpha_s^{(3)}(\overline m_b)$

  double a3mbmb = corebc.alpha(mbmb, 3);

  // Next, we create the Core to be employed for the mass scheme conversion. We
  // use the  computed $n_f$=3 strong coupling and specify a total number of 4
  // flavors with 3 massless flavors and the massive bottom quark

  Core coreb3(4, RunPar(3, a3mbmb, mbmb), {RunPar(4, mbmb, mbmb)});

  // The value of the kinetic bottom quark mass computed by REvolver and the
  // difference to the result quoted in Ref. [arXiv:2005.06487] can now be
  // obtained by

  double mKinb3Ref = 4.521;
  double mKinb3REvo = coreb3.masses().mKin(4, muCutb, 4, mbmb, mbmb);
  print(mKinb3REvo);
  print(mKinb3Ref - mKinb3REvo, 2);

  // The numbers are in perfect agreement.

  // In Ref. [arXiv:2011.11655] lighter massive flavor effects to the relation
  // between the pole and kinetic masses were considered adopting the scheme
  // where these lighter quark mass corrections exclusively come from the flavor
  // number decoupling relations of the strong coupling. This approach (based on
  // what the authors refer to as "scheme B") for the lighter massive quark
  // corrections for the kinetic mass is adopted in REvolver. In Eq. (79) of
  // that reference the bottom quark kinetic mass
  // $m_b^{\rm kin}(1\,{\rm GeV})=4.526\,$GeV was obtained using
  // $\alpha_s^{(5)}(m_Z)=0.1179$ for the strong coupling, the charm quark
  // running mass $\overline m_c(2\,{\rm GeV})=0.993\,$GeV and the bottom quark
  // standard running mass $\overline m_b$=4.136 GeV. For the calculation,
  // fixed-order conversion was applied using the standard running bottom mass
  // $\overline m_b$ as the renormalization scale of the strong coupling. To
  // reproduce the result with REvolver we define the input values with

  amZ = 0.1179, mbmb = 4.163;
  double mc3 = 0.993;

  // and the related Core with

  mPar = {RunPar(4, mc3, 3), RunPar(5, mbmb, mbmb)};
  corebc = Core(5, RunPar(5, amZ, mZdef), mPar);

  // The bottom quark kinetic mass extracted by REvolver and its difference to
  // the reference value are returned by

  double mbKinRef = 4.526;
  double mbKinREvo = corebc.masses().mKin(5, 1.0, 5, mbmb, mbmb);
  print(mbKinREvo);
  print(mbKinRef - mbKinREvo, 2);

  // The small deviation of 1 MeV originates from the fact that in Ref.
  // [arXiv:2011.11655] the light massive flavor corrections related to the
  // charm quark are parametrized in terms of $\overline m_c$(3 GeV), while in
  // REvolver they are parametrized in terms of the charm standard running mass
  // $\overline m_c$.

  // Performing the same conversion with R-evolution to resum logarithms of the
  // intrinsic physical scales of the mass schemes gives

  print(corebc.masses().mKin(5, 1.0), 2);

  // i.e. the bottom quark kinetic mass with log resummation is larger by about
  // 7 MeV. We note that, following Ref. [arXiv:2011.11655], the default value
  // for the kinetic mass intrinsic scale (where the default log-resummed
  // conversion between the running and the kinetic mass is carried out) is set
  // to be twice its renormalization scale, that is, 2 GeV in the example above.

  cout << "### 6.2.4. Top quark mass at low scales\n\n";

  // We return to Ref. [arXiv:1502.01030] and investigate the top quark mass
  // conversion between the MSbar and various short-distance schemes with 4-loop
  // accuracy. This concerns Tab. I of that article.

  // Following Ref. [arXiv:1502.01030] we first define the values for the strong
  // QCD coupling $\alpha_s^{(5)}=0.1185$, the top quark standard running mass
  // $\overline m_t=163.643\,$GeV and the renormalization scale of the PS mass
  // $\mu_f$=20 GeV:

  amZ = 0.1185, mtmt = 163.643;
  double mufT = 20.0;

  // and a Core with a top threshold matching scale of twice the top standard
  // running mass

  mPar = {RunPar(6, mtmt, mtmt)};
  fMatch = {2.0};
  Core coret(6, RunPar(5, amZ, mZdef), mPar, fMatch);

  // We evaluate the PS and 1S top quark mass employing REvolver and determine
  // the difference to the values $m_t^{\rm 1S} = 172.227\,$GeV and
  // $m_t^{\rm PS}(20\,{\rm GeV})=171.792\,$GeV quoted in the reference paper by
  // converting directly from the MSbar scheme, leading to

  double mtPSRef = 171.792;
  double mtPSRevo = coret.masses().mPS(6, mufT, 6, mtmt, mtmt);
  print(mtPSRevo);
  print(mtPSRevo - mtPSRef, 2);

  // for the PS scheme, and

  double mt1SRef = 172.227;
  double mt1SRevo = coret.masses().m1S(6, 6, mtmt, Count1S::Relativistic);
  print(mt1SRevo);
  print(mt1SRevo - mt1SRef, 2);

  // for the 1S scheme, respectively.

  // We now investigate the influence of large logarithms of the
  // ratio between the top quark mass and the intrinsic scale of the respective
  // short-distance schemes. For the top quark the impact of the summation of
  // these logarithms can be significant. To this end we consider the
  // convergence of the perturbative series relating the standard running top
  // mass $\overline m_t$ to the PS mass $m_t^{\rm PS}(20\,{\rm GeV})$, the 1S
  // mass $m_t^{\rm 1S}$ as well as the running mass $m_t^{(5)}(2\,{\rm GeV})$.

  // Using fixed-order conversion (without log resummation) from $\overline m_t$
  // and using $\overline m_t$ as the renormalization scale, the top PS mass at
  // 4 loops, and the corresponding corrections of order
  // $\mathcal{O}(\alpha_s^n)$ with $1 \leq n \leq 4$ amount to

  print(coret.masses().mPS(6, mufT, 6, mtmt, mtmt));
  for (int n = 1; n < 5; ++n) {
    print(coret.masses().mPS(6, mufT, 6, mtmt, mtmt, 1.0, n) -
          coret.masses().mPS(6, mufT, 6, mtmt, mtmt, 1.0, n - 1));
  }
  cout << "\n";

  // On the other hand, employing R-evolution to first evolve down to the PS
  // mass renormalization scale $\mu_f=20\,$GeV and then converting to the PS
  // mass at that scale, which is REvolvers default procedure, we obtain

  print(coret.masses().mPS(6, mufT));
  for (int n = 1; n < 5; ++n) {
    print(coret.masses().mPS(6, mufT, 5, mufT, mufT, 1.0, n) -
          coret.masses().mPS(6, mufT, 5, mufT, mufT, 1.0, n - 1));
  }
  cout << "\n";

  // We observe that the corrections are considerably smaller when R-evolution
  // is accounted for and that there is a shift of around 4 MeV in the final
  // 4-loop converted values. The $\mathcal{O}(\alpha_s)$ correction term is
  // zero in the case of the log resummed conversion since the 1-loop
  // perturbative coefficient of the PS and MSR masses coincide.

  // For the conversion to the 1S mass the observation regarding fixed-order
  // versus log-resummed conversion is similar. In the fixed-order case we get

  print(coret.masses().m1S(6, 6, mtmt, Count1S::Relativistic, mtmt));
  for (int n = 1; n < 5; ++n) {
    print(coret.masses().m1S(6, 6, mtmt, Count1S::Relativistic, mtmt, n) -
          coret.masses().m1S(6, 6, mtmt, Count1S::Relativistic, mtmt, n - 1));
  }
  cout << "\n";

  // while the log-resummed evaluation gives again substantially smaller
  // corrections:

  double mBohr = coret.masses().mBohr(6);
  print(mBohr);
  print(coret.masses().m1S(6, 5));
  for (int n = 1; n < 5; ++n) {
    print(coret.masses().m1S(6, 5, mBohr, Count1S::Nonrelativistic, mBohr, n) -
          coret.masses().m1S(6, 5, mBohr, Count1S::Nonrelativistic, mBohr,
                             n - 1));
  }
  cout << "\n";

  // where the inverse Bohr radius, i.e. the intrinsic scale of the 1S mass, is
  // shown as the first output for completeness.

  // Finally, we investigate the effect of log resummation on the computation of
  // the top quark 5-flavor running mass at 2 GeV, $m_t^{(5)}(2\,{\rm GeV})$,
  // when converting from $m_t^{(5)}(m_t)$. For simplicity we create a new Core
  // named "coret", this time where for the top threshold matching scale the
  // default value, the top quark standard running mass, is adopted:

  coret = Core(6, RunPar(5, amZ, mZdef), mPar);

  // Now we extract the value of the 5-flavor MSR mass $m_t^{(5)}(m_t)$ at the
  // scale of the standard top running mass from this Core as a reference

  double m5mt = coret.masses().mMS(6, mtmt, 5);
  print(m5mt, 2);

  // REvolver does not provide fixed-order conversions of the running masses
  // between two different renormalization scales. It is, however, possible to
  // access these fixed-order corrections through the pole mass routine
  // mPoleFO. Care has to be taken that the two calls of the mPoleFO
  // routines involve the same renormalization scale (which here is the standard
  // running mass) to ensure that the pole mass renormalon is properly
  // cancelled:

  doubleV FOTab;
  for (int i = 0; i < 5; ++i) {
    FOTab.push_back(coret.masses().mMS(6, 2.0, 5) -
                    coret.masses().mPoleFO(6, 5, 2.0, mtmt, i) +
                    coret.masses().mPoleFO(6, 5, mtmt, mtmt, i));
  }

  print(FOTab.back());
  for (int n = 1; n < 5; ++n) {
    print(FOTab[n] - FOTab[n - 1]);
  }
  cout << "\n";

  // The difference of the two calls of the mPoleFO routine removes the log
  // resummed corrections from the output of mMS and replaces it by the
  // corresponding fixed-order terms. As for the above examples, we first
  // display the $m_t^{(5)}(2\,{\rm GeV})$ value at $\mathcal{O}(\alpha_s^4)$
  // and the fixed-order perturbative correction terms at order
  // $\mathcal{O}(\alpha_s^n)$ with $1 \leq n \leq 4$.

  // For the investigation of the correction terms in the case of R-evolution we
  // create 4 Cores, setting the respective loop order n of the R-evolution
  // equation} to $1 \leq n \leq 4$

  fMatch = {1.0};
  vector<Core> tRCoreList;
  for (int i = 1; i < 5; ++i) {
    tRCoreList.emplace_back(6, RunPar(5, amZ, mZdef), mPar, fMatch, 5, 1, 4, 5,
                            1, 4, i);
  }

  // The four Cores differ only by the loop order used for R-evolution, but
  // employ the default 4-loop matching to determine $m_t^{(5)}(m_t)$ from the
  // standard running mass $\overline m_t$. The value of
  // $m_t^{(5)}(2\,{\rm GeV})$ corresponding to 4-loop running and the
  // corrections coming from the individual running orders can be extracted with

  doubleV RevoTab = {m5mt};
  for (const Core& core : tRCoreList) {
    RevoTab.push_back(core.masses().mMS(6, 2.0));
  }
  print(tRCoreList[3].masses().mMS(6, 2.0));
  for (int n = 1; n < 5; ++n) {
    print(RevoTab[n] - RevoTab[n - 1]);
  }
  cout << "\n";

  // where the first correction is the difference between $m_t^{(5)}(m_t)$ and
  // $m_t^{(5)}(2\,{\rm GeV})$ obtained with 1-loop R-evolution while the rest
  // refer to the differences in $m_t^{(5)}(2\,{\rm GeV})$ obtained when adding
  // the 2-, 3- and 4-loop terms to the R-evolution anomalous dimension. Once
  // again we observe that the convergence is much better when using
  // R-evolution, and that the corresponding results are more precise than using
  // fixed-order conversion. Using 4-loop R-evolution leads to a value for
  // $m_t^{(5)}(2\,{\rm GeV})$ that is about 130 MeV higher than when using
  // 4-loop fixed-order conversion. The difference is consistent with the size
  // of the 4-loop fixed-order correction, when adopting the latter as an
  // uncertainty for the 4-loop fixed-order conversion.

  // We see that the higher order corrections to the R-evolution anomalous
  // dimension lead to very small effects, so that one could worry that their
  // size may not reflect the perturbative uncertainty at the corresponding loop
  // order. A different way to estimate perturbative uncertainties is to perform
  // $\lambda$-variation in the R-evolution equation, which we shortly
  // demonstrate in the following.

  // We create a set of Cores with $\lambda$ values in the range $1/2 \leq
  // \lambda \leq 2$ for various loop orders in the R-evolution equation. The
  // Cores are created with

  vector<vector<Core>> coreTlist(4);
  for (int i = 1; i < 5; ++i) {
    for (int j = -50; j <= 50; j += 2) {
      coreTlist[i - 1].emplace_back(6, RunPar(5, amZ, mZdef), mPar, fMatch, 5,
                                    1, 4, 5, 1, 4, i, pow(2.0, j / 50.0));
    }
  }

  // and the table containing the corresponding values of
  // $m_t^{(5)}(2\,{\rm GeV})$ is generated by

  vector<doubleV> mListRevo(4);
  for (int i = 0; i < 4; ++i) {
    for (const Core& core : coreTlist[i]) {
      mListRevo[i].push_back(core.masses().mMS(6, 2.0, 5));
    }
  }

  // Order by order, the values of the $lambda$ variations divided by two are
  // then given by

  for (const doubleV& el : mListRevo) {
    print((*max_element(el.begin(), el.end()) -
           *min_element(el.begin(), el.end())) /
          2.0);
  }
  cout << "\n";

  // The values of $\lambda$ variations are consistent with the size of the
  // corrections to the R-evolution equation and substantially smaller than the
  // size of the fixed-order corrections, illustrating that resumming logarithms
  // via R-evolution leads to more a precise mass conversion than using
  // fixed-order corrections.

  cout << "### 6.2.5. Running masses for complex renormalization scales\n\n";

  // REvolver provides the functionality to determine running masses at complex
  // renormalization scales. We demonstrate the quality of the running mass
  // evolution in the complex plane by showing numerical consistency for the
  // high-scale ($\overline {\rm MS}$) running top quark mass concerning the
  // Cauchy's integral formula, where we adopt $\mu_{\rm MS}$=350 GeV for the
  // central scale and  174 GeV for the radius of the Cauchy integral. We set up
  // a Core with the top quark standard running mass $\overline m_t$=163 GeV
  // and subsequently determine the central running mass and the corresponding
  // value using the residue theorem.

  Core coreO(6, RunPar(5, 0.1181, mZdef), {RunPar(6, 163, 163)});

  double muMS = 350.0, rMS = 174.0;
  double mMSDirect = coreO.masses().mMS(6, muMS, 6);
  complex<double> mMSResidue = IMass(muMS, rMS, 6, coreO);
  print(mMSDirect);
  print(mMSResidue.real());
  print(mMSResidue.imag());
  print(mMSResidue.real() - mMSDirect, 2);

  // Both results are in perfect agreement reflecting the high numerical
  // precision of the evolution routines implemented in REvolver.

  // This high numerical precision is also maintained at much smaller
  // renormalization scales (where perturbation theory in general is less
  // reliable). We demonstrate this for the top quark running (MSR) mass at a
  // central scale of 2 GeV and a radius of 1 GeV for the Cauchy integral:

  double muR = 2.0;
  double mRDirect = coreO.masses().mMS(6, muR, 5);
  complex<double> mRResidue = IMass(muR, 1.0, 5, coreO);
  print(mRDirect);
  print(mRResidue.real());
  print(mRResidue.imag());
  print(mRResidue.real() - mRDirect, 2);

  cout << "### 6.2.6. Top quark pole masses\n\n";

  // REvolver provides functionalities to extract the order-dependent as well as
  // the asymptotic pole mass value from a Core. In case of the asymptotic value
  // and the associated ambiguity REvolver provides routines employing a number
  // of different methods and allowing for various options. In the following we
  // demonstrate some of these functionalities reproducing results quoted in
  // Ref. [arXiv:1605.03609] and [arXiv:1704.01580] focusing on the scenario of
  // the top quark with massless bottom and charm quarks.

  cout << "#### Minimal correction approach for the pole mass ambiguity\n\n";

  // First, we consider Ref. [arXiv:1605.03609] where the top quark pole mass
  // renormalon ambiguity quoted in  Eq. (4.7) is determined from the
  // perturbative series between the pole mass and the standard running mass for
  // the case where the bottom and charm mass effects are ignored. To reproduce
  // the result we define the top standard running mass value used in Ref.
  // [arXiv:1605.03609]

  mtmt = 163.508;

  // and create an associated Core

  coret = Core(6, RunPar(5, 0.1181, mZdef), {RunPar(6, mtmt, mtmt)});

  // We execute the function mPole to extract the asymptotic pole mass value,
  // the ambiguity, and the order of the smallest correction term. The method
  // adopted in Ref. [arXiv:1605.03609] to determine the pole mass ambiguity was
  // associated to the size of the minimal correction and is accessed in
  // REvolver specifying the "min" method:

  double amb;
  int nMin;
  print(
      coret.masses().mPole(6, mtmt, mtmt, PoleMethod::Min, 1.25, &amb, &nMin));
  print(amb);
  print(nMin, 2);

  // Furthermore the third and fourth arguments are set to the standard running
  // top mass mtmt to mimic the choices adopted in Ref. [arXiv:1605.03609]. The
  // choice of arguments when calling the function mPole ensures that
  // the analyzed series is the one for the difference between pole and the
  // (5-flavor) MSR mass at the scale mtmt and that the renormalization scale
  // for the strong coupling is mtmt as well. The values for the asymptotic pole
  // mass and its ambiguity quoted in Eq. (4.8) in Ref. [arXiv:1605.03609] are
  // 173.608 GeV and 67 MeV, respectively. The asymptotic value is in perfect
  // agreement. The small discrepancy of 3 MeV in the ambiguity is related to
  // the fact that REvolver uses the perturbative series for the relation
  // between the pole and the MSR mass for the routine mPole while in
  // Ref. [arXiv:1605.03609] the relation between the pole and the standard
  // running mass was considered.

  cout << "#### Asymptotic series for the pole-MSbar mass relation\n\n";

  // The method of estimating higher order coefficients in the pole-MSbar
  // relation employed by REvolver is described in Sec. 4.4 of Ref.
  // [arXiv:1704.01580] and relies on an asymptotic formula that can, depending
  // on the specified options, reproduce the exactly known coefficients.

  // The values of the asymptotic coefficients can be easily read out using the
  // function mPoleFO. For the orders 5-9 REvolver returns

  for (int i = 5; i < 10; ++i) {
    print((coret.masses().mPoleFO(6, 5, mtmt, mtmt, i) -
           coret.masses().mPoleFO(6, 5, mtmt, mtmt, i - 1)) /
          mtmt / pow((coret.alpha(mtmt, 5) / 4.0 / M_PI), i));
  }
  cout << "\n";

  // for $a_n^{\rm MSR'}$ (i.e. the series coefficients for the MSR and pole
  // mass difference) and

  for (int i = 5; i < 10; ++i) {
    print((coret.masses().mPoleFO(6, 6, mtmt, mtmt, i) -
           coret.masses().mPoleFO(6, 6, mtmt, mtmt, i - 1)) /
          mtmt / pow((coret.alpha(mtmt, 6) / 4.0 / M_PI), i));
  }
  cout << "\n";

  // for $a_n^{\rm MS'}$ (i.e. the series coefficients for the standard running
  // and pole mass difference), agreeing well with Tab. 2 of Ref.
  // [arXiv:1704.01580] within errors.  The deviation from the central values
  // quoted in that table arises because in Ref. [arXiv:1704.01580] the central
  // values of asymmetric uncertainty intervals have been quoted.

  cout << "#### Asymptotic series for the pole-MSbar mass relation and the "
       << "asymptotic pole mass\n\n";

  // Having access to the pole mass at in principle arbitrary order using
  // conversion from MSbar and MSR masses at arbitrary renormalization scales,
  // REvolver allows to easily reproduce the numbers utilized to produce Fig. 6
  // of Ref. [arXiv:1706.08526], where the order-dependent top quark pole mass
  // was shown. We create a new Core to switch to the top standard running mass
  // value $\overline m_t$=163 GeV in accordance with Ref. [arXiv:1706.08526]

  mtmt = 163.0;
  coret = Core(6, RunPar(5, 0.1181, mZdef), {RunPar(6, mtmt, mtmt)});

  // Converting directly from the MSbar mass at the scale $\overline m_t$,
  // including scale variation in the range
  // $\overline m_t/2\leq \mu \leq 2 \overline m_t$ the pole mass, order by
  // order $0 \leq n \leq 12$, is given by

  for (int ord = 0; ord < 13; ++ord) {
    cout << ord << ", " << coret.masses().mPoleFO(6, 6, mtmt, mtmt, ord)
         << "\n  "
         << coret.masses().mPoleFO(6, 6, mtmt, mtmt / 2, ord) -
                coret.masses().mPoleFO(6, 6, mtmt, mtmt, ord)
         << ", "
         << coret.masses().mPoleFO(6, 6, mtmt, 2 * mtmt, ord) -
                coret.masses().mPoleFO(6, 6, mtmt, mtmt, ord)
         << "\n";
  }
  cout << "\n";

  // Analogous commands can be used to extract the relevant values of the pole
  // mass when converting from the MSR scheme at various scales of R. The
  // asymptotic value which can be assigned to the pole mass lies in the region
  // where the series grows linearly, which is roughly in the region around 173
  // GeV. This is confirmed by the output of the command mPole, which
  // gives

  print(
      coret.masses().mPole(6, mtmt, mtmt, PoleMethod::Min, 1.25, &amb, &nMin));
  print(amb);
  print(nMin, 2);

  print(coret.masses().mPole(6, mtmt, mtmt, PoleMethod::Range, 1.25, &amb,
                             &nMin));
  print(amb);
  print(nMin, 2);

  print(coret.masses().mPole(6, mtmt, mtmt, PoleMethod::DRange, 1.25, &amb,
                             &nMin));
  print(amb);
  print(nMin, 2);

  // for the three methods to estimate the asymptotic pole mass supported by
  // REvolver from the relation between the pole and the MSR mass
  // $m_t^{(5)}(\overline m_t)$ at the scale 163 GeV. While the estimation of
  // the ambiguity can vary by more than a factor of 2 depending on the employed
  // strategy, the estimated asymptotic pole mass is fairly stable.

  cout << "## 6.3. Cores with Multiple Massive Quarks\n\n";

  cout << "### 6.3.1. Strong coupling and $\\Lambda_{\\rm QCD}$\n\n";

  cout << "#### Strong coupling evolution through multiple flavor "
          "thresholds\n\n";

  // In Ref. [arXiv:1805.08176] the 3-flavor strong coupling value
  // $\alpha_s^{(3)}(m_\tau)$ was determined from hadronic $e^+ e^-$ R-ratio
  // data for c.m. energies below the charm production threshold. For this
  // determination, fixed-order (FO) as well as contour-improved (CI)
  // perturbation theory were applied, and the corresponding values
  // $\alpha_s^{(3)}(m_\tau)_{\rm FO}$=0.298 and
  // $\alpha_s^{(3)}(m_\tau)_{\rm CI}$=0.304 were subsequently converted to
  // $\alpha_s^{(5)}(m_Z)$ employing the 4-loop beta function  as well as the
  // 3-loop matching relations at the charm and bottom thresholds. The
  // respective values for the strong coupling at the Z-scale were quoted in
  // Eq. (4.7) of that reference as $\alpha_s^{(5)}(m_Z)_{\rm FO}$=0.1158 and
  // $\alpha_s^{(5)}(m_Z)_{\rm CI}$=0.1166, respectively.

  // For the conversion, the values $m_c$=1.28 GeV and $m_b$=4.2 GeV were used
  // for the charm and bottom standard running masses, respectively, and the
  // charm and bottom threshold matching scales were set $\mu_c$=2.0 GeV and
  // $\mu_b$=4.0 GeV, respectively. To reproduce the conversion with REvolver
  // we define these input values with

  mTau = 1.77686;
  mcmc = 1.28, mbmb = 4.2;
  double amTauFO = 0.298, amTauCI = 0.304;
  fMatch = {2.0 / mcmc, 4.0 / mbmb};
  mPar = {RunPar(4, mcmc, mcmc), RunPar(5, mbmb, mbmb)};

  // and define one Core for each value of $\alpha_s^{(3)}(m_\tau)$

  Core coreFO(5, RunPar(3, amTauFO, mTau), mPar, fMatch, 4, 1, 3);
  Core coreCI(5, RunPar(3, amTauCI, mTau), mPar, fMatch, 4, 1, 3);

  // where the parameters relevant for running and matching were set with the
  // optional parameters runAlpha, orderAlpha and fMatch.

  // The values of $\alpha_s^{(5)}(m_Z)$ can now be easily extracted by

  print(coreFO.alpha(mZdef, 5));
  print(coreCI.alpha(mZdef, 5), 2);

  // in full agreement with Eq. (4.7) of  Ref. [arXiv:1805.08176]

  // Similarly, in Ref. [arXiv:0801.1821] $\alpha_s^{(3)}(m_\tau)$ was
  // determined from $\tau$ decay data and converted to $\alpha_s^{(5)}(m_Z)$
  // employing 4-loop evolution and matching. We update the values for the
  // standard running bottom and charm masses to  the ones in the reference
  // paper and define the value of $\alpha_s^{(3)}(m_\tau)$ as given in Eq. (19)
  // therein as well as the associated Core

  mcmc = 1.286, mbmb = 4.164;
  double amTau = 0.332;

  fMatch = {1.0, 1.0};
  mPar = {RunPar(4, mcmc, mcmc), RunPar(5, mbmb, mbmb)};
  coreO = Core(5, RunPar(3, amTau, mTau), mPar, fMatch, 4);

  // where, following Ref. [arXiv:0801.1821], the strong coupling evolution is
  // set to 4-loop precision. We focus on the central value here for brevity.
  // The value of $\alpha_s^{(5)}(m_Z)$ can now easily be extracted with

  print(coreO.alpha(mZdef, 5), 2);

  // agreeing with Eq. (20) of Ref. [arXiv:0801.1821].

  cout << "#### Flavor number dependence of the QCD scale $\\Lambda_{\\rm "
          "QCD}^{(n_f)}$\n\n";

  // To demonstrate the capability of REvolver to extract
  // $\Lambda_{\rm QCD}^{(n_f)}$ in various flavor number schemes we consider
  // Ref. [arXiv:1701.03075], where $\alpha_s^{(5)}(m_Z)$=0.1179 has been
  // determined by the ALPHA collaboration and values for
  // $\Lambda_{\rm QCD}^{(n_f)}$ with $3 \leq n_f \leq 5$ were determined, see
  // Eqs. (4.14) as well as (5.3) and (5.4) of that reference.

  // To reproduce the $\Lambda_{\rm QCD}^{(n_f)}$ values with REvolver we create
  // a Core with massive bottom and charm quarks (with standard running mass
  // values taken from the PDG [DOI: 10.1093/ptep/ptaa104]) and define the value
  // of $\alpha_s^{(5)}(m_Z)$ to be the one given in the reference paper

  amZ = 0.1179, mcmc = 1.27, mbmb = 4.18;

  mPar = {RunPar(4, mcmc, mcmc), RunPar(5, mbmb, mbmb)};
  coreO = Core(5, RunPar(5, amZ, mZdef), mPar);

  // We now extract $\Lambda_{\rm QCD}^{(n_f)}$ in the $n_f$=5,4,3 flavor
  // schemes in the usual so-called MSbar scheme with

  for (int nf = 5; nf > 2; --nf) {
    print(coreO.alpha().lambdaQCD(nf));
  }
  cout << "\n";

  // The numbers are in exact agreement with the values shown in Ref.
  // [arXiv:1701.03075].

  cout << "### 6.3.2. Top quark running mass at low scales\n\n";

  // In Ref. [arXiv:1608.01318] a calibration analysis was provided
  // which suggested that the top quark running mass $m_t(1\,{\rm GeV})$
  // agrees within theoretical uncertainties with the MC top mass
  // parameter $m_t^{\rm MC}$ of the Pythia event generator.
  // Specifically, the authors quote a value of
  // $m_t(1\,{\rm GeV})=172.82\pm0.19\,$ GeV for a calibration with
  // $m_t^{\rm MC} = 173\,$GeV, given in Tab. 1. That analysis was
  // carried out in the approximation of massless bottom and charm
  // quarks. Here we demonstrate how REvolver can be used to to
  // investigate how finite charm and bottom quark masses affect the
  // value of the standard running mass $\overline m_t$ calculated from
  // $m_t$(1 GeV). As an estimate of the uncertainties we quote the
  // difference of the values computed using 4- and 3-loop R-evolution.

  // To this end we create six Cores: two with massless bottom and charm
  // quarks, two with a massless charm quark and a massive bottom quark,
  // and two with massive bottom and charm quarks, employing
  // $\overline m_c=1.27\,$GeV and $\overline m_b=4.18\,$GeV for the
  // standard running charm and bottom quark masses. Among the two
  // respective Cores one employs 3-loop and the other 4-loop precision
  // of the R-evolution equation:

  mcmc = 1.27, mbmb = 4.18;
  double mt1 = 172.82;

  mPar = {RunPar(5, mt1, 1.0)};
  fMatch = {1.0};
  Core coreT4(6, RunPar(5, amZdef, mZdef), mPar, fMatch, 5, 1, 4, 5, 1, 4, 4);
  Core coreT3(6, RunPar(5, amZdef, mZdef), mPar, fMatch, 5, 1, 4, 5, 1, 4, 3);

  mPar = {RunPar(5, mbmb, mbmb), RunPar(4, mt1, 1.0)};
  fMatch.push_back(1.0);
  Core coreBT4(6, RunPar(5, amZdef, mZdef), mPar, fMatch, 5, 1, 4, 5, 1, 4, 4);
  Core coreBT3(6, RunPar(5, amZdef, mZdef), mPar, fMatch, 5, 1, 4, 5, 1, 4, 3);

  mPar = {RunPar(4, mcmc, mcmc), RunPar(5, mbmb, mbmb), RunPar(3, mt1, 1.0)};
  fMatch.push_back(1.0);
  Core coreCBT4(6, RunPar(5, amZdef, mZdef), mPar, fMatch, 5, 1, 4, 5, 1, 4, 4);
  Core coreCBT3(6, RunPar(5, amZdef, mZdef), mPar, fMatch, 5, 1, 4, 5, 1, 4, 3);

  // The Core setups specify that $m_t^{\rm MSR}(1\,{\rm  GeV})=172.82\,$GeV is
  // the running top quark mass in the scheme in which all massive flavors above
  // the scale of 1 GeV are integrated out. It is easy to extract the top quark
  // standard running masses and their uncertainties from the Cores above with

  print(coreT4.masses().mMS(6));
  print(coreT4.masses().mMS(6) - coreT3.masses().mMS(6), 2);

  print(coreBT4.masses().mMS(6));
  print(coreBT4.masses().mMS(6) - coreBT3.masses().mMS(6), 2);

  print(coreCBT4.masses().mMS(6));
  print(coreCBT4.masses().mMS(6) - coreCBT3.masses().mMS(6), 2);

  // where the first numbers refer to the setup with massless bottom and charm
  // quarks and the last ones to the one where bottom and charm quarks have
  // mass.

  // We observe that the finite bottom mass lowers the standard top running mass
  // by around 60 MeV and the finite charm mass decreases the standard top
  // running mass by about another 20 MeV. The perturbative uncertainty of the
  // conversion amounts to around 50 MeV in all cases. Together, the finite
  // charm and bottom masses lower the standard running top mass by about 80
  // MeV, which is larger than the perturbative uncertainty.

  cout << "### 6.3.3. Bottom at low scales\n\n";

  // To demonstrate the effect of lighter massive flavors in conversions between
  // short-distance mass schemes we consider Ref. [arXiv:hep-ph/9911461], where
  // effects of the finite charm quark mass on bottom quark mass determinations
  // from $\Upsilon$ mesons were (calculated and) examined. In Eq. (99) of that
  // paper the bottom standard running is computed from the 1S mass
  // $m_b^{1S} = 4.7\,$GeV with and without the contribution of the charm
  // standard running quark mass $\overline m_c$=1.5 GeV. The strong coupling
  // value was set to $\alpha_s^{(4)}(m_b^{1S})=0.216$. We define these
  // parameters with

  double mb1S = 4.7, amb1S = 0.216;
  mcmc = 1.5;

  // and create two 4-flavor Cores, one with a massless charm quark and one with
  // the charm mass given above

  Core corec0(RunPar(4, amb1S, mb1S));
  Core corecm(4, RunPar(4, amb1S, mb1S), {RunPar(4, mcmc, mcmc)});

  // We then add the bottom quark 1S mass $m_b^{1S} = 4.7\,$GeV to the existing
  // Cores, where, following Ref. [arXiv:hep-ph/9911461], the perturbative order
  // of its relation to the standard running mass is $\mathcal{O}(\alpha_s^3)$
  // and the relativistic ("upsilon-expansion") counting scheme is adopted

  corec0.add1SMass(mb1S, 5, mb1S, Count1S::Relativistic, mb1S, 3);
  corecm.add1SMass(mb1S, 5, mb1S, Count1S::Relativistic, mb1S, 3);

  // We have furthermore set the running mass scale and the renormalization
  // scale of the strong coupling to $m_b^{1S}$ and the parameter nfConv to 5 to
  // specify direct conversion without R-evolution to be in accordance with the
  // computation carried out in Ref. [arXiv:hep-ph/9911461].

  // Extracting the standard mass from the massless charm Core gives

  print(corec0.masses().mMS(5), 2);

  // in perfect agreement with
  // $\overline m_b$=(4.7-0.382-0.098-0.030)GeV=4.190GeV as quoted in Ref.
  // [arXiv:hep-ph/9911461]. For the standard mass from the massive charm Core
  // we get

  print(corecm.masses().mMS(5), 2);

  // with a sub-MeV difference to the reference value
  // $\overline m_b$=(4.7-0.382-(0.098+0.0072)-(0.03+0.0049))GeV=4.178GeV.

  cout << "### 6.3.4. Top quark pole masses\n\n";

  // In the following we investigate the influence of massive bottom and charm
  // quarks on the perturbative series relating the top quark standard running
  // mass and the pole mass.

  cout << "#### Pole - MSbar mass series dependence on lighter massive "
          "quarks\n\n";

  // In Ref. [arXiv:1706.08526] the dependence of the high-order asymptotic
  // series for the top quark pole-MSbar mass relation on finite bottom and
  // charm quark mass was examined and an algorithm to determine explicit
  // analytic formulae accounting for the semianalytical results given in Ref.
  // [arXiv:0708.1729] was provided, which is implemented in REvolver. Explicit
  // results for the asymptotic series coefficients beyond
  // $\mathcal{O}(\alpha_s^4)$ were provided in Ref. [arXiv:1706.08526} for the
  // scenarios of massless bottom and charm quarks, massive bottom and massless
  // charm quarks as well as massive bottom and charm quarks. To compare the
  // results of REvolver to those of Ref. [arXiv:1706.08526] we set the
  // appropriate relevant parameters for the standard running charm, bottom and
  // top quark masses as well as for the strong coupling and create the Cores
  // for the three scenarios

  mcmc = 1.3, mbmb = 4.2, mtmt = 163;
  amZ = 0.118;
  double mZ = 91.187;

  alphaPar = RunPar(5, amZ, mZ);
  mPar = {RunPar(6, mtmt, mtmt)};
  Core coreT(6, alphaPar, mPar);

  mPar.emplace(mPar.begin(), 5, mbmb, mbmb);
  Core coreBT(6, alphaPar, mPar);

  mPar.emplace(mPar.begin(), 4, mcmc, mcmc);
  Core coreCBT(6, alphaPar, mPar);

  // In accordance with Ref. [arXiv:1706.08526] we read out the value of the
  // asymptotic top quark pole mass, the associated ambiguity and the order of
  // the minimal correction term from each Core employing the drange approach,
  // which is based on all series terms in the superasymptotic region, by
  // executing

  print(coreT.masses().mPole(6, mtmt, mtmt, PoleMethod::DRange, 1.25, &amb,
                             &nMin));
  print(amb);
  print(nMin, 2);

  print(coreBT.masses().mPole(6, mtmt, mtmt, PoleMethod::DRange, 1.25, &amb,
                              &nMin));
  print(amb);
  print(nMin, 2);

  print(coreCBT.masses().mPole(6, mtmt, mtmt, PoleMethod::DRange, 1.25, &amb,
                               &nMin));
  print(amb);
  print(nMin, 2);

  // The three results agree with the numbers given in Tab. 5 of Ref.
  // [arXiv:1706.08526] within uncertainties.

  // We now return to Ref. [arXiv:1605.03609]. There, finite charm and bottom
  // quark mass effects on the value of the top quark asymptotic pole mass were
  // quantified by an approximate and heuristic method utilizing order-dependent
  // light flavor decoupling in the strong coupling. A numerical value for the
  // resulting ambiguity of the asymptotic pole mass was quoted in Eq. (5.1) of
  // that paper using size of the minimal correction of the asymptotic series.

  // Fixing the value of the top, bottom and charm quark standard running masses
  // to the values given in Ref. [arXiv:1605.03609] and creating an associated
  // Core

  mtmt = 163.508, mbmb = 4.2, mcmc = 1.3;

  mPar = {RunPar(4, mcmc, mcmc), RunPar(5, mbmb, mbmb), RunPar(6, mtmt, mtmt)};
  Core coreCBT2(6, RunPar(5, amZdef, mZdef), mPar);

  print(coreCBT2.masses().mPole(6, mtmt, mtmt, PoleMethod::Min, 1.25, &amb,
                                &nMin));
  print(amb);
  print(nMin, 2);

  // where we employ the min method to match the approach used in Ref.
  // [arXiv:1605.03609]. The asymptotic pole mass value and the associated
  // ambiguity are quoted as 173.667 GeV and 108 MeV, respectively, in that
  // reference and differs by around 50 and 8 MeV, respectively, to the result
  // provided by REvolver. The difference is mainly related to the exact
  // treatment of light massive flavor corrections provided by REvolver.

  cout << "#### Normalization of the pole mass renormalon\n\n";

  // Finally, we demonstrate REvolver's capability to compute the normalization
  // $N_{1/2}^{(n_\ell)}$ of the pole mass renormalon for a given number
  // $n_\ell$ of massless quark flavors. Reusing the Cores created in one of the
  // previous examples, the value of the normalization for $n_\ell$=3,4,5 can be
  // extracted with

  print(coreCBT.masses().N12());
  print(coreBT.masses().N12());
  print(coreT.masses().N12(), 2);

  // in excellent agreement with the corresponding values
  // $N_{1/2}^{(n_\ell=1,2,3)} = 0.5370 \pm 0.0011,0.5056\pm
  // 0.0015,0.4616\pm0.0020$ given for $\mu/\,\overline m=1$ in Tab. 1 of Ref.
  // [arXiv:1605.03609].

  // In Eq. (2.21) of Ref. [arXiv:1706.08526] the $\lambda$-parameter was varied
  // in the range $1/2\leq\lambda\leq 2$, and the associated central
  // value and uncertainty of the renormalon normalization $N_{1/2}^{(n_\ell)}$
  // were determined. The corresponding values for $n_\ell=3,4,5$ can be
  // reproduced by REvolver with

  doubleV list2;
  for (int i = -10; i <= 10; ++i) {
    list2.push_back(pow(2.0, i / 10.0));
  }

  doubleV N3Tab;
  for (double el : list2) {
    N3Tab.push_back(coreCBT.masses().N12(el));
  }
  print((*max_element(N3Tab.begin(), N3Tab.end()) +
         *min_element(N3Tab.begin(), N3Tab.end())) /
        2.0);
  print((*max_element(N3Tab.begin(), N3Tab.end()) -
         *min_element(N3Tab.begin(), N3Tab.end())) /
            2.0,
        2);

  doubleV N4Tab;
  for (double el : list2) {
    N4Tab.push_back(coreBT.masses().N12(el));
  }
  print((*max_element(N4Tab.begin(), N4Tab.end()) +
         *min_element(N4Tab.begin(), N4Tab.end())) /
        2.0);
  print((*max_element(N4Tab.begin(), N4Tab.end()) -
         *min_element(N4Tab.begin(), N4Tab.end())) /
            2.0,
        2);

  doubleV N5Tab;
  for (double el : list2) {
    N5Tab.push_back(coreT.masses().N12(el));
  }
  print((*max_element(N5Tab.begin(), N5Tab.end()) +
         *min_element(N5Tab.begin(), N5Tab.end())) /
        2.0);
  print((*max_element(N5Tab.begin(), N5Tab.end()) -
         *min_element(N5Tab.begin(), N5Tab.end())) /
            2.0,
        2);

  // in perfect agreement with the values $N_{1/2}^{(n_\ell=1,2,3)} = 0.526 \pm
  // 0.012,0.492\pm0.016,0.446\pm0.024$, quoted in Eq. (2.21) of Ref.
  // [arXiv:1706.08526].

  return 0;
}
